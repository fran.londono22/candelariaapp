import 'dart:io';
import 'package:path/path.dart';
import 'package:sqflite/sqflite.dart';
import 'package:path_provider/path_provider.dart';

// database table and column names
final String tableOcupaciones = 'ocupaciones';
final String columnId = '_id';
final String columnIdOcupaciones = 'idOcupaciones';
final String columnNombre  = 'nombre';





// data model class
class Ocupaciones {
  int id;
  int idOcupaciones;
  String nombre;


  Ocupaciones();

  // convenience constructor to create a Word object
  Ocupaciones.fromMap(Map<String, dynamic> map) {
    id = map[columnId];
    idOcupaciones = map[columnIdOcupaciones];
    nombre = map[columnNombre];





  }

  // convenience method to create a Map from this Word object
  Map<String, dynamic> toMap() {
    var map = <String, dynamic>{
      columnIdOcupaciones: idOcupaciones,
      columnNombre: nombre,
  
    };
    if (id != null) {
      map[columnId] = id;
    }
    return map;
  }
}

// singleton class to manage the database
class DatabaseOcupaciones {
  // This is the actual database filename that is saved in the docs directory.
  static final _databaseName = "MyDatabase16.db";
  // Increment this version when you need to change the schema.
  static final _databaseVersion = 1;

  // Make this a singleton class.
  DatabaseOcupaciones._privateConstructor();
  static final DatabaseOcupaciones instance = DatabaseOcupaciones._privateConstructor();

  // Only allow a single open connection to the database.
  static Database _database;
  Future<Database> get database async {
    if (_database != null) return _database;
    _database = await _initDatabase();
    return _database;
  }

  // open the database
  _initDatabase() async {
    // The path_provider plugin gets the right directory for Android or iOS.
    Directory documentsDirectory = await getApplicationDocumentsDirectory();
    String path = join(documentsDirectory.path, _databaseName);
    // Open the database. Can also add an onUpdate callback parameter.
    return await openDatabase(path,
        version: _databaseVersion, onCreate: _onCreate);
  }

  _deleteDatabase() async {
    Directory documentsDirectory = await getApplicationDocumentsDirectory();
    String path = join(documentsDirectory.path, _databaseName);
    await deleteDatabase(path);
  }

  // SQL string to create the database
  Future _onCreate(Database db, int version) async {
    await db.execute('''
              CREATE TABLE $tableOcupaciones (
                $columnId INTEGER PRIMARY KEY,
                $columnIdOcupaciones INTEGER NOT NULL,
                $columnNombre TEXT NOT NULL
                )
                ''');
  }

  // Database helper methods:

  Future<int> insertOcupaciones(Ocupaciones encuesta) async {
    Database db = await database;
    int id = await db.insert(tableOcupaciones, encuesta.toMap());
    return id;
  }

  Future<Ocupaciones> queryOcupacionesFirst(int idOcupaciones) async {
    Database db = await database;
    List<Map> maps = await db.query(tableOcupaciones,
        columns: [
          columnId,
          columnIdOcupaciones,
          columnNombre
        ],
         where: '$columnIdOcupaciones = ?',
        whereArgs: [idOcupaciones]);
    if (maps.length > 0) {
      return Ocupaciones.fromMap(maps.first);
    }
    return null;
  }

  Future<Ocupaciones> queryOcupaciones(int id) async {
    Database db = await database;
    List<Map> maps = await db.query(tableOcupaciones,
        columns: [
          columnId,
          columnIdOcupaciones,
          columnNombre
        ],
         where: '$columnId = ?',
        whereArgs: [id]);
    if (maps.length > 0) {
      return Ocupaciones.fromMap(maps.first);
    }
    return null;
  }

  Future<Ocupaciones> queryOcupacionesId(String nombre) async {
    Database db = await database;
    List<Map> maps = await db.query(tableOcupaciones,
        columns: [
          columnId,
          columnIdOcupaciones,
          columnNombre
        ],
         where: '$columnNombre = ?',
        whereArgs: [nombre]);
    if (maps.length > 0) {
      return Ocupaciones.fromMap(maps.first);
    }
    return null;
  }

  Future<int> updateOcupaciones(Ocupaciones encuesta) async {
     Database db = await database;
    return await db.update(tableOcupaciones, encuesta.toMap(),
        where: '$columnId = ?', whereArgs: [encuesta.id]);
  }

  Future<int> getCountOcupaciones() async {
    //database connection
    Database db = await database;
    var x = await db.rawQuery('SELECT COUNT (*) from $tableOcupaciones');
    int count = Sqflite.firstIntValue(x);
    return count;
}

  /*Future<int> delete(int id) async {
    Database db = await database;
    return await db
        .delete(tableEmpresa, where: '$columnNitEmpresa = ?', whereArgs: [id]);
  }*/

  Future<int> deleteDb() async {
    _deleteDatabase();
    /*
  _deleteDb() async {
    DatabaseHelper helper = DatabaseHelper.instance;
    await helper.deleteDb();
    print('Deleted database');
  }*/
  }

  // TODO: queryAllWords()
  // TODO: delete(int id)
  // TODO: update(Word word)
}
