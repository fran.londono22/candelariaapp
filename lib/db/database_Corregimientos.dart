import 'dart:io';
import 'package:path/path.dart';
import 'package:sqflite/sqflite.dart';
import 'package:path_provider/path_provider.dart';

// database table and column names
final String tableCorregimientos = 'corregimientos';
final String columnId = '_id';
final String columnIdCorregimientos = 'idCorregimientos';
final String columnNombre  = 'nombre';





// data model class
class Corregimientos {
  int id;
  int idCorregimientos;
  String nombre;


  Corregimientos();

  // convenience constructor to create a Word object
  Corregimientos.fromMap(Map<String, dynamic> map) {
    id = map[columnId];
    idCorregimientos = map[columnIdCorregimientos];
    nombre = map[columnNombre];





  }

  // convenience method to create a Map from this Word object
  Map<String, dynamic> toMap() {
    var map = <String, dynamic>{
      columnIdCorregimientos: idCorregimientos,
      columnNombre: nombre,
  
    };
    if (id != null) {
      map[columnId] = id;
    }
    return map;
  }
}

// singleton class to manage the database
class DatabaseCorregimientos {
  // This is the actual database filename that is saved in the docs directory.
  static final _databaseName = "MyDatabase7.db";
  // Increment this version when you need to change the schema.
  static final _databaseVersion = 1;

  // Make this a singleton class.
  DatabaseCorregimientos._privateConstructor();
  static final DatabaseCorregimientos instance = DatabaseCorregimientos._privateConstructor();

  // Only allow a single open connection to the database.
  static Database _database;
  Future<Database> get database async {
    if (_database != null) return _database;
    _database = await _initDatabase();
    return _database;
  }

  // open the database
  _initDatabase() async {
    // The path_provider plugin gets the right directory for Android or iOS.
    Directory documentsDirectory = await getApplicationDocumentsDirectory();
    String path = join(documentsDirectory.path, _databaseName);
    // Open the database. Can also add an onUpdate callback parameter.
    return await openDatabase(path,
        version: _databaseVersion, onCreate: _onCreate);
  }

  _deleteDatabase() async {
    Directory documentsDirectory = await getApplicationDocumentsDirectory();
    String path = join(documentsDirectory.path, _databaseName);
    await deleteDatabase(path);
  }

  // SQL string to create the database
  Future _onCreate(Database db, int version) async {
    await db.execute('''
              CREATE TABLE $tableCorregimientos (
                $columnId INTEGER PRIMARY KEY,
                $columnIdCorregimientos INTEGER NOT NULL,
                $columnNombre TEXT NOT NULL
                )
                ''');
  }

  // Database helper methods:

  Future<int> insertCorregimientos(Corregimientos encuesta) async {
    Database db = await database;
    int id = await db.insert(tableCorregimientos, encuesta.toMap());
    return id;
  }

  Future<Corregimientos> queryCorregimientosFirst(int idCorregimiento) async {
    Database db = await database;
    List<Map> maps = await db.query(tableCorregimientos,
        columns: [
          columnId,
          columnIdCorregimientos,
          columnNombre
        ],
         where: '$columnIdCorregimientos = ?',
        whereArgs: [idCorregimiento]);
    if (maps.length > 0) {
      return Corregimientos.fromMap(maps.first);
    }
    return null;
  }

  Future<Corregimientos> queryCorregimientos(int id) async {
    Database db = await database;
    List<Map> maps = await db.query(tableCorregimientos,
        columns: [
          columnId,
          columnIdCorregimientos,
          columnNombre
        ],
         where: '$columnId = ?',
        whereArgs: [id]);
    if (maps.length > 0) {
      return Corregimientos.fromMap(maps.first);
    }
    return null;
  }

   Future<Corregimientos> queryCorregimientosId(String nombre) async {
    Database db = await database;
    List<Map> maps = await db.query(tableCorregimientos,
        columns: [
          columnId,
          columnIdCorregimientos,
          columnNombre
        ],
         where: '$columnNombre = ?',
        whereArgs: [nombre]);
    if (maps.length > 0) {
      return Corregimientos.fromMap(maps.first);
    }
    return null;
  }

  Future<int> updateCorregimientos(Corregimientos corregimiento) async {
     Database db = await database;
    return await db.update(tableCorregimientos, corregimiento.toMap(),
        where: '$columnId = ?', whereArgs: [corregimiento.id]);
  }

  Future<int> getCountCorregimientos() async {
    //database connection
    Database db = await database;
    var x = await db.rawQuery('SELECT COUNT (*) from $tableCorregimientos');
    int count = Sqflite.firstIntValue(x);
    return count;
}

  /*Future<int> delete(int id) async {
    Database db = await database;
    return await db
        .delete(tableEmpresa, where: '$columnNitEmpresa = ?', whereArgs: [id]);
  }*/

  Future<int> deleteDb() async {
    _deleteDatabase();
    /*
  _deleteDb() async {
    DatabaseHelper helper = DatabaseHelper.instance;
    await helper.deleteDb();
    print('Deleted database');
  }*/
  }

  // TODO: queryAllWords()
  // TODO: delete(int id)
  // TODO: update(Word word)
}
