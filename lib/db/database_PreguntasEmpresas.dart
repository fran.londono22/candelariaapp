import 'dart:io';
import 'package:path/path.dart';
import 'package:sqflite/sqflite.dart';
import 'package:path_provider/path_provider.dart';

// database table and column names
final String tablePreguntasEmpresas = 'preguntasEmpresas';
final String columnId = '_id';
final String columnIdPreguntas = 'idPreguntas';
final String columnPreguntas  = 'preguntas';
final String columnInfoPreguntas = 'infoPreguntas';
final String columnCategoria = 'categoria';





// data model class
class PreguntasEmpresas {
  int id;
  int idPreguntas;
  String preguntas;
  String infoPreguntas;
  String categoria;


  PreguntasEmpresas();

  // convenience constructor to create a Word object
  PreguntasEmpresas.fromMap(Map<String, dynamic> map) {
    id = map[columnId];
    idPreguntas = map[columnIdPreguntas];
    preguntas = map[columnPreguntas];
    infoPreguntas = map[columnInfoPreguntas];
    categoria = map[columnCategoria];





  }

  // convenience method to create a Map from this Word object
  Map<String, dynamic> toMap() {
    var map = <String, dynamic>{
      columnIdPreguntas: idPreguntas,
      columnPreguntas: preguntas,
      columnInfoPreguntas: infoPreguntas,
      columnCategoria: categoria,
  
    };
    if (id != null) {
      map[columnId] = id;
    }
    return map;
  }
}

// singleton class to manage the database
class DatabasePreguntasEmpresas {
  // This is the actual database filename that is saved in the docs directory.
  static final _databaseName = "MyDatabase6.db";
  // Increment this version when you need to change the schema.
  static final _databaseVersion = 1;

  // Make this a singleton class.
  DatabasePreguntasEmpresas._privateConstructor();
  static final DatabasePreguntasEmpresas instance = DatabasePreguntasEmpresas._privateConstructor();

  // Only allow a single open connection to the database.
  static Database _database;
  Future<Database> get database async {
    if (_database != null) return _database;
    _database = await _initDatabase();
    return _database;
  }

  // open the database
  _initDatabase() async {
    // The path_provider plugin gets the right directory for Android or iOS.
    Directory documentsDirectory = await getApplicationDocumentsDirectory();
    String path = join(documentsDirectory.path, _databaseName);
    // Open the database. Can also add an onUpdate callback parameter.
    return await openDatabase(path,
        version: _databaseVersion, onCreate: _onCreate);
  }

  _deleteDatabase() async {
    Directory documentsDirectory = await getApplicationDocumentsDirectory();
    String path = join(documentsDirectory.path, _databaseName);
    await deleteDatabase(path);
  }

  // SQL string to create the database
  Future _onCreate(Database db, int version) async {
    await db.execute('''
              CREATE TABLE $tablePreguntasEmpresas (
                $columnId INTEGER PRIMARY KEY,
                $columnIdPreguntas INTEGER NOT NULL,
                $columnPreguntas TEXT NOT NULL,
                $columnInfoPreguntas TEXT NOT NULL,
                $columnCategoria TEXT NOT NULL
                )
                ''');
  }

  // Database helper methods:

  Future<int> insertPregunta(PreguntasEmpresas encuesta) async {
    Database db = await database;
    int id = await db.insert(tablePreguntasEmpresas, encuesta.toMap());
    return id;
  }

  Future<PreguntasEmpresas> queryPregunta(int id) async {
    Database db = await database;
    List<Map> maps = await db.query(tablePreguntasEmpresas,
        columns: [
          columnId,
          columnIdPreguntas,
          columnPreguntas,
          columnInfoPreguntas,
          columnCategoria
        ],
         where: '$columnIdPreguntas = ?',
        whereArgs: [id]);
    if (maps.length > 0) {
      return PreguntasEmpresas.fromMap(maps.first);
    }
    return null;
  }

  Future<int> updatePregunta(PreguntasEmpresas encuesta) async {
     Database db = await database;
    return await db.update(tablePreguntasEmpresas, encuesta.toMap(),
        where: '$columnId = ?', whereArgs: [encuesta.id]);
  }

  Future<int> getCountPregunta() async {
    //database connection
    Database db = await database;
    var x = await db.rawQuery('SELECT COUNT (*) from $tablePreguntasEmpresas');
    int count = Sqflite.firstIntValue(x);
    return count;
}

  /*Future<int> delete(int id) async {
    Database db = await database;
    return await db
        .delete(tableEmpresa, where: '$columnNitEmpresa = ?', whereArgs: [id]);
  }*/

  Future<int> deleteDb() async {
    _deleteDatabase();
    /*
  _deleteDb() async {
    DatabaseHelper helper = DatabaseHelper.instance;
    await helper.deleteDb();
    print('Deleted database');
  }*/
  }

  // TODO: queryAllWords()
  // TODO: delete(int id)
  // TODO: update(Word word)
}
