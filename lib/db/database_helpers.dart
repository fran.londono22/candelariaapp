import 'dart:io';
import 'package:path/path.dart';
import 'package:sqflite/sqflite.dart';
import 'package:path_provider/path_provider.dart';

// database table and column names
final String tableEmpresas = 'empresas';
final String columnId = '_id';
final String columnNitEmpresa = 'nitEmpresa';
final String columnIdEmpresa = 'idEmpresa';
final String columnNombre = 'nombre';
final String columnTelefono = 'telefono';
final String columnCorreo = 'correo';
final String columnDireccion = 'direccion';
final String columnCorregimiento = 'corregimiento';
//final String columnActividad = 'actividad';
//final String columnCategoria = 'categoria';
final String columnEncuestada = 'encuestada';
final String columnRepresentante = 'representante';
/*
final String columnResponsable = 'responsable';
final String columnNombreARL = 'nombrearl';
final String columnNombreEPS = 'nombreeps';
final String columnNombreAFP = 'nombreafp';
final String columnARL = 'arl';
final String columnEPS = 'eps';
final String columnAFP = 'afp';
final String columnNombreNormativa = 'nombrenormativa';
final String columnNormativa = 'normativa';
final String columnBioseguridad = 'bioseguridad';
final String columnCertificadoARL = 'certficadoARL';
*/
// data model class
class Empresa {
  int id;
  String nit;
  int idEmpresa;
  String nombre;
  String telefono;
  String correo;
  String direccion;
  String corregimiento;
  //String actividad;
  //String categoria;
  String encuestada;
  String representante;
  /*
  String responsable;
  bool arl;
  bool eps;
  bool afp;
  String nombrearl;
  String nombreeps;
  String nombreafp;
  bool normativa;
  String nombrenormativa;
  bool bioseguridad;
  bool certificadoARL;
*/
  Empresa();

  // convenience constructor to create a Word object
  Empresa.fromMap(Map<String, dynamic> map) {
    id = map[columnId];
    nit = map[columnNitEmpresa];
    idEmpresa = map[columnIdEmpresa];
    nombre = map[columnNombre];
    telefono = map[columnTelefono];
    correo = map[columnCorreo];
    direccion = map[columnDireccion];
    corregimiento = map[columnCorregimiento];
    //actividad = map[columnActividad];
    //categoria = map[columnCategoria];
    encuestada = map[columnEncuestada];
    representante = map[columnRepresentante];
/*
    responsable = map[columnResponsable];
    arl = map[columnARL];
    eps = map[columnEPS];
    afp = map[columnAFP];
    nombrearl = map[columnNombreARL];
    nombreeps = map[columnNombreEPS];
    nombreafp = map[columnNombreAFP];
    normativa = map[columnNormativa];
    nombrenormativa = map[columnNombreNormativa];
    bioseguridad = map[columnBioseguridad];
    certificadoARL = map[columnCertificadoARL];
    */
  }

  // convenience method to create a Map from this Word object
  Map<String, dynamic> toMap() {
    var map = <String, dynamic>{
      columnNitEmpresa: nit,
      columnIdEmpresa: idEmpresa,
      columnNombre: nombre,
      columnTelefono: telefono,
      columnCorreo: correo,
      columnDireccion: direccion,
      columnCorregimiento: corregimiento,
      //columnActividad: actividad,
      //columnCategoria: categoria,
      columnEncuestada: encuestada,
      columnRepresentante: representante,
      /*
      columnResponsable: responsable,
      columnARL: arl,
      columnEPS: eps,
      columnAFP: afp,
      columnNombreARL: nombrearl,
      columnNombreEPS: nombreeps,
      columnNombreAFP: nombreafp,
      columnNormativa: normativa,
      columnNombreNormativa: nombrenormativa,
      columnBioseguridad: bioseguridad,
      columnCertificadoARL: certificadoARL
      */
    };
    if (id != null) {
      map[columnId] = id;
    }
    return map;
  }
}

// singleton class to manage the database
class DatabaseHelper {
  // This is the actual database filename that is saved in the docs directory.
  static final _databaseName = "MyDatabase.db";
  // Increment this version when you need to change the schema.
  static final _databaseVersion = 1;

  // Make this a singleton class.
  DatabaseHelper._privateConstructor();
  static final DatabaseHelper instance = DatabaseHelper._privateConstructor();

  // Only allow a single open connection to the database.
  static Database _database;
  Future<Database> get database async {
    if (_database != null) return _database;
    _database = await _initDatabase();
    return _database;
  }

  // open the database
  _initDatabase() async {
    // The path_provider plugin gets the right directory for Android or iOS.
    Directory documentsDirectory = await getApplicationDocumentsDirectory();
    String path = join(documentsDirectory.path, _databaseName);
    // Open the database. Can also add an onUpdate callback parameter.
    return await openDatabase(path,
        version: _databaseVersion, onCreate: _onCreate);
  }

  _deleteDatabase() async {
    Directory documentsDirectory = await getApplicationDocumentsDirectory();
    String path = join(documentsDirectory.path, _databaseName);
    await deleteDatabase(path);
  }

  // SQL string to create the database
  Future _onCreate(Database db, int version) async {
    await db.execute('''
              CREATE TABLE $tableEmpresas (
                $columnId INTEGER PRIMARY KEY,
                $columnNitEmpresa TEXT NOT NULL,
                $columnIdEmpresa INTEGER NOT NULL,
                $columnNombre TEXT NOT NULL,
                $columnTelefono TEXT NOT NULL,
                $columnCorreo TEXT NOT NULL,
                $columnDireccion TEXT NOT NULL,
                $columnCorregimiento TEXT NOT NULL,                
                $columnEncuestada TEXT NOT NULL,
                $columnRepresentante TEXT NOT NULL
                )
                ''');
                /*
                $columnResponsable TEXT NOT NULL,
                $columnARL BOOl NOT NULL,
                $columnEPS BOOL NOT NULL,
                $columnAFP BOOL NOT NULL,
                $columnNombreARL TEXT NOT NULL,
                $columnNombreEPS TEXT NOT NULL,
                $columnNombreAFP TEXT NOT NULL,
                $columnNormativa BOOL NOT NULL,
                $columnNombreNormativa TEXT NOT NULL,
                $columnBioseguridad BOOL NOT NULL,
                $columnCertificadoARL BOOL NOT NULL,
              )
              ''');*/
  }

  // Database helper methods:

  Future<int> insert(Empresa empresa) async {
    Database db = await database;
    int id = await db.insert(tableEmpresas, empresa.toMap());
    return id;
  }

  Future<Empresa> queryProducto(String nitEm) async {
    Database db = await database;
    List<Map> maps = await db.query(tableEmpresas,
        columns: [
          columnId,
          columnNitEmpresa,
          columnIdEmpresa,
          columnNombre,
          columnTelefono,
          columnCorreo,
          columnDireccion,
          columnCorregimiento,          
          //columnActividad,
          //columnCategoria,
          columnEncuestada,
          columnRepresentante,
          /*
          columnResponsable,
          columnARL,
          columnEPS,
          columnAFP,
          columnNombreARL,
          columnNombreEPS,
          columnNombreAFP,
          columnNormativa,
          columnNombreNormativa,
          columnBioseguridad,
          columnCertificadoARL
          */
        ],
         where: '$columnNitEmpresa = ?',
        whereArgs: [nitEm]);
    if (maps.length > 0) {
      return Empresa.fromMap(maps.first);
    }
    return null;
  }

  Future<int> update(Empresa empresa) async {
     Database db = await database;
    return await db.update(tableEmpresas, empresa.toMap(),
        where: '$columnNitEmpresa = ?', whereArgs: [empresa.nit]);
  }

  /*Future<int> delete(int id) async {
    Database db = await database;
    return await db
        .delete(tableEmpresa, where: '$columnNitEmpresa = ?', whereArgs: [id]);
  }*/

  Future<int> deleteDb() async {
    _deleteDatabase();
    /*
  _deleteDb() async {
    DatabaseHelper helper = DatabaseHelper.instance;
    await helper.deleteDb();
    print('Deleted database');
  }*/
  }

  // TODO: queryAllWords()
  // TODO: delete(int id)
  // TODO: update(Word word)
}
