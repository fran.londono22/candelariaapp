import 'dart:io';
import 'package:path/path.dart';
import 'package:sqflite/sqflite.dart';
import 'package:path_provider/path_provider.dart';

// database table and column names
final String tableIndenfiticacion = 'indenfiticacion';
final String columnId = '_id';
final String columnIdIndenfiticacion = 'idIndenfiticacion';
final String columnNombre  = 'nombre';





// data model class
class Indenfiticacion {
  int id;
  int idIndenfiticacion;
  String nombre;


  Indenfiticacion();

  // convenience constructor to create a Word object
  Indenfiticacion.fromMap(Map<String, dynamic> map) {
    id = map[columnId];
    idIndenfiticacion = map[columnIdIndenfiticacion];
    nombre = map[columnNombre];





  }

  // convenience method to create a Map from this Word object
  Map<String, dynamic> toMap() {
    var map = <String, dynamic>{
      columnIdIndenfiticacion: idIndenfiticacion,
      columnNombre: nombre,
  
    };
    if (id != null) {
      map[columnId] = id;
    }
    return map;
  }
}

// singleton class to manage the database
class DatabaseIndenfiticacion {
  // This is the actual database filename that is saved in the docs directory.
  static final _databaseName = "MyDatabase11.db";
  // Increment this version when you need to change the schema.
  static final _databaseVersion = 1;

  // Make this a singleton class.
  DatabaseIndenfiticacion._privateConstructor();
  static final DatabaseIndenfiticacion instance = DatabaseIndenfiticacion._privateConstructor();

  // Only allow a single open connection to the database.
  static Database _database;
  Future<Database> get database async {
    if (_database != null) return _database;
    _database = await _initDatabase();
    return _database;
  }

  // open the database
  _initDatabase() async {
    // The path_provider plugin gets the right directory for Android or iOS.
    Directory documentsDirectory = await getApplicationDocumentsDirectory();
    String path = join(documentsDirectory.path, _databaseName);
    // Open the database. Can also add an onUpdate callback parameter.
    return await openDatabase(path,
        version: _databaseVersion, onCreate: _onCreate);
  }

  _deleteDatabase() async {
    Directory documentsDirectory = await getApplicationDocumentsDirectory();
    String path = join(documentsDirectory.path, _databaseName);
    await deleteDatabase(path);
  }

  // SQL string to create the database
  Future _onCreate(Database db, int version) async {
    await db.execute('''
              CREATE TABLE $tableIndenfiticacion (
                $columnId INTEGER PRIMARY KEY,
                $columnIdIndenfiticacion INTEGER NOT NULL,
                $columnNombre TEXT NOT NULL
                )
                ''');
  }

  // Database helper methods:

  Future<int> insertIndenfiticacion(Indenfiticacion indenfiticacion) async {
    Database db = await database;
    int id = await db.insert(tableIndenfiticacion, indenfiticacion.toMap());
    return id;
  }

  Future<Indenfiticacion> queryIndenfiticacionFirst(int idIdentificacion) async {
    Database db = await database;
    List<Map> maps = await db.query(tableIndenfiticacion,
        columns: [
          columnId,
          columnIdIndenfiticacion,
          columnNombre
        ],
         where: '$columnIdIndenfiticacion = ?',
        whereArgs: [idIdentificacion]);
    if (maps.length > 0) {
      return Indenfiticacion.fromMap(maps.first);
    }
    return null;
  }

  Future<Indenfiticacion> queryIndenfiticacion(int id) async {
    Database db = await database;
    List<Map> maps = await db.query(tableIndenfiticacion,
        columns: [
          columnId,
          columnIdIndenfiticacion,
          columnNombre
        ],
         where: '$columnId = ?',
        whereArgs: [id]);
    if (maps.length > 0) {
      return Indenfiticacion.fromMap(maps.first);
    }
    return null;
  }

  Future<Indenfiticacion> queryIndenfiticacionId(String nombre) async {
    Database db = await database;
    List<Map> maps = await db.query(tableIndenfiticacion,
        columns: [
          columnId,
          columnIdIndenfiticacion,
          columnNombre
        ],
         where: '$columnNombre = ?',
        whereArgs: [nombre]);
    if (maps.length > 0) {
      return Indenfiticacion.fromMap(maps.first);
    }
    return null;
  }

  

  Future<int> updateIndenfiticacion(Indenfiticacion indenfiticacion) async {
     Database db = await database;
    return await db.update(tableIndenfiticacion, indenfiticacion.toMap(),
        where: '$columnId = ?', whereArgs: [indenfiticacion.id]);
  }

  Future<int> getCountIndenfiticacion() async {
    //database connection
    Database db = await database;
    var x = await db.rawQuery('SELECT COUNT (*) from $tableIndenfiticacion');
    int count = Sqflite.firstIntValue(x);
    return count;
}

  /*Future<int> delete(int id) async {
    Database db = await database;
    return await db
        .delete(tableEmpresa, where: '$columnNitEmpresa = ?', whereArgs: [id]);
  }*/

  Future<int> deleteDb() async {
    _deleteDatabase();
    /*
  _deleteDb() async {
    DatabaseHelper helper = DatabaseHelper.instance;
    await helper.deleteDb();
    print('Deleted database');
  }*/
  }

  // TODO: queryAllWords()
  // TODO: delete(int id)
  // TODO: update(Word word)
}
