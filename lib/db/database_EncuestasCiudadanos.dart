import 'dart:io';
import 'package:encuesta/db/database_Respuestas.dart';
import 'package:encuesta/db/database_helpers.dart';
import 'package:path/path.dart';
import 'package:sqflite/sqflite.dart';
import 'package:path_provider/path_provider.dart';

// database table and column names
final String tableEncuestasCiudadanos = 'encuestasCiudadanos';
final String columnId = '_id';
final String columnNombre = 'nombre';
final String columnApellidos = 'apellidos';
final String columnTipoIdentificacion = 'tipoIdentificacion';
final String columnIdentificacion = 'identificacion';
final String columTelefono = 'telefono';
final String columViveCandelaria = 'viveCandelaria';
final String columTrabajaCandelaria = 'trabajaCandelaria';
final String columDepartamento = 'departamento';
final String columMunicipio = 'municipio';
final String columCorregimiento = 'corregimiento';
final String columVereda = 'vereda';
final String columCallejon = 'callejon';
final String columnBarrio = 'barrio';
final String columDireccion = 'direccion';
final String columFrecuenciaVisita = 'frecuenciaVisita';
final String columLugarFrecuenta = 'lugarFrecuenta';
final String columMotivoVisita = 'motivoVisita';
final String columTiempoVisita = 'tiempoVisita';
final String columnSexo = 'sexo';
final String columNacimiento = 'nacimiento';
final String columnEtnia = 'etnia';
final String columnGrupoPoblacional = 'grupoPoblacional';
final String columnVulnerabilidad = 'vulnerabilidad';
final String columnOcupacion = 'ocupacion';
final String columnTipoTrabajador = 'tipoTrabajador';
final String columnNombreEmpresa = 'nombreEmpresa';
final String columnNombreDireccionEmpresa = 'direccionEmpresa';
final String columnEps = 'eps';
final String columnOtraEps = 'otraEps';
final String columnDificultadEps = 'dificultadEps';
final String columnDiabetes = 'diabetes';
final String columnEnfermedadCardiovascular = 'enfermedadCardiovascular';
final String columnCancer = 'cancer';
final String columnEnfermedadRenal = 'enfermedadRenal';
final String columnAsma = 'asma';
final String columnEnfermedadPulmonar = 'enfermedadPulmonar';
final String columnImunusupresion = 'imunusupresion';
final String columnObesidad = 'obesidad';
final String columnDesnutricion = 'desnutricion';
final String columnTrastornoNeurologico = 'trastornoNeurologico';
final String columnTabaquismo = 'tabaquismo';
final String columnAlcoholismo = 'alcoholismo';
final String columnSaludMental = 'saludMental';
final String columnTos = 'tos';
final String columnDificultadRespiratoria = 'dificultadRespiratoria';
final String columnDolorGarganta = 'dolorGarganta';
final String columnFiebre = 'fiebre';
final String columnFatiga = 'fatiga';
final String columnMalestarGeneral = 'malestarGeneral';
final String columnDisminucionGusto = 'disminucionGusto';
final String columnDisminucionOlfato = 'disminucionOlfato';
final String columnSecrecionNasal = 'secrecionNasal';
final String columnFechaInicioSintomas = 'fechaInicioSintomas';
final String columnDesplazamientoUltimos = 'desplazamientoUltimos';
final String columnLugarViaje = 'lugarViaje';
final String columnContactoPosible = 'contactoPosible';
final String columnLugarContacto = 'lugarContacto';
final String columnPruebaTemperatura = 'pruebaTemperatura';
final String columnResultadoPruebaTemp = 'resultadoPruebaTemp';
final String columnCasoSospechoso = 'casoSospechoso';
final String columnPruebaAnticuerpos = 'pruebaAnticuerpos';
final String columnPruebaAntigenos = 'pruebaAntigenos';
final String columnPruebaMolecular = 'pruebaMolecular';
final String columnLatitud = 'latitud';
final String columnLongitud = 'longitud';
final String columnEstado = 'estado';

// data model class
class EncuestaCiudadano {
  int id;
  String nombre;
  String apellidos;
  int tipoIdentificacion;
  String identificacion;
  String telefono;
  String viveCandelaria;
  String trabajaCandelaria;
  String frecuenciaVisita;
  String lugarFrecuenta;
  String motivoVisita;
  String tiempoVisita;
  int departamento;
  int municipio;
  int corregimiento;
  int vereda;
  int callejon;
  int barrio;
  String direccion;
  int sexo;
  String nacimiento;
  int etnia;
  int grupoPoblacional;
  int vulnerabilidad;
  int ocupacion;
  int tipoTrabajador;
  String nombreEmpresa;
  String direccionEmpresa;
  int eps;
  String otraEps;
  int dificultadEps;
  String diabetes;
  String enfermedadCardiovascular;
  String cancer;
  String enfermedadRenal;
  String asma;
  String enfermedadPulmonar;
  String imunusupresion;
  String obesidad;
  String desnutricion;
  String trastornoNeurologico;
  String tabaquismo;
  String alcoholismo;
  int saludMental;
  String tos;
  String dificultadRespiratoria;
  String dolorGarganta;
  String fiebre;
  String fatiga;
  String malestarGeneral;
  String disminucionGusto;
  String disminucionOlfato;
  String secrecionNasal;
  String fechaInicioSintomas;
  String desplazamientoUltimos;
  String lugarViaje;
  String contactoPosible;
  String lugarContacto;
  String pruebaTemperatura;
  String resultadoPruebaTemp;
  String casoSospechoso;
  String pruebaAnticuerpos;
  String pruebaAntigenos;
  String pruebaMolecular;
  String latitud;
  String longitud;
  String estado;

  EncuestaCiudadano();

  // convenience constructor to create a Word object
  EncuestaCiudadano.fromMap(Map<String, dynamic> map) {
    id = map[columnId];
    nombre = map[columnNombre];
    apellidos = map[columnApellidos];
    tipoIdentificacion = map[columnTipoIdentificacion];
    identificacion = map[columnIdentificacion];
    telefono = map[columTelefono];
    viveCandelaria = map[columViveCandelaria];
    trabajaCandelaria = map[columTrabajaCandelaria];
    frecuenciaVisita = map[columFrecuenciaVisita];
    lugarFrecuenta = map[columLugarFrecuenta];
    motivoVisita = map[columMotivoVisita];
    tiempoVisita = map[columTiempoVisita];
    departamento = map[columDepartamento];
    municipio = map[columMunicipio];
    corregimiento = map[columCorregimiento];
    vereda = map[columVereda];
    callejon = map[columCallejon];
    barrio = map[columnBarrio];
    direccion = map[columnDireccion];
    sexo = map[columnSexo];
    nacimiento = map[columNacimiento];
    etnia = map[columnEtnia];
    grupoPoblacional = map[columnGrupoPoblacional];
    vulnerabilidad = map[columnVulnerabilidad];
    ocupacion = map[columnOcupacion];
    tipoTrabajador = map[columnTipoTrabajador];
    nombreEmpresa = map[columnNombreEmpresa];
    direccionEmpresa = map[columnNombreDireccionEmpresa];
    eps = map[columnEps];
    otraEps = map[columnOtraEps];
    dificultadEps = map[columnDificultadEps];
    diabetes = map[columnDiabetes];
    enfermedadCardiovascular = map[columnEnfermedadCardiovascular];
    cancer = map[columnCancer];
    enfermedadRenal = map[columnEnfermedadRenal];
    asma = map[columnAsma];
    enfermedadPulmonar = map[columnEnfermedadPulmonar];
    imunusupresion = map[columnImunusupresion];
    obesidad = map[columnObesidad];
    desnutricion = map[columnDesnutricion];
    trastornoNeurologico = map[columnTrastornoNeurologico];
    tabaquismo = map[columnTabaquismo];
    alcoholismo = map[columnAlcoholismo];
    saludMental = map[columnSaludMental];
    tos = map[columnTos];
    dificultadRespiratoria = map[columnDificultadRespiratoria];
    dolorGarganta = map[columnDolorGarganta];
    fiebre = map[columnFiebre];
    fatiga = map[columnFatiga];
    malestarGeneral = map[columnMalestarGeneral];
    disminucionGusto = map[columnDisminucionGusto];
    disminucionOlfato = map[columnDisminucionOlfato];
    secrecionNasal = map[columnSecrecionNasal];
    fechaInicioSintomas = map[columnFechaInicioSintomas];
    desplazamientoUltimos = map[columnDesplazamientoUltimos];
    lugarViaje = map[columnLugarViaje];
    contactoPosible = map[columnContactoPosible];
    lugarContacto = map[columnLugarContacto];
    pruebaTemperatura = map[columnPruebaTemperatura];
    resultadoPruebaTemp = map[columnResultadoPruebaTemp];
    casoSospechoso = map[columnCasoSospechoso];
    pruebaAnticuerpos = map[columnPruebaAnticuerpos];
    pruebaAntigenos = map[columnPruebaAntigenos];
    pruebaMolecular = map[columnPruebaMolecular];
    latitud = map[columnLatitud];
    longitud = map[columnLongitud];
    estado = map[columnEstado];
  }

  // convenience method to create a Map from this Word object
  Map<String, dynamic> toMap() {
    var map = <String, dynamic>{
      columnNombre: nombre,
      columnApellidos: apellidos,
      columnTipoIdentificacion: tipoIdentificacion,
      columnIdentificacion: identificacion,
      columTelefono: telefono,
      columViveCandelaria: viveCandelaria,
      columTrabajaCandelaria: trabajaCandelaria,
      columFrecuenciaVisita: frecuenciaVisita,
      columLugarFrecuenta: lugarFrecuenta,
      columMotivoVisita: motivoVisita,
      columTiempoVisita: tiempoVisita,
      columDepartamento: departamento,
      columMunicipio: municipio,
      columCorregimiento: corregimiento,
      columVereda: vereda,
      columCallejon: callejon,
      columnBarrio: barrio,
      columDireccion: direccion,
      columnSexo: sexo,
      columNacimiento: nacimiento,
      columnEtnia: etnia,
      columnGrupoPoblacional: grupoPoblacional,
      columnVulnerabilidad: vulnerabilidad,
      columnOcupacion: ocupacion,
      columnTipoTrabajador: tipoTrabajador,
      columnNombreEmpresa: nombreEmpresa,
      columnNombreDireccionEmpresa: direccionEmpresa,
      columnEps: eps,
      columnOtraEps: otraEps,
      columnDificultadEps: dificultadEps,
      columnDiabetes: diabetes,
      columnEnfermedadCardiovascular: enfermedadCardiovascular,
      columnCancer: cancer,
      columnEnfermedadRenal: enfermedadRenal,
      columnAsma: asma,
      columnEnfermedadPulmonar: enfermedadPulmonar,
      columnImunusupresion: imunusupresion,
      columnObesidad: obesidad,
      columnDesnutricion: desnutricion,
      columnTrastornoNeurologico: trastornoNeurologico,
      columnTabaquismo: tabaquismo,
      columnAlcoholismo: alcoholismo,
      columnSaludMental: saludMental,
      columnTos: tos,
      columnDificultadRespiratoria: dificultadRespiratoria,
      columnDolorGarganta: dolorGarganta,
      columnFiebre: fiebre,
      columnFatiga: fatiga,
      columnMalestarGeneral: malestarGeneral,
      columnDisminucionGusto: disminucionGusto,
      columnDisminucionOlfato: disminucionOlfato,
      columnSecrecionNasal: secrecionNasal,
      columnFechaInicioSintomas: fechaInicioSintomas,
      columnDesplazamientoUltimos: desplazamientoUltimos,
      columnLugarViaje: lugarViaje,
      columnContactoPosible: contactoPosible,
      columnLugarContacto: lugarContacto,
      columnPruebaTemperatura: pruebaTemperatura,
      columnResultadoPruebaTemp: resultadoPruebaTemp,
      columnCasoSospechoso: casoSospechoso,
      columnPruebaAnticuerpos: pruebaAnticuerpos,
      columnPruebaAntigenos: pruebaAntigenos,
      columnPruebaMolecular: pruebaMolecular,
      columnLatitud: latitud,
      columnLongitud: longitud,
      columnEstado: estado,
    };
    if (id != null) {
      map[columnId] = id;
    }
    return map;
  }
}

// singleton class to manage the database
class DatabaseEncuestaCiudadano {
  // This is the actual database filename that is saved in the docs directory.
  static final _databaseName = "MyDatabase5.db";
  // Increment this version when you need to change the schema.
  static final _databaseVersion = 1;

  // Make this a singleton class.
  DatabaseEncuestaCiudadano._privateConstructor();
  static final DatabaseEncuestaCiudadano instance =
      DatabaseEncuestaCiudadano._privateConstructor();

  // Only allow a single open connection to the database.
  static Database _database;
  Future<Database> get database async {
    if (_database != null) return _database;
    _database = await _initDatabase();
    return _database;
  }

  // open the database
  _initDatabase() async {
    // The path_provider plugin gets the right directory for Android or iOS.
    Directory documentsDirectory = await getApplicationDocumentsDirectory();
    String path = join(documentsDirectory.path, _databaseName);
    // Open the database. Can also add an onUpdate callback parameter.
    return await openDatabase(path,
        version: _databaseVersion, onCreate: _onCreate);
  }

  _deleteDatabase() async {
    Directory documentsDirectory = await getApplicationDocumentsDirectory();
    String path = join(documentsDirectory.path, _databaseName);
    await deleteDatabase(path);
  }

  // SQL string to create the database
  Future _onCreate(Database db, int version) async {
    await db.execute('''
              CREATE TABLE $tableEncuestasCiudadanos (
                $columnId INTEGER PRIMARY KEY,
                $columnNombre TEXT NOT NULL,
                $columnApellidos TEXT NOT NULL, 
                $columnTipoIdentificacion INTEGER NOT NULL,
                $columnIdentificacion TEXT NOT NULL, 
                $columTelefono TEXT NOT NULL,
                $columViveCandelaria TEXT NOT NULL,
                $columTrabajaCandelaria TEXT,
                $columFrecuenciaVisita TEXT,
                $columLugarFrecuenta TEXT,
                $columMotivoVisita TEXT,
                $columTiempoVisita TEXT,
                $columDepartamento INTEGER, 
                $columMunicipio INTEGER, 
                $columCorregimiento INTEGER, 
                $columVereda INTEGER,
                $columCallejon INTEGER,
                $columnBarrio INTEGER,
                $columDireccion TEXT,
                $columnSexo INTEGER,
                $columNacimiento TEXT,
                $columnEtnia INTEGER,
                $columnGrupoPoblacional INTEGER,
                $columnVulnerabilidad INTEGER,
                $columnOcupacion INTEGER,
                $columnTipoTrabajador INTEGER,
                $columnNombreEmpresa TEXT,
                $columnNombreDireccionEmpresa TEXT,
                $columnEps INTEGER, 
                $columnOtraEps TEXT,
                $columnDificultadEps INTEGER,
                $columnDiabetes TEXT,
                $columnEnfermedadCardiovascular TEXT,
                $columnCancer TEXT,
                $columnEnfermedadRenal TEXT,
                $columnAsma TEXT,
                $columnEnfermedadPulmonar TEXT,
                $columnImunusupresion TEXT,
                $columnObesidad TEXT,
                $columnDesnutricion TEXT,
                $columnTrastornoNeurologico TEXT,
                $columnTabaquismo TEXT,
                $columnAlcoholismo TEXT,
                $columnSaludMental INTEGER,
                $columnTos TEXT NOT NULL,
                $columnDificultadRespiratoria TEXT NOT NULL,
                $columnDolorGarganta TEXT NOT NULL,
                $columnFiebre TEXT NOT NULL,
                $columnFatiga TEXT NOT NULL,
                $columnMalestarGeneral TEXT NOT NULL,
                $columnDisminucionGusto TEXT NOT NULL,
                $columnDisminucionOlfato TEXT NOT NULL,
                $columnSecrecionNasal TEXT NOT NULL,
                $columnFechaInicioSintomas TEXT,
                $columnDesplazamientoUltimos TEXT NOT NULL,
                $columnLugarViaje TEXT,
                $columnContactoPosible TEXT NOT NULL,
                $columnLugarContacto TEXT,
                $columnPruebaTemperatura TEXT NOT NULL,
                $columnResultadoPruebaTemp TEXT,
                $columnCasoSospechoso TEXT NOT NULL,
                $columnPruebaAnticuerpos TEXT NOT NULL,
                $columnPruebaAntigenos TEXT NOT NULL,
                $columnPruebaMolecular TEXT NOT NULL,
                $columnLatitud TEXT,
                $columnLongitud TEXT,
                $columnEstado TEXT NOT NULL
                )
                ''');
  }

  // Database helper methods:

  Future<int> insertEncuestaCiudadano(
      EncuestaCiudadano encuestaCiudadano) async {
    Database db = await database;
    int id =
        await db.insert(tableEncuestasCiudadanos, encuestaCiudadano.toMap());
    return id;
  }

  Future<EncuestaCiudadano> queryEncuestaCiudadano(int id) async {
    Database db = await database;
    List<Map> maps = await db.query(tableEncuestasCiudadanos,
        columns: [
          columnId,
          columnNombre,
          columnApellidos,
          columnTipoIdentificacion,
          columnIdentificacion,
          columTelefono,
          columViveCandelaria,
          columTrabajaCandelaria,
          columFrecuenciaVisita,
          columLugarFrecuenta,
          columMotivoVisita,
          columTiempoVisita,
          columDepartamento,
          columMunicipio,
          columCorregimiento,
          columVereda,
          columCallejon,
          columnBarrio,
          columDireccion,
          columnSexo,
          columNacimiento,
          columnEtnia,
          columnGrupoPoblacional,
          columnVulnerabilidad,
          columnOcupacion,
          columnTipoTrabajador,
          columnNombreEmpresa,
          columnNombreDireccionEmpresa,
          columnEps,
          columnOtraEps,
          columnDificultadEps,
          columnDiabetes,
          columnEnfermedadCardiovascular,
          columnCancer,
          columnEnfermedadRenal,
          columnAsma,
          columnEnfermedadPulmonar,
          columnImunusupresion,
          columnObesidad,
          columnDesnutricion,
          columnTrastornoNeurologico,
          columnTabaquismo,
          columnAlcoholismo,
          columnSaludMental,
          columnTos,
          columnDificultadRespiratoria,
          columnDolorGarganta,
          columnFiebre,
          columnFatiga,
          columnMalestarGeneral,
          columnDisminucionGusto,
          columnDisminucionOlfato,
          columnSecrecionNasal,
          columnFechaInicioSintomas,
          columnDesplazamientoUltimos,
          columnLugarViaje,
          columnContactoPosible,
          columnLugarContacto,
          columnPruebaTemperatura,
          columnResultadoPruebaTemp,
          columnCasoSospechoso,
          columnPruebaAnticuerpos,
          columnPruebaAntigenos,
          columnPruebaMolecular,
          columnLatitud,
          columnLongitud,
          columnEstado,
        ],
        where: '$columnId = ?',
        whereArgs: [id]);
    if (maps.length > 0) {
      return EncuestaCiudadano.fromMap(maps.first);
    }
    return null;
  }

  Future<int> updateEncuestaCiudadano(
      EncuestaCiudadano encuestaCiudadano) async {
    Database db = await database;
    return await db.update(tableEncuestasCiudadanos, encuestaCiudadano.toMap(),
        where: '$columnId = ?', whereArgs: [encuestaCiudadano.id]);
  }

  Future<int> getCountCiudadano() async {
    //database connection
    Database db = await database;
    var x =
        await db.rawQuery('SELECT COUNT (*) from $tableEncuestasCiudadanos');
    int count = Sqflite.firstIntValue(x);
    return count;
  }

  /*Future<int> delete(int id) async {
    Database db = await database;
    return await db
        .delete(tableEmpresa, where: '$columnNitEmpresa = ?', whereArgs: [id]);
  }*/

  Future<int> deleteDb() async {
    _deleteDatabase();
    /*
  _deleteDb() async {
    DatabaseHelper helper = DatabaseHelper.instance;
    await helper.deleteDb();
    print('Deleted database');
  }*/
  }

  // TODO: queryAllWords()
  // TODO: delete(int id)
  // TODO: update(Word word)
}
