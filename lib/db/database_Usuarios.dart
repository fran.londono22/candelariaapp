import 'dart:io';
import 'package:path/path.dart';
import 'package:sqflite/sqflite.dart';
import 'package:path_provider/path_provider.dart';

// database table and column names
final String tableUsuarios = 'usuarios';
final String columnId = '_id';
final String columnNombre = 'nombre';
final String columnToken = 'token';

// data model class
class Usuario {
  int id;
  String nombre;
  String token;

  Usuario();

  // convenience constructor to create a Word object
  Usuario.fromMap(Map<String, dynamic> map) {
    id = map[columnId];
    nombre = map[columnNombre];
    token = map[columnToken];
  }

  // convenience method to create a Map from this Word object
  Map<String, dynamic> toMap() {
    var map = <String, dynamic>{
      columnNombre: nombre,
      columnToken: token,
    };
    if (id != null) {
      map[columnId] = id;
    }
    return map;
  }
}

// singleton class to manage the database
class DatabaseUsuarios {
  // This is the actual database filename that is saved in the docs directory.
  static final _databaseName = "MyDatabase4.db";
  // Increment this version when you need to change the schema.
  static final _databaseVersion = 1;

  // Make this a singleton class.
  DatabaseUsuarios._privateConstructor();
  static final DatabaseUsuarios instance =
      DatabaseUsuarios._privateConstructor();

  // Only allow a single open connection to the database.
  static Database _database;
  Future<Database> get database async {
    if (_database != null) return _database;
    _database = await _initDatabase();
    return _database;
  }

  // open the database
  _initDatabase() async {
    // The path_provider plugin gets the right directory for Android or iOS.
    Directory documentsDirectory = await getApplicationDocumentsDirectory();
    String path = join(documentsDirectory.path, _databaseName);
    // Open the database. Can also add an onUpdate callback parameter.
    return await openDatabase(path,
        version: _databaseVersion, onCreate: _onCreate);
  }

  _deleteDatabase() async {
    Directory documentsDirectory = await getApplicationDocumentsDirectory();
    String path = join(documentsDirectory.path, _databaseName);
    await deleteDatabase(path);
  }

  // SQL string to create the database
  Future _onCreate(Database db, int version) async {
    await db.execute('''
              CREATE TABLE $tableUsuarios (
                $columnId INTEGER PRIMARY KEY,
                $columnNombre TEXT NOT NULL,
                $columnToken TEXT NOT NULL

                )
                ''');
  }

  // Database helper methods:

  Future<int> insertUsuario(Usuario respuesta) async {
    Database db = await database;
    int id = await db.insert(tableUsuarios, respuesta.toMap());
    return id;
  }

  Future<Usuario> queryUsuario(String nombre) async {
    Database db = await database;
    List<Map> maps = await db.query(tableUsuarios,
        columns: [
          columnId,
          columnNombre,
          columnToken,
        ],
        where: '$columnNombre = ?',
        whereArgs: [nombre]);
    if (maps.length > 0) {
      return Usuario.fromMap(maps.first);
    }
    return null;
  }

  Future<int> updateUsuario(Usuario encuesta) async {
    Database db = await database;
    return await db.update(tableUsuarios, encuesta.toMap(),
        where: '$columnNombre = ?', whereArgs: [encuesta.nombre]);
  }

  /*Future<int> delete(int id) async {
    Database db = await database;
    return await db
        .delete(tableEmpresa, where: '$columnNitEmpresa = ?', whereArgs: [id]);
  }*/

  Future<int> deleteDb() async {
    _deleteDatabase();
    /*
  _deleteDb() async {
    DatabaseHelper helper = DatabaseHelper.instance;
    await helper.deleteDb();
    print('Deleted database');
  }*/
  }

  // TODO: queryAllWords()
  // TODO: delete(int id)
  // TODO: update(Word word)
}
