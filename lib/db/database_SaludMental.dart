import 'dart:io';
import 'package:path/path.dart';
import 'package:sqflite/sqflite.dart';
import 'package:path_provider/path_provider.dart';

// database table and column names
final String tableSaludMental = 'saludMental';
final String columnId = '_id';
final String columnIdSaludMental = 'idSaludMental';
final String columnNombre  = 'nombre';





// data model class
class SaludMental {
  int id;
  int idSaludMental;
  String nombre;


  SaludMental();

  // convenience constructor to create a Word object
  SaludMental.fromMap(Map<String, dynamic> map) {
    id = map[columnId];
    idSaludMental = map[columnIdSaludMental];
    nombre = map[columnNombre];





  }

  // convenience method to create a Map from this Word object
  Map<String, dynamic> toMap() {
    var map = <String, dynamic>{
      columnIdSaludMental: idSaludMental,
      columnNombre: nombre,
  
    };
    if (id != null) {
      map[columnId] = id;
    }
    return map;
  }
}

// singleton class to manage the database
class DatabaseSaludMental {
  // This is the actual database filename that is saved in the docs directory.
  static final _databaseName = "MyDatabase22.db";
  // Increment this version when you need to change the schema.
  static final _databaseVersion = 1;

  // Make this a singleton class.
  DatabaseSaludMental._privateConstructor();
  static final DatabaseSaludMental instance = DatabaseSaludMental._privateConstructor();

  // Only allow a single open connection to the database.
  static Database _database;
  Future<Database> get database async {
    if (_database != null) return _database;
    _database = await _initDatabase();
    return _database;
  }

  // open the database
  _initDatabase() async {
    // The path_provider plugin gets the right directory for Android or iOS.
    Directory documentsDirectory = await getApplicationDocumentsDirectory();
    String path = join(documentsDirectory.path, _databaseName);
    // Open the database. Can also add an onUpdate callback parameter.
    return await openDatabase(path,
        version: _databaseVersion, onCreate: _onCreate);
  }

  _deleteDatabase() async {
    Directory documentsDirectory = await getApplicationDocumentsDirectory();
    String path = join(documentsDirectory.path, _databaseName);
    await deleteDatabase(path);
  }

  // SQL string to create the database
  Future _onCreate(Database db, int version) async {
    await db.execute('''
              CREATE TABLE $tableSaludMental (
                $columnId INTEGER PRIMARY KEY,
                $columnIdSaludMental INTEGER NOT NULL,
                $columnNombre TEXT NOT NULL
                )
                ''');
  }

  // Database helper methods:

  Future<int> insertSaludMental(SaludMental saludMental) async {
    Database db = await database;
    int id = await db.insert(tableSaludMental, saludMental.toMap());
    return id;
  }

  Future<SaludMental> querySaludMentalFirst(int idSaludMental) async {
    Database db = await database;
    List<Map> maps = await db.query(tableSaludMental,
        columns: [
          columnId,
          columnIdSaludMental,
          columnNombre
        ],
         where: '$columnIdSaludMental = ?',
        whereArgs: [idSaludMental]);
    if (maps.length > 0) {
      return SaludMental.fromMap(maps.first);
    }
    return null;
  }

  Future<SaludMental> querySaludMental(int id) async {
    Database db = await database;
    List<Map> maps = await db.query(tableSaludMental,
        columns: [
          columnId,
          columnIdSaludMental,
          columnNombre
        ],
         where: '$columnId = ?',
        whereArgs: [id]);
    if (maps.length > 0) {
      return SaludMental.fromMap(maps.first);
    }
    return null;
  }

  Future<SaludMental> querySaludMentalId(String nombre) async {
    Database db = await database;
    List<Map> maps = await db.query(tableSaludMental,
        columns: [
          columnId,
          columnIdSaludMental,
          columnNombre
        ],
         where: '$columnNombre = ?',
        whereArgs: [nombre]);
    if (maps.length > 0) {
      return SaludMental.fromMap(maps.first);
    }
    return null;
  }

  Future<int> updateSaludMental(SaludMental encuesta) async {
     Database db = await database;
    return await db.update(tableSaludMental, encuesta.toMap(),
        where: '$columnId = ?', whereArgs: [encuesta.id]);
  }

  Future<int> getCountSaludMental() async {
    //database connection
    Database db = await database;
    var x = await db.rawQuery('SELECT COUNT (*) from $tableSaludMental');
    int count = Sqflite.firstIntValue(x);
    return count;
}

  /*Future<int> delete(int id) async {
    Database db = await database;
    return await db
        .delete(tableEmpresa, where: '$columnNitEmpresa = ?', whereArgs: [id]);
  }*/

  Future<int> deleteDb() async {
    _deleteDatabase();
    /*
  _deleteDb() async {
    DatabaseHelper helper = DatabaseHelper.instance;
    await helper.deleteDb();
    print('Deleted database');
  }*/
  }

  // TODO: queryAllWords()
  // TODO: delete(int id)
  // TODO: update(Word word)
}
