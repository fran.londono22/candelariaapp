import 'dart:io';
import 'package:path/path.dart';
import 'package:sqflite/sqflite.dart';
import 'package:path_provider/path_provider.dart';

// database table and column names
final String tableMunicipios = 'municipios';
final String columnId = '_id';
final String columnIdMunicipios = 'idMunicipios';
final String columnCodigoDepartamento = 'codigoDepartamento';
final String columnNombre  = 'nombre';





// data model class
class Municipios {
  int id;
  int idMunicipios;
  int codigoDepartamento;
  String nombre;


  Municipios();

  // convenience constructor to create a Word object
  Municipios.fromMap(Map<String, dynamic> map) {
    id = map[columnId];
    idMunicipios = map[columnIdMunicipios];
    codigoDepartamento = map[columnCodigoDepartamento];
    nombre = map[columnNombre];





  }

  // convenience method to create a Map from this Word object
  Map<String, dynamic> toMap() {
    var map = <String, dynamic>{
      columnIdMunicipios: idMunicipios,
      columnCodigoDepartamento: codigoDepartamento,
      columnNombre: nombre,
  
    };
    if (id != null) {
      map[columnId] = id;
    }
    return map;
  }
}

// singleton class to manage the database
class DatabaseMunicipios {
  // This is the actual database filename that is saved in the docs directory.
  static final _databaseName = "MyDatabase18.db";
  // Increment this version when you need to change the schema.
  static final _databaseVersion = 1;

  // Make this a singleton class.
  DatabaseMunicipios._privateConstructor();
  static final DatabaseMunicipios instance = DatabaseMunicipios._privateConstructor();

  // Only allow a single open connection to the database.
  static Database _database;
  Future<Database> get database async {
    if (_database != null) return _database;
    _database = await _initDatabase();
    return _database;
  }

  // open the database
  _initDatabase() async {
    // The path_provider plugin gets the right directory for Android or iOS.
    Directory documentsDirectory = await getApplicationDocumentsDirectory();
    String path = join(documentsDirectory.path, _databaseName);
    // Open the database. Can also add an onUpdate callback parameter.
    return await openDatabase(path,
        version: _databaseVersion, onCreate: _onCreate);
  }

  _deleteDatabase() async {
    Directory documentsDirectory = await getApplicationDocumentsDirectory();
    String path = join(documentsDirectory.path, _databaseName);
    await deleteDatabase(path);
  }

  // SQL string to create the database
  Future _onCreate(Database db, int version) async {
    await db.execute('''
              CREATE TABLE $tableMunicipios (
                $columnId INTEGER PRIMARY KEY,
                $columnIdMunicipios INTEGER NOT NULL,
                $columnCodigoDepartamento INTEGER NOT NULL,
                $columnNombre TEXT NOT NULL
                )
                ''');
  }

  // Database helper methods:

  Future<int> insertMunicipios(Municipios encuesta) async {
    Database db = await database;
    int id = await db.insert(tableMunicipios, encuesta.toMap());
    return id;
  }

  Future<Municipios> queryMunicipiosFirst(int idMunicipios) async {
    Database db = await database;
    List<Map> maps = await db.query(tableMunicipios,
        columns: [
          columnId,
          columnIdMunicipios,
          columnCodigoDepartamento,
          columnNombre
        ],
         where: '$columnIdMunicipios = ?',
        whereArgs: [idMunicipios]);
    if (maps.length > 0) {
      return Municipios.fromMap(maps.first);
    }
    return null;
  }

  Future<Municipios> queryMunicipios(int id, int codigoDepartamento) async {
    Database db = await database;
    List<Map> maps = await db.query(tableMunicipios,
        columns: [
          columnId,
          columnIdMunicipios,
          columnCodigoDepartamento,
          columnNombre
        ],
         where: ' $columnId = ? AND $columnCodigoDepartamento = ?',
        whereArgs: [id, codigoDepartamento]);
    if (maps.length > 0) {
      return Municipios.fromMap(maps.first);
    }
    return null;
  }

   Future<Municipios> queryMunicipiosId(String nombre) async {
    Database db = await database;
    List<Map> maps = await db.query(tableMunicipios,
        columns: [
          columnId,
          columnIdMunicipios,
          columnCodigoDepartamento,
          columnNombre
        ],
         where: '$columnNombre = ?',
        whereArgs: [nombre]);
    if (maps.length > 0) {
      return Municipios.fromMap(maps.first);
    }
    return null;
  }

  Future<int> updateMunicipios(Municipios encuesta) async {
     Database db = await database;
    return await db.update(tableMunicipios, encuesta.toMap(),
        where: '$columnId = ?', whereArgs: [encuesta.id]);
  }

  Future<int> getCountMunicipios() async {
    //database connection
    Database db = await database;
    var x = await db.rawQuery('SELECT COUNT (*) from $tableMunicipios');
    int count = Sqflite.firstIntValue(x);
    return count;
}

  /*Future<int> delete(int id) async {
    Database db = await database;
    return await db
        .delete(tableEmpresa, where: '$columnNitEmpresa = ?', whereArgs: [id]);
  }*/

  Future<int> deleteDb() async {
    _deleteDatabase();
    /*
  _deleteDb() async {
    DatabaseHelper helper = DatabaseHelper.instance;
    await helper.deleteDb();
    print('Deleted database');
  }*/
  }

  // TODO: queryAllWords()
  // TODO: delete(int id)
  // TODO: update(Word word)
}
