import 'dart:io';
import 'package:path/path.dart';
import 'package:sqflite/sqflite.dart';
import 'package:path_provider/path_provider.dart';

// database table and column names
final String tableEncuestas = 'encuestas';
final String columnId = '_id';
final String columnIdEmpresa = 'idEmpresa';
final String columnResponsableSST = 'responsablesst';
final String columnIdentificacionSST = 'identificacionsst';
final String columnEncuestado = 'encuestado';
final String columnIdentificacionEncuestado = 'identificacionencuestado';
final String columnNormatividad = 'normatividad';
final String columndescripcionNormatividad = 'descripcionnormatividad';
final String columnEncuestador = 'encuestador';
final String columnEstado = 'estado';
final String columnImagen1 = 'imagen1';
final String columnImagen2 = 'imagen2';
final String columnImagen3 = 'imagen3';
final String columnImagen4 = 'imagen4';
final String columnImagen5 = 'imagen5';




// data model class
class Encuesta {
  int id;
  int idEmpresa;
  String responsablesst;
  int identificacionsst;
  String encuestado;
  int identificacionencuestado;
  String normatividad;
  String descripcionnormatividad;
  String encuestador;
  String estado;
  String imagen1;
  String imagen2;
  String imagen3;
  String imagen4;
  String imagen5;

  /*
  String responsable;
  bool arl;
  bool eps;
  bool afp;
  String nombrearl;
  String nombreeps;
  String nombreafp;
  bool normativa;
  String nombrenormativa;
  bool bioseguridad;
  bool certificadoARL;
*/
  Encuesta();

  // convenience constructor to create a Word object
  Encuesta.fromMap(Map<String, dynamic> map) {
    id = map[columnId];
    idEmpresa = map[columnIdEmpresa];
    responsablesst = map[columnResponsableSST];
    identificacionsst = map[columnIdentificacionSST];
    encuestado = map[columnEncuestado];
    identificacionencuestado = map[columnIdentificacionEncuestado];
    normatividad = map[columnNormatividad];
    descripcionnormatividad = map[columndescripcionNormatividad];
    encuestador = map[columnEncuestador];
    estado = map[columnEstado];
    imagen1 = map[columnImagen1];
    imagen2 = map[columnImagen2];
    imagen3 = map[columnImagen3];
    imagen4 = map[columnImagen4];
    imagen5 = map[columnImagen5];




  }

  // convenience method to create a Map from this Word object
  Map<String, dynamic> toMap() {
    var map = <String, dynamic>{
      columnIdEmpresa: idEmpresa,
      columnResponsableSST: responsablesst,
      columnIdentificacionSST: identificacionsst,
      columnEncuestado: encuestado,
      columnIdentificacionEncuestado: identificacionencuestado,
      columnNormatividad: normatividad,
      columndescripcionNormatividad: descripcionnormatividad,
      columnEncuestador: encuestador,
      columnEstado: estado,
      columnImagen1: imagen1,
      columnImagen2: imagen2,
      columnImagen3: imagen3,
      columnImagen4: imagen4,
      columnImagen5: imagen5,


    };
    if (id != null) {
      map[columnId] = id;
    }
    return map;
  }
}

// singleton class to manage the database
class DatabaseEncuesta {
  // This is the actual database filename that is saved in the docs directory.
  static final _databaseName = "MyDatabase2.db";
  // Increment this version when you need to change the schema.
  static final _databaseVersion = 1;

  // Make this a singleton class.
  DatabaseEncuesta._privateConstructor();
  static final DatabaseEncuesta instance = DatabaseEncuesta._privateConstructor();

  // Only allow a single open connection to the database.
  static Database _database;
  Future<Database> get database async {
    if (_database != null) return _database;
    _database = await _initDatabase();
    return _database;
  }

  // open the database
  _initDatabase() async {
    // The path_provider plugin gets the right directory for Android or iOS.
    Directory documentsDirectory = await getApplicationDocumentsDirectory();
    String path = join(documentsDirectory.path, _databaseName);
    // Open the database. Can also add an onUpdate callback parameter.
    return await openDatabase(path,
        version: _databaseVersion, onCreate: _onCreate);
  }

  _deleteDatabase() async {
    Directory documentsDirectory = await getApplicationDocumentsDirectory();
    String path = join(documentsDirectory.path, _databaseName);
    await deleteDatabase(path);
  }

  // SQL string to create the database
  Future _onCreate(Database db, int version) async {
    await db.execute('''
              CREATE TABLE $tableEncuestas (
                $columnId INTEGER PRIMARY KEY,
                $columnIdEmpresa INTEGER NOT NULL,
                $columnResponsableSST TEXT NOT NULL,
                $columnIdentificacionSST INTEGER NOT NULL,
                $columnEncuestado TEXT NOT NULL,
                $columnIdentificacionEncuestado INTEGER NOT NULL,
                $columnNormatividad TEXT NOT NULL,                
                $columndescripcionNormatividad TEXT NOT NULL,
                $columnEncuestador TEXT NOT NULL,
                $columnEstado TEXT NOT NULL,
                $columnImagen1 TEXT NOT NULL,
                $columnImagen2 TEXT NOT NULL,
                $columnImagen3 TEXT NOT NULL,
                $columnImagen4 TEXT NOT NULL,
                $columnImagen5 TEXT NOT NULL
                )
                ''');
  }

  // Database helper methods:

  Future<int> insertEncuesta(Encuesta encuesta) async {
    Database db = await database;
    int id = await db.insert(tableEncuestas, encuesta.toMap());
    return id;
  }

  Future<Encuesta> queryEncuesta(int id) async {
    Database db = await database;
    List<Map> maps = await db.query(tableEncuestas,
        columns: [
          columnId,
          columnIdEmpresa,
          columnResponsableSST,
          columnIdentificacionSST,
          columnEncuestado,
          columnIdentificacionEncuestado,
          columnNormatividad,          
          columndescripcionNormatividad,
          columnEncuestador,
          columnEstado,
          columnImagen1,
          columnImagen2,
          columnImagen3,
          columnImagen4,
          columnImagen5
        ],
         where: '$columnIdEmpresa = ?',
        whereArgs: [id]);
    if (maps.length > 0) {
      return Encuesta.fromMap(maps.first);
    }
    return null;
  }

  Future<int> updateEncuesta(Encuesta encuesta) async {
     Database db = await database;
    return await db.update(tableEncuestas, encuesta.toMap(),
        where: '$columnId = ?', whereArgs: [encuesta.id]);
  }

  Future<int> getCount() async {
    //database connection
    Database db = await database;
    var x = await db.rawQuery('SELECT COUNT (*) from $tableEncuestas');
    int count = Sqflite.firstIntValue(x);
    return count;
}

  /*Future<int> delete(int id) async {
    Database db = await database;
    return await db
        .delete(tableEmpresa, where: '$columnNitEmpresa = ?', whereArgs: [id]);
  }*/

  Future<int> deleteDb() async {
    _deleteDatabase();
    /*
  _deleteDb() async {
    DatabaseHelper helper = DatabaseHelper.instance;
    await helper.deleteDb();
    print('Deleted database');
  }*/
  }

  // TODO: queryAllWords()
  // TODO: delete(int id)
  // TODO: update(Word word)
}
