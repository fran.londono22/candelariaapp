import 'dart:io';
import 'package:path/path.dart';
import 'package:sqflite/sqflite.dart';
import 'package:path_provider/path_provider.dart';

// database table and column names
final String tableRespuestas = 'encuestas';
final String columnId = '_id';
final String columnIdEmpresa = 'idEmpresa';
final String columnIdPregunta = 'idPregunta';
final String columnRespuesta = 'respuestas';
final String columnObservaciones = 'observaciones';


// data model class
class Respuesta {
  int id;
  int idEmpresa;
  int idPregunta;
  String respuestas;
  String observaciones;


  Respuesta();

  // convenience constructor to create a Word object
  Respuesta.fromMap(Map<String, dynamic> map) {
    id = map[columnId];
    idEmpresa = map[columnIdEmpresa];
    idPregunta = map[columnIdPregunta];
    respuestas = map[columnRespuesta];
    observaciones = map[columnObservaciones];
 
  }

  // convenience method to create a Map from this Word object
  Map<String, dynamic> toMap() {
    var map = <String, dynamic>{
      columnIdEmpresa: idEmpresa,
      columnIdPregunta: idPregunta,
      columnRespuesta: respuestas,
      columnObservaciones: observaciones,
  
    };
    if (id != null) {
      map[columnId] = id;
    }
    return map;
  }
}

// singleton class to manage the database
class DatabaseRespuestas {
  // This is the actual database filename that is saved in the docs directory.
  static final _databaseName = "MyDatabase3.db";
  // Increment this version when you need to change the schema.
  static final _databaseVersion = 1;

  // Make this a singleton class.
  DatabaseRespuestas._privateConstructor();
  static final DatabaseRespuestas instance =
      DatabaseRespuestas._privateConstructor();

  // Only allow a single open connection to the database.
  static Database _database;
  Future<Database> get database async {
    if (_database != null) return _database;
    _database = await _initDatabase();
    return _database;
  }

  // open the database
  _initDatabase() async {
    // The path_provider plugin gets the right directory for Android or iOS.
    Directory documentsDirectory = await getApplicationDocumentsDirectory();
    String path = join(documentsDirectory.path, _databaseName);
    // Open the database. Can also add an onUpdate callback parameter.
    return await openDatabase(path,
        version: _databaseVersion, onCreate: _onCreate);
  }

  _deleteDatabase() async {
    Directory documentsDirectory = await getApplicationDocumentsDirectory();
    String path = join(documentsDirectory.path, _databaseName);
    await deleteDatabase(path);
  }

  // SQL string to create the database
  Future _onCreate(Database db, int version) async {
    await db.execute('''
              CREATE TABLE $tableRespuestas (
                $columnId INTEGER PRIMARY KEY,
                $columnIdEmpresa INTEGER NOT NULL,
                $columnIdPregunta INTEGER NOT NULL,
                $columnRespuesta TEXT NOT NULL,
                $columnObservaciones TEXT NOT NULL
                )
                ''');
  }

  // Database helper methods:

  Future<int> insertRespuesta(Respuesta respuesta) async {
    Database db = await database;
    int id = await db.insert(tableRespuestas, respuesta.toMap());
    return id;
  }

  Future<Respuesta> queryRespuestas(int idEmpresa, int idPregunta) async {
    Database db = await database;
    List<Map> maps = await db.query(tableRespuestas,
        columns: [
          columnId,
          columnIdEmpresa,
          columnIdPregunta,
          columnRespuesta,
          columnObservaciones
        ],
        where: '$columnIdEmpresa = ? AND $columnIdPregunta = ?',
        whereArgs: [idEmpresa, idPregunta]);
    if (maps.length > 0) {
      return Respuesta.fromMap(maps.first);
    }
    return null;
  }

  Future<int> updateEncuesta(Respuesta encuesta) async {
    Database db = await database;
    return await db.update(tableRespuestas, encuesta.toMap(),
        where: '$columnId = ?', whereArgs: [encuesta.id]);
  }

  /*Future<int> delete(int id) async {
    Database db = await database;
    return await db
        .delete(tableEmpresa, where: '$columnNitEmpresa = ?', whereArgs: [id]);
  }*/

  Future<int> deleteDb() async {
    _deleteDatabase();
    /*
  _deleteDb() async {
    DatabaseHelper helper = DatabaseHelper.instance;
    await helper.deleteDb();
    print('Deleted database');
  }*/
  }

  // TODO: queryAllWords()
  // TODO: delete(int id)
  // TODO: update(Word word)
}
