import 'package:encuesta/db/database_Encuestas.dart';
import 'package:encuesta/db/database_Respuestas.dart';
import 'package:flutter/material.dart';
import './db/database_Encuestas.dart';
import './db/database_Respuestas.dart';
import 'buscarEmpresa.dart';
import 'graficas.dart';
import 'package:flutter/material.dart';
import 'package:percent_indicator/percent_indicator.dart';
import 'package:image_picker/image_picker.dart';
import 'package:mime/mime.dart';
import 'package:path_provider/path_provider.dart';
import 'dart:io';
import 'dart:convert';
import 'db/database_PreguntasEmpresas.dart';


final _observacionController = TextEditingController();
bool opcion;

class EncuestaPage extends StatefulWidget {
  EncuestaPage({Key key, this.idUsuario, this.encuesta}) : super(key: key);
  String idUsuario;
  var encuesta;

  @override
  _EncuestaPage createState() => _EncuestaPage();
}



class _EncuestaPage extends State<EncuestaPage> {
  int id = 0;
  int _radioValue = -1;

  Future<File> file1;
  Future<File> file2;
  Future<File> file3;
  Future<File> file4;
  Future<File> file5;
  String status = '';
  File tmpFile1;
  File tmpFile2;
  File tmpFile3;
  File tmpFile4;
  File tmpFile5;
  String errMessage = 'Error Uploading Image';
  String imagePath = "";
  bool sinc = false;

    @override
  void initState() {
    super.initState();
     print(widget.encuesta);
  sincronizarPreguntas();
  }

  @override
  void dispose() {
    super.dispose();
  }



  final Map<int, String> _respuestas = {

   
  };

  _showPicker1(context) {
  showModalBottomSheet(
      context: context,
      builder: (BuildContext bc) {
        return SafeArea(
          child: Container(
            child: new Wrap(
              children: <Widget>[
                new ListTile(
                    leading: new Icon(Icons.photo_library),
                    title: new Text('Galeria'),
                    onTap: () {
                      chooseImage1();
                      Navigator.of(context).pop();
                    }),
                new ListTile(
                  leading: new Icon(Icons.photo_camera),
                  title: new Text('Camara'),
                  onTap: () {
                    _imgFromCamera1();
                    Navigator.of(context).pop();
                  },
                ),
              ],
            ),
          ),
        );
      }
    );
}

_showPicker2(context) {
  showModalBottomSheet(
      context: context,
      builder: (BuildContext bc) {
        return SafeArea(
          child: Container(
            child: new Wrap(
              children: <Widget>[
                new ListTile(
                    leading: new Icon(Icons.photo_library),
                    title: new Text('Galeria'),
                    onTap: () {
                      chooseImage2();
                      Navigator.of(context).pop();
                    }),
                new ListTile(
                  leading: new Icon(Icons.photo_camera),
                  title: new Text('Camara'),
                  onTap: () {
                    _imgFromCamera2();
                    Navigator.of(context).pop();
                  },
                ),
              ],
            ),
          ),
        );
      }
    );
}
_showPicker3(context) {
  showModalBottomSheet(
      context: context,
      builder: (BuildContext bc) {
        return SafeArea(
          child: Container(
            child: new Wrap(
              children: <Widget>[
                new ListTile(
                    leading: new Icon(Icons.photo_library),
                    title: new Text('Galeria'),
                    onTap: () {
                      chooseImage3();
                      Navigator.of(context).pop();
                    }),
                new ListTile(
                  leading: new Icon(Icons.photo_camera),
                  title: new Text('Camara'),
                  onTap: () {
                    _imgFromCamera3();
                    Navigator.of(context).pop();
                  },
                ),
              ],
            ),
          ),
        );
      }
    );
}
_showPicker4(context) {
  showModalBottomSheet(
      context: context,
      builder: (BuildContext bc) {
        return SafeArea(
          child: Container(
            child: new Wrap(
              children: <Widget>[
                new ListTile(
                    leading: new Icon(Icons.photo_library),
                    title: new Text('Galeria'),
                    onTap: () {
                      chooseImage4();
                      Navigator.of(context).pop();
                    }),
                new ListTile(
                  leading: new Icon(Icons.photo_camera),
                  title: new Text('Camara'),
                  onTap: () {
                    _imgFromCamera4();
                    Navigator.of(context).pop();
                  },
                ),
              ],
            ),
          ),
        );
      }
    );
}
_showPicker5(context) {
  showModalBottomSheet(
      context: context,
      builder: (BuildContext bc) {
        return SafeArea(
          child: Container(
            child: new Wrap(
              children: <Widget>[
                new ListTile(
                    leading: new Icon(Icons.photo_library),
                    title: new Text('Galeria'),
                    onTap: () {
                      chooseImage5();
                      Navigator.of(context).pop();
                    }),
                new ListTile(
                  leading: new Icon(Icons.photo_camera),
                  title: new Text('Camara'),
                  onTap: () {
                    _imgFromCamera5();
                    Navigator.of(context).pop();
                  },
                ),
              ],
            ),
          ),
        );
      }
    );
}

  chooseImage1() {
    setState(() {
      file1 = ImagePicker.pickImage(source: ImageSource.gallery);
    });
  }

  _imgFromCamera1() {
  file1 = ImagePicker.pickImage(
    source: ImageSource.camera, imageQuality: 50
  );
  }

  chooseImage2() {
    setState(() {
      file2 = ImagePicker.pickImage(source: ImageSource.gallery);
    });
  }
  _imgFromCamera2() {
  file2 = ImagePicker.pickImage(
    source: ImageSource.camera, imageQuality: 50
  );
  }
  chooseImage3() {
    setState(() {
      file3 = ImagePicker.pickImage(source: ImageSource.gallery);
    });
  }
  _imgFromCamera3() {
  file3 = ImagePicker.pickImage(
    source: ImageSource.camera, imageQuality: 50
  );
  }
  chooseImage4() {
    setState(() {
      file4 = ImagePicker.pickImage(source: ImageSource.gallery);
    });
  }
  _imgFromCamera4() {
  file4 = ImagePicker.pickImage(
    source: ImageSource.camera, imageQuality: 50
  );
  }
  chooseImage5() {
    setState(() {
      file5 = ImagePicker.pickImage(source: ImageSource.gallery);
    });
  }
  _imgFromCamera5() {
  file5 = ImagePicker.pickImage(
    source: ImageSource.camera, imageQuality: 50
  );
  }
  final Map<int, String> _observaciones = {};

  //nombre del encuestado y numero de cedula
  //nombre y apellido y cedula de  responsable sst

  var info = [


  ] ;

  var preg = [
   
  ];

   var categoria = [
   
  ];

  sincronizarPreguntas() async {

    
   DatabasePreguntasEmpresas preguntasSinc = DatabasePreguntasEmpresas.instance;

   int contarPreguntas = await preguntasSinc.getCountPregunta();

   for(var i = 0; i <= contarPreguntas; i++){

          PreguntasEmpresas getPreguntas = await preguntasSinc
              .queryPregunta(i);
              if(getPreguntas != null){

              preg.add(getPreguntas.preguntas);
              info.add(getPreguntas.infoPreguntas);
              categoria.add(getPreguntas.categoria);
              }


   }

   print(preg.length);

   setState(() {
     sinc = true;
   });
   
  print(preg);
  }

  void _handleRadioValueChange(int value) {
    setState(() {
      _radioValue = value;

      switch (_radioValue) {
        case 0:
          opcion = true;
          break;
        case 1:
          opcion = false;
          break;
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
        onWillPop: _onBackPressed,
        child: Scaffold(
      body: SafeArea(
          child: Container(
        color: Color.fromRGBO(255, 255, 255, 1),
        child: ListView(
          children: <Widget>[
            if(sinc == false)
             Container(
                  height: 20,
                  child: CircularProgressIndicator(
                    value: null,
                    strokeWidth: 5.0,
                  )),
            if(sinc == true)
            widgetFormulario()
            ],
        ),
      )),
    ));
  }

  widgetTitulo() {
    return Container(
        margin: const EdgeInsets.only(top: 5.0, bottom: 20.0),
        child: RichText(
          textAlign: TextAlign.center,
          text: TextSpan(
            text:
                "Lista de chequeo - Resolución 666 del 2020 \n Ministerio de Salud y Protección Social",
            style:
                TextStyle(color: Color.fromRGBO(193, 29, 27, 1), fontSize: 20),
          ),
        ));
  }

  widgetFormulario() {
    return Center(
        child: Column(children: <Widget>[
      widgetTitulo(),
      Container(
          alignment: Alignment.centerLeft,
          child: RichText(
            textAlign: TextAlign.left,
            text: TextSpan(
              text: categoria[id].toString(),
              style: TextStyle(color: Color.fromRGBO(100, 100, 100, 1), fontSize: 18, fontWeight: FontWeight.bold),
            ),
          )),
      Container(
          alignment: Alignment.centerLeft,
          child: RichText(
            textAlign: TextAlign.left,
            text: TextSpan(
              text: preg[id].toString(),
              style: TextStyle(color: Color.fromRGBO(0, 0, 0, 1), fontSize: 18),
            ),
          )),
           
      Container(
          child: Row(children: <Widget>[
        new Text(
          'CUMPLE',
          style: new TextStyle(fontSize: 16.0),
        ),
        new Radio(
          value: 0,
          groupValue: _radioValue,
          onChanged: _handleRadioValueChange,
        ),
        new Text(
          'NO CUMPLE',
          style: new TextStyle(fontSize: 16.0),
        ),
        new Radio(
          value: 1,
          groupValue: _radioValue,
          onChanged: _handleRadioValueChange,
        ),
        new Text(
          'NO APLICA',
          style: new TextStyle(fontSize: 16.0),
        ),
        new Radio(
          value: 2,
          groupValue: _radioValue,
          onChanged: _handleRadioValueChange,
        ),
      ])),
      if(id == 0 || id == 1 || id == 2 || id == 5 || id == 8)
      Container(
            padding: const EdgeInsets.only(bottom: 20.0),
            child: Row(children: <Widget>[
              RaisedButton(
                onPressed: () {
                  if(id == 0){
                  _showPicker1(context);
                  }
                  if(id == 1){
                  _showPicker2(context);
                  }
                  if(id == 2){
                  _showPicker3(context);
                  }
                  if(id == 5){
                  _showPicker4(context);
                  }
                  if(id == 8){
                  _showPicker5(context);
                  }
                },
                child: RichText(
                  textAlign: TextAlign.center,
                  text: TextSpan(
                    text: "Seleccionar archivo",
                    style: TextStyle(
                        color: Color.fromRGBO(193, 29, 27, 1), fontSize: 18),
                  ),
                ),
              ),
              if(id == 0)
              FutureBuilder<File>(
                future: file1,
                builder: (BuildContext context, AsyncSnapshot<File> snapshot) {
                  if (snapshot.connectionState == ConnectionState.done &&
                      null != snapshot.data) {
                    tmpFile1 = snapshot.data;

                    return Text(
                      tmpFile1.path,
                      textAlign: TextAlign.center,
                    );
                  } else if (null != snapshot.error) {
                    return const Text(
                       'Error no se cargo la imagen',
                      textAlign: TextAlign.center,
                    );
                  } else {
                    return const Text(
                      'No se ha seleccionado una imagen',
                      textAlign: TextAlign.center,
                    );
                  }
                },
              ),
              if(id == 1)
              FutureBuilder<File>(
                future: file2,
                builder: (BuildContext context, AsyncSnapshot<File> snapshot) {
                  if (snapshot.connectionState == ConnectionState.done &&
                      null != snapshot.data) {
                    tmpFile2 = snapshot.data;

                    return Text(
                      tmpFile2.path,
                      textAlign: TextAlign.center,
                    );
                  } else if (null != snapshot.error) {
                    return const Text(
                       'Error no se cargo la imagen',
                      textAlign: TextAlign.center,
                    );
                  } else {
                    return const Text(
                      'No se ha seleccionado una imagen',
                      textAlign: TextAlign.center,
                    );
                  }
                },
              ),
              if(id == 2)
              FutureBuilder<File>(
                future: file3,
                builder: (BuildContext context, AsyncSnapshot<File> snapshot) {
                  if (snapshot.connectionState == ConnectionState.done &&
                      null != snapshot.data) {
                    tmpFile3 = snapshot.data;
    
                    return Text(
                      tmpFile3.path,
                      textAlign: TextAlign.center,
                    );
                  } else if (null != snapshot.error) {
                    return const Text(
                       'Error no se cargo la imagen',
                      textAlign: TextAlign.center,
                    );
                  } else {
                    return const Text(
                      'No se ha seleccionado una imagen',
                      textAlign: TextAlign.center,
                    );
                  }
                },
              ),
              if(id == 5)
              FutureBuilder<File>(
                future: file4,
                builder: (BuildContext context, AsyncSnapshot<File> snapshot) {
                  if (snapshot.connectionState == ConnectionState.done &&
                      null != snapshot.data) {
                    tmpFile4 = snapshot.data;

                    return Text(
                      tmpFile4.path,
                      textAlign: TextAlign.center,
                    );
                  } else if (null != snapshot.error) {
                    return const Text(
                      'Error no se cargo la imagen',
                      textAlign: TextAlign.center,
                    );
                  } else {
                    return const Text(
                      'No se ha seleccionado una imagen',
                      textAlign: TextAlign.center,
                    );
                  }
                },
              ),
              if(id == 8)
              FutureBuilder<File>(
                future: file5,
                builder: (BuildContext context, AsyncSnapshot<File> snapshot) {
                  if (snapshot.connectionState == ConnectionState.done &&
                      null != snapshot.data) {
                    tmpFile5 = snapshot.data;

                    return Text(
                      tmpFile5.path,
                      textAlign: TextAlign.center,
                    );
                  } else if (null != snapshot.error) {
                    return const Text(
                       'Error no se cargo la imagen',
                      textAlign: TextAlign.center,
                    );
                  } else {
                    return const Text(
                      'No se ha seleccionado una imagen',
                      textAlign: TextAlign.center,
                    );
                  }
                },
              ),
            ]),
          ),
      Container(
          child: TextFormField(
        controller: _observacionController,
        decoration: const InputDecoration(
          hintText: 'OBSERVACIONES',
          border: const OutlineInputBorder(),
          filled: true,
          fillColor: Colors.white,
        ),
      )),
      Container(
          alignment: Alignment.centerLeft,
          child: RichText(
            textAlign: TextAlign.left,
            text: TextSpan(
              text: info[id].toString(),
              style: TextStyle(color: Color.fromRGBO(0, 0, 0, 1), fontSize: 18),
            ),
          )),
          
      Row(mainAxisAlignment: MainAxisAlignment.spaceBetween, children: <Widget>[
        if (id == 0)
          RaisedButton(
            color: Color.fromRGBO(193, 29, 27, 1),
            onPressed: () {
              if (_respuestas.isNotEmpty) {
                borrarEncuesta();
              } else {
                Navigator.of(context).pushAndRemoveUntil(
                    MaterialPageRoute(
                        builder: (BuildContext context) =>
                            BuscarEmpresaPage(idUsuario: "idUsuario")),
                    (Route<dynamic> route) => false);
              }
            },
            child: RichText(
              textAlign: TextAlign.center,
              text: TextSpan(
                text: "Volver",
                style: TextStyle(
                    color: Color.fromRGBO(255, 255, 255, 1), fontSize: 18),
              ),
            ),
          ),
        if (id != 0)
          RaisedButton(
            color: Color.fromRGBO(193, 29, 27, 1),
            onPressed: () {
              setState(() {
                id = id - 1;
                setState(() {
                  if (_respuestas[id] == "CUMPLE") {
                    _radioValue = 0;
                  }
                  if (_respuestas[id] == "NO CUMPLE") {
                    _radioValue = 1;
                  }
                  if (_respuestas[id] == "NO APLICA") {
                    _radioValue = 2;
                  }
                  if (_respuestas[id] == null) {
                    _radioValue = -1;
                  }

                  if (_observaciones[id] == null) {
                    _observacionController.text = "";
                  } else {
                    _observacionController.text = _observaciones[id];
                  }
                });
              });
            },
            child: RichText(
              textAlign: TextAlign.center,
              text: TextSpan(
                text: "Pregunta Anterior",
                style: TextStyle(
                    color: Color.fromRGBO(255, 255, 255, 1), fontSize: 18),
              ),
            ),
          ),
        if (id != preg.length - 1)
          RaisedButton(
            color: Color.fromRGBO(193, 29, 27, 1),
            onPressed: () async {
              setState(() {
                if (_radioValue == -1) {
                  _campoVacio();
                } else {
                  var res;
                  if (_radioValue == 0) {
                    res = "CUMPLE";
                  }

                  if (_radioValue == 1) {
                    res = "NO CUMPLE";
                  }

                  if (_radioValue == 2) {
                    res = "NO APLICA";
                  }
                  _respuestas[id] = res;
                  _observaciones[id] = _observacionController.text;
                  id = id + 1;
                }
                if (_respuestas[id] == "CUMPLE") {
                  _radioValue = 0;
                }
                if (_respuestas[id] == "NO CUMPLE") {
                  _radioValue = 1;
                }
                if (_respuestas[id] == "NO APLICA") {
                  _radioValue = 2;
                }
                if (_respuestas[id] == null) {
                  _radioValue = -1;
                }

                if (_observaciones[id] == null) {
                  _observacionController.text = "";
                } else {
                  _observacionController.text = _observaciones[id];
                }
              });

              

               
             
            },
            child: RichText(
              textAlign: TextAlign.center,
              text: TextSpan(
                text: "Siguiente Pregunta",
                style: TextStyle(
                    color: Color.fromRGBO(255, 255, 255, 1), fontSize: 18),
              ),
            ),
          ),
        if (id == preg.length - 1)
          RaisedButton(
            color: Color.fromRGBO(193, 29, 27, 1),
            onPressed: () async {
              setState(() {
                if (_radioValue == -1) {
                  _campoVacio();
                } else {
                  var res;
                  if (_radioValue == 0) {
                    res = "CUMPLE";
                  }

                  if (_radioValue == 1) {
                    res = "NO CUMPLE";
                  }

                  if (_radioValue == 2) {
                    res = "NO APLICA";
                  }
                  _respuestas[id] = res;
                  _observaciones[id] = _observacionController.text;
                }
                if (_respuestas[id] == "CUMPLE") {
                  _radioValue = 0;
                }
                if (_respuestas[id] == "NO CUMPLE") {
                  _radioValue = 1;
                }
                if (_respuestas[id] == "NO APLICA") {
                  _radioValue = 2;
                }
                if (_respuestas[id] == null) {
                  _radioValue = -1;
                }
              });

               DatabaseEncuesta helper = DatabaseEncuesta.instance;
              Encuesta encuestahecha = await helper
                  .queryEncuesta((widget.encuesta["id_empresa"]));

              if (encuestahecha == null) {
                _save(widget.encuesta, _respuestas, _observaciones);
              }

                  Navigator.of(context).pushAndRemoveUntil(
                      MaterialPageRoute(
                          builder: (BuildContext context) => Chart(respuestas: _respuestas)),
                      (Route<dynamic> route) => false);
              
            },
            child: RichText(
              textAlign: TextAlign.center,
              text: TextSpan(
                text: "Finalizar encuesta y ver indicadores",
                style: TextStyle(
                    color: Color.fromRGBO(255, 255, 255, 1), fontSize: 18),
              ),
            ),
          )
      ]),
      Padding(
              padding: EdgeInsets.all(15.0),
              child: new LinearPercentIndicator(
                width: MediaQuery.of(context).size.width - 50,
                animation: true,
                lineHeight: 20.0,
                animationDuration: 2000,
                percent:(id + 1) / preg.length,
                center: Text((id + 1).toString() + "/" + (preg.length).toString() ),
                linearStrokeCap: LinearStrokeCap.roundAll,
                progressColor: Colors.greenAccent,
              ),
            ),
    ]));
  }

  Future<void> borrarEncuesta() async {
    return showDialog<void>(
      context: context,
      barrierDismissible: false, // user must tap button!
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text('Se eliminarán los datos'),
          content: SingleChildScrollView(
            child: ListBody(
              children: <Widget>[
                Text(
                    'Desea regresar, si lo hace los datos que lleno previamente se eliminarán'),
              ],
            ),
          ),
          actions: <Widget>[
            FlatButton(
              child: Text('Si'),
              onPressed: () {
                Navigator.of(context).pushAndRemoveUntil(
                    MaterialPageRoute(
                        builder: (BuildContext context) =>
                            BuscarEmpresaPage(idUsuario: "idUsuario")),
                    (Route<dynamic> route) => false);
              },
            ),
            FlatButton(
              child: Text('No'),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
          ],
        );
      },
    );
  }

  Future<void> _campoVacio() async {
    return showDialog<void>(
      context: context,
      barrierDismissible: false, // user must tap button!
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text('Hay un campo vacío'),
          content: SingleChildScrollView(
            child: ListBody(
              children: <Widget>[
                Text('Por favor llene todos los campos'),
              ],
            ),
          ),
          actions: <Widget>[
            FlatButton(
              child: Text('Ok'),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
          ],
        );
      },
    );
  }

  _save(encuestaInfo, respuestas, observaciones) async {
    Encuesta encuesta = Encuesta();

    encuesta.idEmpresa = encuestaInfo["id_empresa"];
    encuesta.responsablesst = encuestaInfo["responsable_sst"];
    encuesta.identificacionsst =
        int.parse(encuestaInfo["identificacion_sst"]);
    encuesta.encuestado = encuestaInfo["encuestado"];
    encuesta.identificacionencuestado =
        int.parse(encuestaInfo["identificacion_encuestado"]);
    encuesta.normatividad = encuestaInfo["normatividad"];
    encuesta.descripcionnormatividad =
        encuestaInfo["descripcion_normatividad"];
    encuesta.encuestador = widget.idUsuario;
    encuesta.estado = "false";
    if(tmpFile1 != null){
    encuesta.imagen1 = tmpFile1.path;
    }else{
    encuesta.imagen1 = "";
    }
    if(tmpFile2 != null){
    encuesta.imagen2 = tmpFile2.path;
    }else{
    encuesta.imagen2 = "";
    }
    if(tmpFile3 != null){
    encuesta.imagen3 = tmpFile3.path;
    }else{
    encuesta.imagen3 = "";
    }
    if(tmpFile4 != null){
    encuesta.imagen4 = tmpFile4.path;
    }else{
    encuesta.imagen4 = "";
    }
    
    if(tmpFile5 != null){
    encuesta.imagen5 = tmpFile5.path;
    }else{
    encuesta.imagen5 = "";
    }

    //print(encuesta);

    DatabaseEncuesta helper = DatabaseEncuesta.instance;
    await helper.insertEncuesta(encuesta);
    //print('inserted row: $id');

    for(var i = 0; i < respuestas.length; i++){
    Respuesta respue = Respuesta();

    respue.idEmpresa = encuestaInfo["id_empresa"];
    respue.idPregunta = i;
    respue.respuestas = respuestas[i];
    respue.observaciones = observaciones[i];
    print(encuestaInfo["id_empresa"]);
    print(i);
    print(respuestas[i]);
    print(observaciones[i]);



    DatabaseRespuestas helperRespuestas = DatabaseRespuestas.instance;
     var conclus = await helperRespuestas.insertRespuesta(respue);
     print('inserted row: $conclus');
    }

    

  }

  Future<bool> _onBackPressed() {
    return showDialog(
          context: context,
          builder: (context) => new AlertDialog(
            title: new Text('Salir de la aplicación'),
            content: new Text('¿Quiere salir de la aplicación?'),
            actions: <Widget>[
              new FlatButton(
                child: new Text("Si"),
                onPressed: () {
                  Navigator.of(context).pop(true);
                },
              ),
              new FlatButton(
                child: new Text("No"),
                onPressed: () {
                  Navigator.of(context).pop(false);
                },
              ),
              SizedBox(height: 16),
            ],
          ),
        ) ??
        false;
  }
}
