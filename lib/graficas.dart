import 'package:encuesta/login.dart';
import 'package:flutter/material.dart';
import 'package:charts_flutter/flutter.dart' as charts;
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

class Chart extends StatefulWidget {
  final Widget child;
  var respuestas;

  Chart({Key key, this.child, this.respuestas}) : super(key: key);

  _Chart createState() => _Chart();
}

class _Chart extends State<Chart> with SingleTickerProviderStateMixin{

  int _tabIndex = 0;
  TabController _tabController;


  void _toggleTab() {
    _tabIndex = _tabController.index + 1;
    _tabController.animateTo(_tabIndex);
  }

  void _toggleTabPrev() {
    _tabIndex = _tabController.index - 1;
    _tabController.animateTo(_tabIndex);
  }




  List<charts.Series<General, String>> _generalData;
  List<charts.Series<General, String>> _bioseguridadData;
  List<charts.Series<General, String>> _prevencionData;
  List<charts.Series<General, String>> _manejoData;
  List<charts.Series<General, String>> _comercialesData;

  //GENERAL

  double totalGeneralCumple = 0;
  double totalGeneralNoCumple = 0;
  double totalGeneralNoAplica = 0;
  double totalGeneralPorcentaje = 0;

  //MEDIDAS DE BIOSEGURIDAD

  double lavadoManosCumple = 0;
  double lavadoManosNoCumple = 0;
  double lavadoManosNoAplica = 0;
  double lavadoManosPorcentaje = 0;

  double distanciamientoFisicoCumple = 0;
  double distanciamientoFisicoNoCumple = 0;
  double distanciamientoFisicoNoAplica = 0;
  double distanciamientoFisicoPorcentaje = 0;

  double elementosdeProteccionCumple = 0;
  double elementosdeProteccionNoCumple = 0;
  double elementosdeProteccionNoAplica = 0;
  double elementosdeProteccionPorcentaje = 0;

  double tapabocasCumple = 0;
  double tapabocasNoCumple = 0;
  double tapabocasNoAplica = 0;
  double tapabocasPorcentaje = 0;

  double limpiezaCumple = 0;
  double limpiezaNoCumple = 0;
  double limpiezaNoAplica = 0;
  double limpiezaPorcentaje = 0;

  double manipulacionCumple = 0;
  double manipulacionNoCumple = 0;
  double manipulacionNoAplica = 0;
  double manipulacionPorcentaje = 0;

  double totalMediosBioseguridadCumple = 0;
  double totalMediosBioseguridadNoCumple = 0;
  double totalMediosBioseguridadNoAplica = 0;
  double totalMediosBioseguridadPorcentaje = 0;

  //PREVENCIÓN Y MANEJO DE SITUACIONES DE RIESGO DE CONTAGIO

  double convivenciaCumple = 0;
  double convivenciaNoCumple = 0;
  double convivenciaNoAplica = 0;
  double convivenciaPorcentaje = 0;

  double recomendacionesCumple = 0;
  double recomendacionesNoCumple = 0;
  double recomendacionesNoAplica = 0;
  double recomendacionesPorcentaje = 0;

  double vigilanciaCumple = 0;
  double vigilanciaNoCumple = 0;
  double vigilanciaNoAplica = 0;
  double vigilanciaPorcentaje = 0;

  double totalPrevencionCumple = 0;
  double totalPrevencionNoCumple = 0;
  double totalPrevencionNoAplica = 0;
  double totalPrevencionPorcentaje = 0;

  //MANEJO - PASOS A ASEGUIR EN CASO DE CONTAGIO - COMUNICACIONES

  double prevencionCumple = 0;
  double prevencionNoCumple = 0;
  double prevencionNoAplica = 0;
  double prevencionPorcentaje = 0;

  double pasosCumple = 0;
  double pasosNoCumple = 0;
  double pasosNoAplica = 0;
  double pasosPorcentaje = 0;

  double planCumple = 0;
  double planNoCumple = 0;
  double planNoAplica = 0;
  double planPorcentaje = 0;

  double totalManejoCumple = 0;
  double totalManejoNoCumple = 0;
  double totalManejoNoAplica = 0;
  double totalManejoPorcentaje = 0;

  //COMERCIALES Y COMPLEMENTARIOS

  double transitoCumple = 0;
  double transitoNoCumple = 0;
  double transitoNoAplica = 0;
  double transitoPorcentaje = 0;

  double gobiernoCumple = 0;
  double gobiernoNoCumple = 0;
  double gobiernoNoAplica = 0;
  double gobiernoPorcentaje = 0;

  double saludCumple = 0;
  double saludNoCumple = 0;
  double saludNoAplica = 0;
  double saludPorcentaje = 0;

  double planeacionCumple = 0;
  double planeacionNoCumple = 0;
  double planeacionNoAplica = 0;
  double planeacionPorcentaje = 0;

  double totalComercialesCumple = 0;
  double totalComercialesNoCumple = 0;
  double totalComercialesNoAplica = 0;
  double totalComercialesPorcentaje = 0;

  _procesarDatos() {
    //MEDIDAS DE BIOSEGURIDAD

    for (var i = 0; i < 4; i++) {
      if (widget.respuestas[i] == "CUMPLE") {
        lavadoManosCumple = lavadoManosCumple + 1;
      } else if (widget.respuestas[i] == "NO CUMPLE") {
        lavadoManosNoCumple = lavadoManosNoCumple + 1;
      } else {
        lavadoManosNoAplica = lavadoManosNoAplica + 1;
      }
    }

    if((lavadoManosCumple + lavadoManosNoCumple - lavadoManosNoAplica) < 1){
      lavadoManosPorcentaje = 0;
    }else{

    lavadoManosPorcentaje = lavadoManosCumple /
        (lavadoManosCumple + lavadoManosNoCumple - lavadoManosNoAplica) *
        100;
    }

    for (var i = 4; i < 7; i++) {
      if (widget.respuestas[i] == "CUMPLE") {
        distanciamientoFisicoCumple = distanciamientoFisicoCumple + 1;
      } else if (widget.respuestas[i] == "NO CUMPLE") {
        distanciamientoFisicoNoCumple = distanciamientoFisicoNoCumple + 1;
      } else {
        distanciamientoFisicoNoAplica = distanciamientoFisicoNoAplica + 1;
      }
    }

    if((distanciamientoFisicoCumple +
            distanciamientoFisicoNoCumple -
            distanciamientoFisicoNoAplica) < 1){
      distanciamientoFisicoPorcentaje = 0;
    }else{

    distanciamientoFisicoPorcentaje = distanciamientoFisicoCumple /
        (distanciamientoFisicoCumple +
            distanciamientoFisicoNoCumple -
            distanciamientoFisicoNoAplica) *
        100;
    }

       


    for (var i = 7; i < 10; i++) {
      if (widget.respuestas[i] == "CUMPLE") {
        elementosdeProteccionCumple = elementosdeProteccionCumple + 1;
      } else if (widget.respuestas[i] == "NO CUMPLE") {
        elementosdeProteccionNoCumple = elementosdeProteccionNoCumple + 1;
      } else {
        elementosdeProteccionNoAplica = elementosdeProteccionNoAplica + 1;
      }
    }


if((elementosdeProteccionCumple +
            elementosdeProteccionNoCumple -
            elementosdeProteccionNoAplica) < 1){
      elementosdeProteccionPorcentaje = 0;
    }else{


    elementosdeProteccionPorcentaje = elementosdeProteccionCumple /
        (elementosdeProteccionCumple +
            elementosdeProteccionNoCumple -
            elementosdeProteccionNoAplica) *
        100;

    }


    for (var i = 10; i < 11; i++) {
      if (widget.respuestas[i] == "CUMPLE") {
        tapabocasCumple = tapabocasCumple + 1;
      } else if (widget.respuestas[i] == "NO CUMPLE") {
        tapabocasNoCumple = tapabocasNoCumple + 1;
      } else {
        tapabocasNoAplica = tapabocasNoAplica + 1;
      }
    }

    if((tapabocasCumple + tapabocasNoCumple - tapabocasNoAplica) < 1){
      tapabocasPorcentaje = 0;
    }else{

    tapabocasPorcentaje = tapabocasCumple /
        (tapabocasCumple + tapabocasNoCumple - tapabocasNoAplica) *
        100;


    }


    for (var i = 11; i < 13; i++) {
      if (widget.respuestas[i] == "CUMPLE") {
        limpiezaCumple = limpiezaCumple + 1;
      } else if (widget.respuestas[i] == "NO CUMPLE") {
        limpiezaNoCumple = limpiezaNoCumple + 1;
      } else {
        limpiezaNoAplica = limpiezaNoAplica + 1;
      }
    }

     if((limpiezaCumple + limpiezaNoCumple - limpiezaNoAplica) < 1){
      limpiezaPorcentaje = 0;
    }else{

    limpiezaPorcentaje = limpiezaCumple /
        (limpiezaCumple + limpiezaNoCumple - limpiezaNoAplica) *
        100;

    }



    for (var i = 13; i < 17; i++) {
      if (widget.respuestas[i] == "CUMPLE") {
        manipulacionCumple = manipulacionCumple + 1;
      } else if (widget.respuestas[i] == "NO CUMPLE") {
        manipulacionNoCumple = manipulacionNoCumple + 1;
      } else {
        manipulacionNoAplica = manipulacionNoAplica + 1;
      }
    }

     if((manipulacionCumple + manipulacionNoCumple - manipulacionNoAplica) < 1){
      manipulacionPorcentaje = 0;
    }else{

    manipulacionPorcentaje = manipulacionCumple /
        (manipulacionCumple + manipulacionNoCumple - manipulacionNoAplica) *
        100;
    }


    totalMediosBioseguridadCumple = lavadoManosCumple +
        distanciamientoFisicoCumple +
        elementosdeProteccionCumple +
        limpiezaCumple +
        manipulacionCumple +
        tapabocasCumple;

    totalMediosBioseguridadNoCumple = lavadoManosNoCumple +
        distanciamientoFisicoNoCumple +
        elementosdeProteccionNoCumple +
        limpiezaNoCumple +
        manipulacionNoCumple +
        tapabocasNoCumple;

    totalMediosBioseguridadNoAplica = lavadoManosNoAplica +
        distanciamientoFisicoNoAplica +
        elementosdeProteccionNoAplica +
        limpiezaNoAplica +
        manipulacionNoAplica +
        tapabocasNoAplica;

         if((totalMediosBioseguridadCumple +
            totalMediosBioseguridadNoCumple -
            totalMediosBioseguridadNoAplica) < 1){
      totalMediosBioseguridadPorcentaje = 0;
    }else{

    totalMediosBioseguridadPorcentaje = totalMediosBioseguridadCumple /
        (totalMediosBioseguridadCumple +
            totalMediosBioseguridadNoCumple -
            totalMediosBioseguridadNoAplica) *
        100;

    }


    // PREVENCIÓN Y MANEJO DE SITUACIONES DE RIESGO DE CONTAGIO

    for (var i = 17; i < 38; i++) {
      if (widget.respuestas[i] == "CUMPLE") {
        vigilanciaCumple = vigilanciaCumple + 1;
      } else if (widget.respuestas[i] == "NO CUMPLE") {
        vigilanciaNoCumple = vigilanciaNoCumple + 1;
      } else {
        vigilanciaNoAplica = vigilanciaNoAplica + 1;
      }
    }

      if((vigilanciaCumple + vigilanciaNoCumple - vigilanciaNoAplica) < 1){
      vigilanciaPorcentaje = 0;
    }else{

    vigilanciaPorcentaje = vigilanciaCumple /
        (vigilanciaCumple + vigilanciaNoCumple - vigilanciaNoAplica) *
        100;

    }


    for (var i = 38; i < 39; i++) {
      if (widget.respuestas[i] == "CUMPLE") {
        recomendacionesCumple = recomendacionesCumple + 1;
      } else if (widget.respuestas[i] == "NO CUMPLE") {
        recomendacionesNoCumple = recomendacionesNoCumple + 1;
      } else {
        recomendacionesNoAplica = recomendacionesNoAplica + 1;
      }
    }

    if((recomendacionesCumple +
            recomendacionesNoCumple -
            recomendacionesNoAplica) < 1){
      recomendacionesPorcentaje = 0;
    }else{

    recomendacionesPorcentaje = recomendacionesCumple /
        (recomendacionesCumple +
            recomendacionesNoCumple -
            recomendacionesNoAplica) *
        100;

    }

    for (var i = 39; i < 40; i++) {
      if (widget.respuestas[i] == "CUMPLE") {
        convivenciaCumple = convivenciaCumple + 1;
      } else if (widget.respuestas[i] == "NO CUMPLE") {
        convivenciaNoCumple = convivenciaNoCumple + 1;
      } else {
        convivenciaNoAplica = convivenciaNoAplica + 1;
      }
    }

     if((convivenciaCumple + convivenciaNoCumple - convivenciaNoAplica) < 1){
      convivenciaPorcentaje = 0;
    }else{

    convivenciaPorcentaje = convivenciaCumple /
        (convivenciaCumple + convivenciaNoCumple - convivenciaNoAplica) *
        100;
    }


    totalPrevencionCumple =
        convivenciaCumple + recomendacionesCumple + vigilanciaCumple;

    totalPrevencionNoCumple =
        convivenciaNoCumple + recomendacionesNoCumple + vigilanciaNoCumple;

    totalPrevencionNoAplica =
        convivenciaNoAplica + recomendacionesNoAplica + vigilanciaNoAplica;

         if((totalPrevencionCumple +
            totalPrevencionNoCumple -
            totalPrevencionNoAplica) < 1){
      totalPrevencionPorcentaje = 0;
    }else{

    totalPrevencionPorcentaje = totalPrevencionCumple /
        (totalPrevencionCumple +
            totalPrevencionNoCumple -
            totalPrevencionNoAplica) *
        100;
    }

    //MANEJO - PASOS A ASEGUIR EN CASO DE CONTAGIO - COMUNICACIONES

    for (var i = 40; i < 42; i++) {
      if (widget.respuestas[i] == "CUMPLE") {
        prevencionCumple = prevencionCumple + 1;
      } else if (widget.respuestas[i] == "NO CUMPLE") {
        prevencionNoCumple = prevencionNoCumple + 1;
      } else {
        prevencionNoAplica = prevencionNoAplica + 1;
      }
    }

       if((prevencionCumple + prevencionNoCumple - prevencionNoAplica) < 1){
      prevencionPorcentaje = 0;
    }else{

    prevencionPorcentaje = prevencionCumple /
        (prevencionCumple + prevencionNoCumple - prevencionNoAplica) *
        100;

    }

    for (var i = 42; i < 43; i++) {
      if (widget.respuestas[i] == "CUMPLE") {
        pasosCumple = pasosCumple + 1;
      } else if (widget.respuestas[i] == "NO CUMPLE") {
        pasosNoCumple = pasosNoCumple + 1;
      } else {
        pasosNoAplica = pasosNoAplica + 1;
      }
    }

    if((pasosCumple + pasosNoCumple - pasosNoAplica) < 1){
      pasosPorcentaje = 0;
    }else{

    pasosPorcentaje =
        pasosCumple / (pasosCumple + pasosNoCumple - pasosNoAplica) * 100;

    }
        

    for (var i = 43; i < 45; i++) {
      if (widget.respuestas[i] == "CUMPLE") {
        planCumple = planCumple + 1;
      } else if (widget.respuestas[i] == "NO CUMPLE") {
        planNoCumple = planNoCumple + 1;
      } else {
        planNoAplica = planNoAplica + 1;
      }
    }

     if((planCumple + planNoCumple - planNoAplica) < 1){
      planPorcentaje = 0;
    }else{

    planPorcentaje =
        planCumple / (planCumple + planNoCumple - planNoAplica) * 100;

    }

    totalManejoCumple = prevencionCumple + pasosCumple + planCumple;

    totalManejoNoCumple = prevencionNoCumple + pasosNoCumple + planNoCumple;

    totalManejoNoAplica = prevencionNoAplica + pasosNoAplica + planNoAplica;

    if((totalManejoCumple + totalManejoNoCumple - totalManejoNoAplica) < 1){
      totalManejoPorcentaje = 0;
    }else{

    totalManejoPorcentaje = totalManejoCumple /
        (totalManejoCumple + totalManejoNoCumple - totalManejoNoAplica) *
        100;

    }

    //COMERCIALES Y COMPLEMENTARIOS

    for (var i = 45; i < 48; i++) {
      if (widget.respuestas[i] == "CUMPLE") {
        transitoCumple = transitoCumple + 1;
      } else if (widget.respuestas[i] == "NO CUMPLE") {
        transitoNoCumple = transitoNoCumple + 1;
      } else {
        transitoNoAplica = transitoNoAplica + 1;
      }
    }

    if((transitoCumple + transitoNoCumple - transitoNoAplica) < 1){
      transitoPorcentaje = 0;
    }else{

    transitoPorcentaje = transitoCumple /
        (transitoCumple + transitoNoCumple - transitoNoAplica) *
        100;

    }


    for (var i = 48; i < 51; i++) {
      if (widget.respuestas[i] == "CUMPLE") {
        gobiernoCumple = gobiernoCumple + 1;
      } else if (widget.respuestas[i] == "NO CUMPLE") {
        gobiernoNoCumple = gobiernoNoCumple + 1;
      } else {
        gobiernoNoAplica = gobiernoNoAplica + 1;
      }
    }

    for (var i = 52; i < 54; i++) {
      if (widget.respuestas[i] == "CUMPLE") {
        gobiernoCumple = gobiernoCumple + 1;
      } else if (widget.respuestas[i] == "NO CUMPLE") {
        gobiernoNoCumple = gobiernoNoCumple + 1;
      } else {
        gobiernoNoAplica = gobiernoNoAplica + 1;
      }
    }

    if((gobiernoCumple + gobiernoNoCumple - gobiernoNoAplica) < 1){
      gobiernoPorcentaje = 0;
    }else{

    gobiernoPorcentaje = gobiernoCumple /
        (gobiernoCumple + gobiernoNoCumple - gobiernoNoAplica) *
        100;

    }



    for (var i = 51; i < 52; i++) {
      if (widget.respuestas[i] == "CUMPLE") {
        saludCumple = saludCumple + 1;
      } else if (widget.respuestas[i] == "NO CUMPLE") {
        saludNoCumple = saludNoCumple + 1;
      } else {
        saludNoAplica = saludNoAplica + 1;
      }
    }

     if((saludCumple + saludNoCumple - saludNoAplica) < 1){
      saludPorcentaje = 0;
    }else{

    saludPorcentaje =
        saludCumple / (saludCumple + saludNoCumple - saludNoAplica) * 100;

          
    }


    for (var i = 54; i < 55; i++) {
      if (widget.respuestas[i] == "CUMPLE") {
        planeacionCumple = planeacionCumple + 1;
      } else if (widget.respuestas[i] == "NO CUMPLE") {
        planeacionNoCumple = planeacionNoCumple + 1;
      } else {
        planeacionNoAplica = planeacionNoAplica + 1;
      }
    }

     if((planeacionCumple + planeacionNoCumple - planeacionNoAplica) < 1){
      planeacionPorcentaje = 0;
    }else{

    planeacionPorcentaje = planeacionCumple /
        (planeacionCumple + planeacionNoCumple - planeacionNoAplica) *
        100;
    }

    totalComercialesCumple =
        transitoCumple + gobiernoCumple + saludCumple + planeacionCumple;

    totalComercialesNoCumple = transitoNoCumple +
        gobiernoNoCumple +
        saludNoCumple +
        planeacionNoCumple;

    totalComercialesNoAplica = transitoNoAplica +
        gobiernoNoAplica +
        saludNoAplica +
        planeacionNoAplica;

         if((totalComercialesCumple +
            totalComercialesNoCumple -
            totalComercialesNoAplica) < 1){
      totalComercialesPorcentaje = 0;
    }else{

    totalComercialesPorcentaje = totalComercialesCumple /
        (totalComercialesCumple +
            totalComercialesNoCumple -
            totalComercialesNoAplica) *
        100;

    }

    // GENERAL

    totalGeneralCumple = totalMediosBioseguridadCumple +
        totalPrevencionCumple +
        prevencionCumple +
        pasosCumple +
        planCumple;
        //totalComercialesCumple;



    totalGeneralNoCumple = totalMediosBioseguridadNoCumple +
        totalPrevencionNoCumple +
        prevencionNoCumple +
        pasosNoCumple +
        planNoCumple;
        //totalComercialesNoCumple;



    totalGeneralNoAplica = totalMediosBioseguridadNoAplica +
        totalPrevencionNoAplica +
        prevencionNoAplica +
        pasosNoAplica +
        planNoAplica;
        //totalComercialesNoAplica;

  
if((totalGeneralCumple +
            totalGeneralNoCumple -
            totalGeneralNoAplica) < 1){
      totalGeneralPorcentaje = 0;
    }else{

    totalGeneralPorcentaje = totalGeneralCumple /
        (totalGeneralCumple + totalGeneralNoCumple - totalGeneralNoAplica) *
        100;
    }
        
  }

  _generateData() {
    var general1 = [
      new General(
          'MEDIDAS DE BIOSEGURIDAD', totalMediosBioseguridadPorcentaje.round()),
      new General('PREVENCIÓN Y MANEJO DE SITUACIONES DE RIESGO DE CONTAGIO',
          totalPrevencionPorcentaje.round()),
      new General(
          'MONITOREO DE SÍNTOMAS DE CONTAGIO', prevencionPorcentaje.round()),
      new General('PASOS A SEGUIR AL PRESENTAR UNA PERSONA CON COVID-19',
          pasosPorcentaje.round()),
      new General('PLAN DE COMUNICACIONES', planPorcentaje.round()),
      new General(
          'COMERCIALES Y COMPLEMENTARIOS', totalComercialesPorcentaje.round()),
      new General('TOTAL', totalGeneralPorcentaje.round()),
    ];

    var bioseguridad1 = [
      new General(
          'Distanciamiento físico', distanciamientoFisicoPorcentaje.round()),
      new General('Elementos de protección personal',
          elementosdeProteccionPorcentaje.round()),
      new General('Lavado de manos', lavadoManosPorcentaje.round()),
      new General('Limpieza y desinfección', limpiezaPorcentaje.round()),
      new General('Manipulación de insumos y productos',
          manipulacionPorcentaje.round()),
      new General('Tapabocas', tapabocasPorcentaje.round()),
      new General('TOTAL', totalMediosBioseguridadPorcentaje.round()),
    ];

    var prevencion1 = [
      new General('Convivencia con una persona de alto riesgo',
          convivenciaPorcentaje.round()),
      new General(
          'Recomendaciones en la vivienda', recomendacionesPorcentaje.round()),
      new General('Vigilancia de salud de los trabajadores en el SG SST',
          vigilanciaPorcentaje.round()),
      new General('TOTAL', totalPrevencionPorcentaje.round()),
    ];

    var manejo1 = [
      new General('Prevención y Manejo de situaciones de riesgo de contagio',
          prevencionPorcentaje.round()),
      new General('Pasos a seguir en caso de síntomas COVID-19',
          pasosPorcentaje.round()),
      new General('Plan de Comunicaciones', planPorcentaje.round()),
      new General('TOTAL', totalManejoPorcentaje.round()),
    ];

    var comerciales1 = [
      new General('TRÁNSITO', transitoPorcentaje.round()),
      new General('GOBIERNO', gobiernoPorcentaje.round()),
      new General('SALUD', saludPorcentaje.round()),
      new General('PLANEACIÓN', planeacionPorcentaje.round()),
      new General('TOTAL', totalComercialesPorcentaje.round()),
    ];

    _generalData.add(
      charts.Series(
          domainFn: (General general, _) => general.grupo,
          measureFn: (General general, _) => general.porcentaje,
          id: '2017',
          data: general1,
          //fillPatternFn: (_, __) => charts.FillPatternType.solid,
          fillColorFn: (General general, _) =>
              charts.ColorUtil.fromDartColor(Color(0xff990099)),
          labelAccessorFn: (General general, _) =>
              '${general.grupo}: ${general.porcentaje}\%'),
    );

    _bioseguridadData.add(
      charts.Series(
          domainFn: (General general, _) => general.grupo,
          measureFn: (General general, _) => general.porcentaje,
          id: '2017',
          data: bioseguridad1,
          //fillPatternFn: (_, __) => charts.FillPatternType.solid,
          fillColorFn: (General general, _) =>
              charts.ColorUtil.fromDartColor(Color(0xff990099)),
          labelAccessorFn: (General general, _) =>
              '${general.grupo}: ${general.porcentaje}\%'),
    );

    _prevencionData.add(charts.Series(
      domainFn: (General general, _) => general.grupo,
      measureFn: (General general, _) => general.porcentaje,
      id: '2017',
      data: prevencion1,
      //fillPatternFn: (_, __) => charts.FillPatternType.solid,
          fillColorFn: (General general, _) =>
              charts.ColorUtil.fromDartColor(Color(0xff990099)),
          labelAccessorFn: (General general, _) =>
              '${general.grupo}: ${general.porcentaje}\%'),
    );

    _manejoData.add(
      charts.Series(
          domainFn: (General general, _) => general.grupo,
          measureFn: (General general, _) => general.porcentaje,
          id: '2017',
          data: manejo1,
          //fillPatternFn: (_, __) => charts.FillPatternType.solid,
          fillColorFn: (General general, _) =>
              charts.ColorUtil.fromDartColor(Color(0xff990099)),
          labelAccessorFn: (General general, _) =>
              '${general.grupo}: ${general.porcentaje}\%'),
    );

    _comercialesData.add(
      charts.Series(
          domainFn: (General general, _) => general.grupo,
          measureFn: (General general, _) => general.porcentaje,
          id: '2017',
          data: comerciales1,
          //fillPatternFn: (_, __) => charts.FillPatternType.solid,
          fillColorFn: (General general, _) =>
              charts.ColorUtil.fromDartColor(Color(0xff990099)),
          labelAccessorFn: (General general, _) =>
              '${general.grupo}: ${general.porcentaje}\%'),
    );
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _procesarDatos();
    _generalData = List<charts.Series<General, String>>();
    _bioseguridadData = List<charts.Series<General, String>>();
    _prevencionData = List<charts.Series<General, String>>();
    _manejoData = List<charts.Series<General, String>>();
    _comercialesData = List<charts.Series<General, String>>();
    _generateData();
    _tabController = TabController(vsync: this, length: 5);
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
        onWillPop: _onBackPressed,
        child: Scaffold(
      body: SafeArea(
        child: DefaultTabController(
          length: 5,
          child: Column(
            children: <Widget>[
              Container(
                constraints: BoxConstraints(maxHeight: 150.0),
                child: Material(
                  color: Colors.indigo,
                  child: TabBar(
                    controller: _tabController,
                    tabs: [
                      Tab(icon: Icon(FontAwesomeIcons.solidChartBar)),
                      Tab(icon: Icon(FontAwesomeIcons.solidChartBar)),
                      Tab(icon: Icon(FontAwesomeIcons.solidChartBar)),
                      Tab(icon: Icon(FontAwesomeIcons.solidChartBar)),
                      Tab(icon: Icon(FontAwesomeIcons.solidChartBar)),
                    ],
                  ),
                ),
              ),
              Expanded(
                child: TabBarView(
                  controller: _tabController,
                  children: [
                    Padding(
                      padding: EdgeInsets.all(8.0),
                      child: Container(
                        child: Center(
                          child: Column(
                            children: <Widget>[
                              Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                children: <Widget>[
                                  RaisedButton(
                                    color: Color.fromRGBO(193, 29, 27, 1),
                                    onPressed: () {
                                      Navigator.of(context).pushAndRemoveUntil(
                                          MaterialPageRoute(
                                              builder: (BuildContext context) =>
                                                  LoginPage()),
                                          (Route<dynamic> route) => false);
                                    },
                                    child: RichText(
                                      textAlign: TextAlign.center,
                                      text: TextSpan(
                                        text: "Volver",
                                        style: TextStyle(
                                            color: Color.fromRGBO(
                                                255, 255, 255, 1),
                                            fontSize: 18),
                                      ),
                                    ),
                                  ),
                                  Text(
                                    'CUMPLIMIENTO EMPRESA',
                                    style: TextStyle(
                                        fontSize: 24.0,
                                        fontWeight: FontWeight.bold),
                                  ),
                                  RaisedButton(
                                    color: Color.fromRGBO(193, 29, 27, 1),
                                    onPressed: () {
                                      _toggleTab();
                                    },
                                    child: RichText(
                                      textAlign: TextAlign.center,
                                      text: TextSpan(
                                        text: "Siguiente",
                                        style: TextStyle(
                                            color: Color.fromRGBO(
                                                255, 255, 255, 1),
                                            fontSize: 18),
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                              Expanded(
                                child: charts.BarChart(
                                  _generalData,
                                  animate: true,
                                  barGroupingType:
                                      charts.BarGroupingType.grouped,
                                  vertical: false,
                                  //behaviors: [new charts.SeriesLegend()],
                                  animationDuration: Duration(seconds: 2),
                                  barRendererDecorator:
                                      new charts.BarLabelDecorator<String>(),
                                  // Hide domain axis.
                                  domainAxis: new charts.OrdinalAxisSpec(
                                      renderSpec: new charts.NoneRenderSpec()),
                                ),
                              ),
                            ],
                          ),
                        ),
                      ),
                    ),
                    Padding(
                      padding: EdgeInsets.all(8.0),
                      child: Container(
                        child: Center(
                          child: Column(
                            children: <Widget>[
                              Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                children: <Widget>[
                              RaisedButton(
                                    color: Color.fromRGBO(193, 29, 27, 1),
                                    onPressed: () {
                                      _toggleTab();
                                    },
                                    child: RichText(
                                      textAlign: TextAlign.center,
                                      text: TextSpan(
                                        text: "Volver",
                                        style: TextStyle(
                                            color: Color.fromRGBO(
                                                255, 255, 255, 1),
                                            fontSize: 18),
                                      ),
                                    ),
                                  ),
                              Text(
                                'MEDIDAS DE BIOSEGURIDAD',
                                style: TextStyle(
                                    fontSize: 24.0,
                                    fontWeight: FontWeight.bold),
                              ),
                              RaisedButton(
                                    color: Color.fromRGBO(193, 29, 27, 1),
                                    onPressed: () {
                                      _toggleTab();
                                    },
                                    child: RichText(
                                      textAlign: TextAlign.center,
                                      text: TextSpan(
                                        text: "Siguiente",
                                        style: TextStyle(
                                            color: Color.fromRGBO(
                                                255, 255, 255, 1),
                                            fontSize: 18),
                                      ),
                                    ),
                                  ),
                                  ],
                              ),
                              Expanded(
                                child: charts.BarChart(
                                  _bioseguridadData,
                                  animate: true,
                                  barGroupingType:
                                      charts.BarGroupingType.grouped,
                                  vertical: false,
                                  //behaviors: [new charts.SeriesLegend()],
                                  animationDuration: Duration(seconds: 2),
                                  barRendererDecorator:
                                      new charts.BarLabelDecorator<String>(),
                                  // Hide domain axis.
                                  domainAxis: new charts.OrdinalAxisSpec(
                                      renderSpec: new charts.NoneRenderSpec()),
                                ),
                              ),
                            ],
                          ),
                        ),
                      ),
                    ),
                    Padding(
                      padding: EdgeInsets.all(8.0),
                      child: Container(
                        child: Center(
                          child: Column(
                            children: <Widget>[
                              Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                children: <Widget>[
                              RaisedButton(
                                    color: Color.fromRGBO(193, 29, 27, 1),
                                    onPressed: () {
                                      _toggleTab();
                                    },
                                    child: RichText(
                                      textAlign: TextAlign.center,
                                      text: TextSpan(
                                        text: "Volver",
                                        style: TextStyle(
                                            color: Color.fromRGBO(
                                                255, 255, 255, 1),
                                            fontSize: 18),
                                      ),
                                    ),
                                  ),
                              Text(
                                'PREVENCION Y MANEJO DE SITUACIONES',
                                style: TextStyle(
                                    fontSize: 22.0,
                                    fontWeight: FontWeight.bold),
                              ),
                              RaisedButton(
                                    color: Color.fromRGBO(193, 29, 27, 1),
                                    onPressed: () {
                                      _toggleTab();
                                    },
                                    child: RichText(
                                      textAlign: TextAlign.center,
                                      text: TextSpan(
                                        text: "Siguiente",
                                        style: TextStyle(
                                            color: Color.fromRGBO(
                                                255, 255, 255, 1),
                                            fontSize: 18),
                                      ),
                                    ),
                                  ),
                                  ],
                              ),
                              Expanded(
                                child: charts.BarChart(
                                  _prevencionData,
                                  animate: true,
                                  barGroupingType:
                                      charts.BarGroupingType.grouped,
                                  vertical: false,
                                  //behaviors: [new charts.SeriesLegend()],
                                  animationDuration: Duration(seconds: 2),
                                  barRendererDecorator:
                                      new charts.BarLabelDecorator<String>(),
                                  // Hide domain axis.
                                  domainAxis: new charts.OrdinalAxisSpec(
                                      renderSpec: new charts.NoneRenderSpec()),
                                ),
                              ),
                            ],
                          ),
                        ),
                      ),
                    ),
                    Padding(
                      padding: EdgeInsets.all(8.0),
                      child: Container(
                        child: Center(
                          child: Column(
                            children: <Widget>[
                              Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                children: <Widget>[
                              RaisedButton(
                                    color: Color.fromRGBO(193, 29, 27, 1),
                                    onPressed: () {
                                      _toggleTab();
                                    },
                                    child: RichText(
                                      textAlign: TextAlign.center,
                                      text: TextSpan(
                                        text: "Volver",
                                        style: TextStyle(
                                            color: Color.fromRGBO(
                                                255, 255, 255, 1),
                                            fontSize: 18),
                                      ),
                                    ),
                                  ),
                              Text(
                                'MANEJO - PASOS A SEGUIR - COMUNICACIONES',
                                style: TextStyle(
                                    fontSize: 20.0,
                                    fontWeight: FontWeight.bold),
                              ),
                              RaisedButton(
                                    color: Color.fromRGBO(193, 29, 27, 1),
                                    onPressed: () {
                                      _toggleTab();
                                    },
                                    child: RichText(
                                      textAlign: TextAlign.center,
                                      text: TextSpan(
                                        text: "Siguiente",
                                        style: TextStyle(
                                            color: Color.fromRGBO(
                                                255, 255, 255, 1),
                                            fontSize: 18),
                                      ),
                                    ),
                                  ),
                                  ],
                              ),
                              Expanded(
                                child: charts.BarChart(
                                  _manejoData,
                                  animate: true,
                                  barGroupingType:
                                      charts.BarGroupingType.grouped,
                                  vertical: false,
                                  //behaviors: [new charts.SeriesLegend()],
                                  animationDuration: Duration(seconds: 2),
                                  barRendererDecorator:
                                      new charts.BarLabelDecorator<String>(),
                                  // Hide domain axis.
                                  domainAxis: new charts.OrdinalAxisSpec(
                                      renderSpec: new charts.NoneRenderSpec()),
                                ),
                              ),
                            ],
                          ),
                        ),
                      ),
                    ),
                    Padding(
                      padding: EdgeInsets.all(8.0),
                      child: Container(
                        child: Center(
                          child: Column(
                            children: <Widget>[
                              Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                children: <Widget>[
                              RaisedButton(
                                    color: Color.fromRGBO(193, 29, 27, 1),
                                    onPressed: () {
                                      _toggleTabPrev();
                                    },
                                    child: RichText(
                                      textAlign: TextAlign.center,
                                      text: TextSpan(
                                        text: "Volver",
                                        style: TextStyle(
                                            color: Color.fromRGBO(
                                                255, 255, 255, 1),
                                            fontSize: 18),
                                      ),
                                    ),
                                  ),
                              Text(
                                'COMERCIALES Y COMPLEMENTARIOS',
                                style: TextStyle(
                                    fontSize: 24.0,
                                    fontWeight: FontWeight.bold),
                              ),
                              RaisedButton(
                                    color: Color.fromRGBO(193, 29, 27, 1),
                                    onPressed: () {
                                      Navigator.of(context).pushAndRemoveUntil(
                                          MaterialPageRoute(
                                              builder: (BuildContext context) =>
                                                  LoginPage()),
                                          (Route<dynamic> route) => false);
                                    },
                                    child: RichText(
                                      textAlign: TextAlign.center,
                                      text: TextSpan(
                                        text: "Salir",
                                        style: TextStyle(
                                            color: Color.fromRGBO(
                                                255, 255, 255, 1),
                                            fontSize: 18),
                                      ),
                                    ),
                                  ),
                              ],
                              ),
                              Expanded(
                                child: charts.BarChart(
                                  _comercialesData,
                                  animate: true,
                                  barGroupingType:
                                      charts.BarGroupingType.grouped,
                                  vertical: false,
                                  //behaviors: [new charts.SeriesLegend()],
                                  animationDuration: Duration(seconds: 2),
                                  barRendererDecorator:
                                      new charts.BarLabelDecorator<String>(),
                                  // Hide domain axis.
                                  domainAxis: new charts.OrdinalAxisSpec(
                                      renderSpec: new charts.NoneRenderSpec()),
                                ),
                              ),
                            ],
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    ));
  }

  Future<bool> _onBackPressed() {
    return showDialog(
          context: context,
          builder: (context) => new AlertDialog(
            title: new Text('Salir de la aplicación'),
            content: new Text('¿Quiere salir de la aplicación?'),
            actions: <Widget>[
              new FlatButton(
                child: new Text("Si"),
                onPressed: () {
                  Navigator.of(context).pop(true);
                },
              ),
              new FlatButton(
                child: new Text("No"),
                onPressed: () {
                  Navigator.of(context).pop(false);
                },
              ),
              SizedBox(height: 16),
            ],
          ),
        ) ??
        false;
  }
}

class General {
  String grupo;

  int porcentaje;

  General(this.grupo, this.porcentaje);
}
