
import 'package:data_connection_checker/data_connection_checker.dart';
import 'package:encuesta/db/database_SaludMental.dart';
import 'package:flutter/material.dart';
import 'dart:convert';
import 'buscarEmpresa.dart';
import 'package:http/http.dart' as http;
import './db/database_helpers.dart';
import './db/database_EncuestasCiudadanos.dart';
import './db/database_Encuestas.dart';
import './db/database_Respuestas.dart';
import './db/database_Usuarios.dart';
import 'db/database_Barrios.dart';
import 'db/database_Callejon.dart';
import 'db/database_Corregimientos.dart';
import 'db/database_Departamentos.dart';
import 'db/database_DificultadEps.dart';
import 'db/database_Eps.dart';
import 'db/database_Etnias.dart';
import 'db/database_Indenfiticacion.dart';
import 'db/database_Municipios.dart';
import 'db/database_Ocupaciones.dart';
import 'db/database_Poblacional.dart';
import 'db/database_PreguntasEmpresas.dart';
import 'db/database_Sexo.dart';
import 'db/database_Trabajos.dart';
import 'db/database_Veredas.dart';
import 'db/database_Vulnerables.dart';
import 'encuestaCiudadano.dart';
import 'package:mime/mime.dart';
import 'package:http_parser/http_parser.dart';
import 'package:geolocator/geolocator.dart';

final _usernameController = TextEditingController();
final _passwordController = TextEditingController();

final String url = "https://gobernante.sigelo.com.co";



class LoginPage extends StatefulWidget {
  LoginPage({Key key, this.title}) : super(key: key);
  final String title;

  @override
  _LoginPageState createState() => _LoginPageState();
}

class RespuestaList {
  int idPreguntaNum;
  String respuesta;
  String observaciones;

  RespuestaList(idPreguntaNum, respuesta, observaciones) {
    this.idPreguntaNum = idPreguntaNum;
    this.respuesta = respuesta;
    this.observaciones = observaciones;
  }
}

class _LoginPageState extends State<LoginPage> {

  double lat = 0;
  double lon = 0;

    final Geolocator geolocator = Geolocator()..forceAndroidLocationManager;


  List<RespuestaList> listaDeRespuestas = [];
  bool cargando = false;
  String errorConec = "version: 22.12";

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
        onWillPop: _onBackPressed,
        child: Scaffold(
      body: SafeArea(
          child: Container(
        color: Color.fromRGBO(255, 255, 255, 1),
        child: ListView(
          children: <Widget>[            
            widgetTituloMobile(),
            widgetNombreUsuarioMobile(),
            wigdetContrasenaMobile(),
            wigdetBtnLoginMobile(),
            widgetversion(),
          ],
        ),
      )),
    ));
  }

  widgetversion() {
    return Container(
        child: RichText(
          textAlign: TextAlign.end,
          text: TextSpan(
            text: errorConec,
            style:
                TextStyle(color: Color.fromRGBO(0, 0, 0, 1), fontSize: 10),
          ),
        ));
  }

  

  widgetTituloMobile() {
    return Container(
        margin: const EdgeInsets.only(top: 10.0, bottom: 20.0),
        child: RichText(
          textAlign: TextAlign.center,
          text: TextSpan(
            text: "Iniciar Sesión",
            style:
                TextStyle(color: Color.fromRGBO(193, 29, 27, 1), fontSize: 50),
          ),
        ));
  }

  widgetNombreUsuarioMobile() {
    return Container(
      padding: const EdgeInsets.only(left: 20.0, right: 20.0, bottom: 50.0),
      child: TextFormField(
        controller: _usernameController,
        decoration: const InputDecoration(
          hintText: 'Usuario',
          border: const OutlineInputBorder(),
          filled: true,
          fillColor: Colors.white,
        ),
      ),
    );
  }

  wigdetContrasenaMobile() {
    return Container(
      padding: const EdgeInsets.only(left: 20.0, right: 20.0, bottom: 20.0),
      child: TextFormField(
        controller: _passwordController,
        obscureText: true,
        decoration: const InputDecoration(
          hintText: 'Contraseña',
          border: const OutlineInputBorder(),
          filled: true,
          fillColor: Colors.white,
        ),
      ),
    );
  }

  wigdetBtnLoginMobile() {
    return Container(
        padding: const EdgeInsets.only(left: 20.0, right: 20.0),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            RaisedButton(
              onPressed: () {
                if (_usernameController.text == "") {
                  _campoVacio();
                } else {
                  ingresarSinConexion(_usernameController.text);
                }
              },
              child: RichText(
                textAlign: TextAlign.center,
                text: TextSpan(
                  text: "Encuesta Empresas",
                  style: TextStyle(
                      color: Color.fromRGBO(193, 29, 27, 1), fontSize: 18),
                ),
              ),
            ),
            RaisedButton(
              onPressed: () {
                if (_usernameController.text == "") {
                  _campoVacio();
                } else {
                  ingresarEncuestaCiudadano(_usernameController.text);
                }
              },
              child: RichText(
                textAlign: TextAlign.center,
                text: TextSpan(
                  text: "Encuesta Ciudadano",
                  style: TextStyle(
                      color: Color.fromRGBO(193, 29, 27, 1), fontSize: 18),
                ),
              ),
            ),
            if (cargando == true)
              Container(
                  height: 20,
                  child: CircularProgressIndicator(
                    value: null,
                    strokeWidth: 5.0,
                  )),
            RaisedButton(
              onPressed: () {
                if (_usernameController.text == "" ||
                    _passwordController.text == "") {
                  _campoVacio();
                } else {
                  setState(() {
                    cargando = true;
                  });
                  sincronizar(
                      _usernameController.text, _passwordController.text);
                }
              },
              child: RichText(
                textAlign: TextAlign.center,
                text: TextSpan(
                  text: "Sincronizar base de datos",
                  style: TextStyle(
                      color: Color.fromRGBO(193, 29, 27, 1), fontSize: 18),
                ),
              ),
            ),
           
          ],
        ));
  }

  Future<void> _campoVacio() async {
    return showDialog<void>(
      context: context,
      barrierDismissible: false, // user must tap button!
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text('Hay un campo vacío'),
          content: SingleChildScrollView(
            child: ListBody(
              children: <Widget>[
                Text('Por favor rellene todos los campos'),
              ],
            ),
          ),
          actions: <Widget>[
            FlatButton(
              child: Text('Ok'),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
          ],
        );
      },
    );
  }

  ingresarSinConexion(username) async {
    DatabaseUsuarios helper = DatabaseUsuarios.instance;
    Usuario usuario = await helper.queryUsuario(username);
    if (usuario != null) {
      Navigator.of(context).pushAndRemoveUntil(
          MaterialPageRoute(
              builder: (BuildContext context) =>
                  BuscarEmpresaPage(idUsuario: _usernameController.text)),
          (Route<dynamic> route) => false);
    } else {
      usuarioNoexiste();
    }
  }

  ingresarEncuestaCiudadano(username) async {
    DatabaseUsuarios helper = DatabaseUsuarios.instance;
    Usuario usuario = await helper.queryUsuario(username);
    if (usuario != null) {
      Navigator.of(context).pushAndRemoveUntil(
          MaterialPageRoute(
              builder: (BuildContext context) => EncuestaCiudadanoPage()),
          (Route<dynamic> route) => false);
    } else {
      usuarioNoexiste();
    }
  }

  sincronizar(usuario, pass) async {
    var token = "";
    Map data = {'username': usuario, 'password': pass};

    bool result = await DataConnectionChecker().hasConnection;
    if (result == false) {
      sinInternet();
      setState(() {
        cargando = false;
      });
    } else {
      var response = await http.post(url + '/api/1.0/login/',
          body: data);

      var jsonResponse = json.decode(response.body);
      print(response.statusCode);
      print(response.body);
      

      setState(() {
              errorConec = response.statusCode.toString();
      });


      if (response.statusCode == 200) {
        token = jsonResponse['token'];

        _updateUsuario(usuario, token);
      } else if (response.statusCode == 401 || response.statusCode == 404) {
        usuarioNoexiste();
        setState(() {
          cargando = false;
        });
      }

      DatabaseEncuesta helper = DatabaseEncuesta.instance;

      int contarEncuestas = await helper.getCount();
      if (contarEncuestas == 0) {
       // print(contarEncuestas.toString() + " encuestas");
        obtenerInformacion(token);
      } else {
       // print(contarEncuestas.toString() + " encuestas");
        obtenerInformacion(token);
        int cantidadSubidas = 0;
        for (var j = 0; j <= 1000; j++) {
          Encuesta encuestahecha = await helper.queryEncuesta((j));
          print(encuestahecha);
          if (encuestahecha != null) {
            print(j);
            for (var i = 1; i < 55; i++) {
              DatabaseRespuestas helperBuscar = DatabaseRespuestas.instance;
              Respuesta resp = await helperBuscar.queryRespuestas(
                  (encuestahecha.idEmpresa), (i));

                  print("Aqui");
                  print(encuestahecha.idEmpresa);
                  print(i);
                  


              listaDeRespuestas.add(RespuestaList(
                  resp.idPregunta, resp.respuestas, resp.observaciones));
            }


            Map jsonEncuesta = {
              "encuesta": {
                "id_empresa": encuestahecha.idEmpresa,
                "responsable_sst": encuestahecha.responsablesst,
                "identificacion_sst": encuestahecha.identificacionsst,
                "encuestado": encuestahecha.encuestado,
                "identificacion_encuestado":
                    encuestahecha.identificacionencuestado,
                "normatividad": encuestahecha.normatividad,
                "descripcion_normatividad":
                    encuestahecha.descripcionnormatividad
              },
              'respuestas': [
                {
                  'id_pregunta': listaDeRespuestas[3].idPreguntaNum,
                  'respuesta': listaDeRespuestas[3].respuesta,
                  'observaciones': listaDeRespuestas[3].observaciones,
                },
                {
                  'id_pregunta': listaDeRespuestas[5].idPreguntaNum,
                  'respuesta': listaDeRespuestas[5].respuesta,
                  'observaciones': listaDeRespuestas[5].observaciones,
                },
                {
                  'id_pregunta': listaDeRespuestas[6].idPreguntaNum,
                  'respuesta': listaDeRespuestas[6].respuesta,
                  'observaciones': listaDeRespuestas[6].observaciones,
                },
                for (var k = 7; k < listaDeRespuestas.length; k++)
                  {
                    'id_pregunta': listaDeRespuestas[k].idPreguntaNum + 1,
                    'respuesta': listaDeRespuestas[k].respuesta,
                    'observaciones': listaDeRespuestas[k].observaciones,
                  }
              ],
            };

            final envioEncuestas = await http.post(
                url + '/api/1.0/respuestas/crear/',
                body: jsonEncode(jsonEncuesta),
                headers: {
                  'Authorization': 'Token ' + token,
                  'Content-Type': 'application/json'
                });
                print(encuestahecha.idEmpresa);
                print(envioEncuestas.body);

            if (envioEncuestas.statusCode == 201) {
              cantidadSubidas = cantidadSubidas + 1;
            }



            var uri = Uri.parse(
                url + '/api/1.0/respuesta/crear/evidencia/' +
                    encuestahecha.idEmpresa.toString() +
                    '/');
            var request = new http.MultipartRequest("POST", uri);

            request.fields['pregunta'] = "1";
            request.fields['respuesta'] = listaDeRespuestas[0].respuesta;
            request.fields['observaciones'] =
                listaDeRespuestas[0].observaciones;
            if (encuestahecha.imagen1 != "") {
              final mimeTypeData = lookupMimeType(encuestahecha.imagen1,
                  headerBytes: [0xFF, 0xD8]).split('/');
              final file = await http.MultipartFile.fromPath(
                'evidencia',
                encuestahecha.imagen1,
                contentType: MediaType(mimeTypeData[0], mimeTypeData[1]),
              );

              request.files.add(file);
            }
            request.headers['Authorization'] = 'Token ' + token;

            var streamedResponse = await request.send();
            var response = await http.Response.fromStream(streamedResponse);


            var request2 = new http.MultipartRequest("POST", uri);

            request2.fields['pregunta'] = "2";
            request2.fields['respuesta'] = listaDeRespuestas[1].respuesta;
            request2.fields['observaciones'] =
                listaDeRespuestas[1].observaciones;
            if (encuestahecha.imagen2 != "") {
              final mimeTypeData2 = lookupMimeType(encuestahecha.imagen2,
                  headerBytes: [0xFF, 0xD8]).split('/');
              final file2 = await http.MultipartFile.fromPath(
                'evidencia',
                encuestahecha.imagen2,
                contentType: MediaType(mimeTypeData2[0], mimeTypeData2[1]),
              );

              request2.files.add(file2);
            }

            request2.headers['Authorization'] = 'Token ' + token;

            var streamedResponse2 = await request2.send();
            var response2 = await http.Response.fromStream(streamedResponse2);


            var request3 = new http.MultipartRequest("POST", uri);

            request3.fields['pregunta'] = "3";
            request3.fields['respuesta'] = listaDeRespuestas[2].respuesta;
            request3.fields['observaciones'] =
                listaDeRespuestas[2].observaciones;
            if (encuestahecha.imagen3 != "") {
              final mimeTypeData3 = lookupMimeType(encuestahecha.imagen3,
                  headerBytes: [0xFF, 0xD8]).split('/');
              final file3 = await http.MultipartFile.fromPath(
                'evidencia',
                encuestahecha.imagen3,
                contentType: MediaType(mimeTypeData3[0], mimeTypeData3[1]),
              );

              request3.files.add(file3);
            }
            request3.headers['Authorization'] = 'Token ' + token;

            var streamedResponse3 = await request3.send();
            var response3 = await http.Response.fromStream(streamedResponse3);


            var request5 = new http.MultipartRequest("POST", uri);

            request5.fields['pregunta'] = "5";
            request5.fields['respuesta'] = listaDeRespuestas[4].respuesta;
            request5.fields['observaciones'] =
                listaDeRespuestas[4].observaciones;
            if (encuestahecha.imagen4 != "") {
              final mimeTypeData5 = lookupMimeType(encuestahecha.imagen4,
                  headerBytes: [0xFF, 0xD8]).split('/');
              final file5 = await http.MultipartFile.fromPath(
                'evidencia',
                encuestahecha.imagen4,
                contentType: MediaType(mimeTypeData5[0], mimeTypeData5[1]),
              );

              request5.files.add(file5);
            }
            request5.headers['Authorization'] = 'Token ' + token;

            var streamedResponse5 = await request5.send();
            var response5 = await http.Response.fromStream(streamedResponse5);


            var request8 = new http.MultipartRequest("POST", uri);

            request8.fields['pregunta'] = "8";
            request8.fields['respuesta'] = listaDeRespuestas[7].respuesta;
            request8.fields['observaciones'] =
                listaDeRespuestas[7].observaciones;
            if (encuestahecha.imagen5 != "") {
              final mimeTypeData8 = lookupMimeType(encuestahecha.imagen5,
                  headerBytes: [0xFF, 0xD8]).split('/');
              final file8 = await http.MultipartFile.fromPath(
                'evidencia',
                encuestahecha.imagen5,
                contentType: MediaType(mimeTypeData8[0], mimeTypeData8[1]),
              );

              request8.files.add(file8);
            }
            request8.headers['Authorization'] = 'Token ' + token;

            var streamedResponse8 = await request8.send();
            var response8 = await http.Response.fromStream(streamedResponse8);

          }

          listaDeRespuestas = [];
        }
      }

      DatabaseEncuestaCiudadano counter = DatabaseEncuestaCiudadano.instance;

      int contarEncuestasCiudadanos = await counter.getCountCiudadano();

      for (var i = 1; i <= 550; i++) {
        DatabaseEncuestaCiudadano helper = DatabaseEncuestaCiudadano.instance;
        EncuestaCiudadano encuestaCiudadanos =
            await helper.queryEncuestaCiudadano(i);

        if (encuestaCiudadanos != null &&
            encuestaCiudadanos.estado == "false") {
         

    if(encuestaCiudadanos.viveCandelaria == "True"){
    _getLocation(encuestaCiudadanos.direccion, true);
    }
    else{
      _getLocation(encuestaCiudadanos.municipio, false);
    }

      if(lon != 0){
        Map jsonEncuestaCiudadanos = {            
        "reportes": [{
        "nombres": encuestaCiudadanos.nombre,
        "apellidos": encuestaCiudadanos.apellidos,
        "tipo_identificacion": encuestaCiudadanos.tipoIdentificacion,
        "num_identificacion": encuestaCiudadanos.identificacion,
        "telefono": encuestaCiudadanos.telefono,
        "vive_candelaria": encuestaCiudadanos.viveCandelaria,
        "trabaja_candelaria": encuestaCiudadanos.trabajaCandelaria,
        "departamento": encuestaCiudadanos.departamento,
        "municipio": encuestaCiudadanos.municipio,
        "corregimiento": encuestaCiudadanos.corregimiento,
        "vereda": encuestaCiudadanos.vereda,
        "callejon": encuestaCiudadanos.callejon,
        "barrio": encuestaCiudadanos.barrio,
        "direccion": encuestaCiudadanos.direccion,
        "sexo": encuestaCiudadanos.sexo,
        "frecuencia_m_visita": encuestaCiudadanos.frecuenciaVisita,
        "lugar_frecuenta": encuestaCiudadanos.lugarFrecuenta,
        "motivo_visita": encuestaCiudadanos.motivoVisita,
        "tiempo_visita": encuestaCiudadanos.tiempoVisita,
        "fecha_nacimiento": encuestaCiudadanos.nacimiento,
        "grupo_etnico": encuestaCiudadanos.etnia,
        "grupo_poblacional": encuestaCiudadanos.grupoPoblacional,
        "vulnerabilidad": encuestaCiudadanos.vulnerabilidad,
        "ocupacion": encuestaCiudadanos.ocupacion,
        "tipo_trabajador": encuestaCiudadanos.tipoTrabajador,
        "empresa": encuestaCiudadanos.nombreEmpresa,
        "direccion_empresa": encuestaCiudadanos.direccionEmpresa,
        "eps": encuestaCiudadanos.eps,
        "otra_eps": encuestaCiudadanos.otraEps,
        "dificultad_eps": encuestaCiudadanos.dificultadEps,
        "diabetes": encuestaCiudadanos.diabetes,
        "enfermedad_cardiovascular": encuestaCiudadanos.enfermedadCardiovascular,
        "cancer": encuestaCiudadanos.cancer,
        "enfermedad_renal": encuestaCiudadanos.enfermedadRenal,
        "asma": encuestaCiudadanos.asma,
        "enfermedad_pulmonar": encuestaCiudadanos.enfermedadPulmonar,
        "imunusupresion": encuestaCiudadanos.imunusupresion,
        "obesidad": encuestaCiudadanos.obesidad,
        "desnutricion": encuestaCiudadanos.desnutricion,
        "trastorno_neurologico": encuestaCiudadanos.trastornoNeurologico,
        "tabaquismo": encuestaCiudadanos.tabaquismo,
        "alcoholismo": encuestaCiudadanos.alcoholismo,      
        "salud_mental": encuestaCiudadanos.saludMental,
        "tos": encuestaCiudadanos.tos,
        "dificultad_respiratoria": encuestaCiudadanos.dificultadRespiratoria,
        "dolor_garganta": encuestaCiudadanos.dolorGarganta,
        "fiebre": encuestaCiudadanos.fiebre,
        "fatiga": encuestaCiudadanos.fatiga,
        "malestar_general": encuestaCiudadanos.malestarGeneral,
        "disminucion_gusto": encuestaCiudadanos.disminucionGusto,
        "disminucion_olfato": encuestaCiudadanos.disminucionOlfato,
        "secrecion_nasal": encuestaCiudadanos.secrecionNasal,
        "fecha_inicio_sintomas": encuestaCiudadanos.fechaInicioSintomas,
        "desplazamiento_ultimos": encuestaCiudadanos.desplazamientoUltimos,
        "lugar_viaje": encuestaCiudadanos.lugarViaje,
        "contacto_posible": encuestaCiudadanos.contactoPosible,
        "lugar_contacto": encuestaCiudadanos.lugarContacto,
        "prueba_temperatura": encuestaCiudadanos.pruebaTemperatura,
        "resultado_prueba_temp": encuestaCiudadanos.resultadoPruebaTemp,
        "caso_sospechoso": encuestaCiudadanos.casoSospechoso,
        "prueba_anticuerpos": encuestaCiudadanos.pruebaAnticuerpos,
        "prueba_antigenos": encuestaCiudadanos.pruebaAntigenos,
        "prueba_molecular": encuestaCiudadanos.pruebaMolecular,
       
        "longitud": lon.toString(),
        "latitud": lat.toString()
        
        
    }]

          };
           
              print(lon);
              print(lat);
          print(encuestaCiudadanos.direccion);


          final envioEncuestasCiudadanos = await http.post(
              url + '/api/1.0/person-data/respuestas/',
              body: jsonEncode(jsonEncuestaCiudadanos),
              headers: {
                'Authorization': 'Token ' + token,
                'Content-Type': 'application/json'
              });
            
            
              

         if (envioEncuestasCiudadanos.statusCode == 201) {
            EncuestaCiudadano encuesta = EncuestaCiudadano();
        encuesta.id = encuestaCiudadanos.id;  
        encuesta.nombre= encuestaCiudadanos.nombre;
        encuesta.apellidos= encuestaCiudadanos.apellidos;
        encuesta.tipoIdentificacion= encuestaCiudadanos.tipoIdentificacion;
        encuesta.identificacion= encuestaCiudadanos.identificacion;
        encuesta.telefono= encuestaCiudadanos.telefono;
        encuesta.viveCandelaria = encuestaCiudadanos.viveCandelaria;
        encuesta.trabajaCandelaria = encuestaCiudadanos.trabajaCandelaria;
        encuesta.departamento= encuestaCiudadanos.departamento;
        encuesta.municipio= encuestaCiudadanos.municipio;
        encuesta.corregimiento= encuestaCiudadanos.corregimiento;
        encuesta.vereda = encuestaCiudadanos.vereda;
        encuesta.callejon = encuestaCiudadanos.callejon;
        encuesta.barrio = encuestaCiudadanos.barrio;
        encuesta.direccion = encuestaCiudadanos.direccion;
        encuesta.frecuenciaVisita = encuestaCiudadanos.frecuenciaVisita;
        encuesta.lugarFrecuenta = encuestaCiudadanos.lugarFrecuenta;
        encuesta.motivoVisita = encuestaCiudadanos.motivoVisita;
        encuesta.tiempoVisita = encuestaCiudadanos.tiempoVisita;
        encuesta.sexo= encuestaCiudadanos.sexo;
        encuesta.nacimiento = encuestaCiudadanos.nacimiento;
        encuesta.etnia= encuestaCiudadanos.etnia;
        encuesta.grupoPoblacional= encuestaCiudadanos.grupoPoblacional;
        encuesta.vulnerabilidad= encuestaCiudadanos.vulnerabilidad;
        encuesta.ocupacion= encuestaCiudadanos.ocupacion;
        encuesta.tipoTrabajador= encuestaCiudadanos.tipoTrabajador;
        encuesta.nombreEmpresa= encuestaCiudadanos.nombreEmpresa;
        encuesta.direccionEmpresa= encuestaCiudadanos.direccionEmpresa;
        encuesta.eps= encuestaCiudadanos.eps;
        encuesta.dificultadEps= encuestaCiudadanos.dificultadEps;
        encuesta.diabetes= encuestaCiudadanos.diabetes;
        encuesta.enfermedadCardiovascular= encuestaCiudadanos.enfermedadCardiovascular;
        encuesta.cancer= encuestaCiudadanos.cancer;
        encuesta.enfermedadRenal= encuestaCiudadanos.enfermedadRenal;
        encuesta.asma= encuestaCiudadanos.asma;
        encuesta.enfermedadPulmonar= encuestaCiudadanos.enfermedadPulmonar;
        encuesta.imunusupresion= encuestaCiudadanos.imunusupresion;
        encuesta.obesidad= encuestaCiudadanos.obesidad;
        encuesta.desnutricion= encuestaCiudadanos.desnutricion;
        encuesta.trastornoNeurologico = encuestaCiudadanos.trastornoNeurologico;
        encuesta.tabaquismo= encuestaCiudadanos.tabaquismo;
        encuesta.alcoholismo= encuestaCiudadanos.alcoholismo;
        encuesta.saludMental = encuestaCiudadanos.saludMental;
        encuesta.tos= encuestaCiudadanos.tos;
        encuesta.dificultadRespiratoria = encuestaCiudadanos.dificultadRespiratoria;
        encuesta.dolorGarganta = encuestaCiudadanos.dolorGarganta;
        encuesta.fiebre= encuestaCiudadanos.fiebre;
        encuesta.fatiga= encuestaCiudadanos.fatiga;
        encuesta.malestarGeneral= encuestaCiudadanos.malestarGeneral;
        encuesta.disminucionGusto= encuestaCiudadanos.disminucionGusto;
        encuesta.disminucionOlfato= encuestaCiudadanos.disminucionOlfato;
        encuesta.secrecionNasal = encuestaCiudadanos.secrecionNasal;
        encuesta.fechaInicioSintomas= encuestaCiudadanos.fechaInicioSintomas;
        encuesta.desplazamientoUltimos= encuestaCiudadanos.desplazamientoUltimos;
        encuesta.lugarViaje= encuestaCiudadanos.lugarViaje;
        encuesta.contactoPosible= encuestaCiudadanos.contactoPosible;
        encuesta.lugarContacto= encuestaCiudadanos.lugarContacto;
        encuesta.pruebaTemperatura= encuestaCiudadanos.pruebaTemperatura;
        encuesta.resultadoPruebaTemp= encuestaCiudadanos.resultadoPruebaTemp;
        encuesta.casoSospechoso= encuestaCiudadanos.casoSospechoso;
        encuesta.pruebaAnticuerpos= encuestaCiudadanos.pruebaAnticuerpos;
        encuesta.pruebaAntigenos = encuestaCiudadanos.pruebaAntigenos;
        encuesta.pruebaMolecular = encuestaCiudadanos.pruebaMolecular;
        encuesta.longitud= encuestaCiudadanos.longitud;
        encuesta.latitud= encuestaCiudadanos.latitud;
        encuesta.estado = "false";

            
           
             DatabaseEncuestaCiudadano editar = DatabaseEncuestaCiudadano.instance;
            await editar.updateEncuestaCiudadano(encuesta);
          }

           


      }

 
        }
      }
    }
  }

  _getLocation(direccion, vive) async {
    if(vive == true){
    try {
      geolocator
          .placemarkFromAddress(
              direccion + "Valle del Cauca" + "Candelaria")

           .then((List<Placemark> placemark) {
        setState(() {
          lat = placemark[0].position.latitude;
          lon = placemark[0].position.longitude;
        });
        print("vive en Candelaria");
        print(placemark[0].position);
      }).catchError((e) {
        print(e);
      });
    } catch (e) {
      print(e);
    }
    }else{
       DatabaseMunicipios municipios = DatabaseMunicipios.instance;
        Municipios municipio = await municipios
            .queryMunicipiosFirst(direccion);
      try {
      geolocator
          .placemarkFromAddress(
              municipio.nombre)

           .then((List<Placemark> placemark) {
        setState(() {
          lat = placemark[0].position.latitude;
          lon = placemark[0].position.longitude;
        });
        print("no vive en Candelaria");
        print(placemark[0].position);
      }).catchError((e) {
        print(e);
      });
    } catch (e) {
      print(e);
    }

    }

  }

  obtenerInformacion(token) async {
    final response1 = await http
        .get( url + '/api/1.0/sincronizacion/', headers: {
      'Authorization': 'Token ' + token,
      'Content-type': 'application/json'
    });
    var data1 = utf8.decode(response1.bodyBytes);
    var jsonResponse1 = jsonDecode(data1);

    if (response1.statusCode == 200) {
      for (var i = 0; i < jsonResponse1["empresas"].length; i++) {
        DatabaseHelper helper = DatabaseHelper.instance;
        Empresa empresa = await helper
            .queryProducto(jsonResponse1["empresas"][i]['nit'].toString());
        if (empresa == null) {
          _save(
              jsonResponse1["empresas"][i]['id'],
              jsonResponse1["empresas"][i]['nit'].toString(),
              jsonResponse1["empresas"][i]['nombre'],
              jsonResponse1["empresas"][i]['telefono'],
              jsonResponse1["empresas"][i]['correo'],
              jsonResponse1["empresas"][i]['direccion'],
              jsonResponse1["empresas"][i]['corregimiento'],
              //jsonResponse1["empresas"][i]['actividad'],
              //jsonResponse1["empresas"][i]['categeoria'],
              jsonResponse1["empresas"][i]['encuestada'],
              jsonResponse1["empresas"][i]['representante_legal']);
        } else {
          _update(
              jsonResponse1["empresas"][i]['id'],
              jsonResponse1["empresas"][i]['nit'].toString(),
              jsonResponse1["empresas"][i]['nombre'],
              jsonResponse1["empresas"][i]['telefono'],
              jsonResponse1["empresas"][i]['correo'],
              jsonResponse1["empresas"][i]['direccion'],
              jsonResponse1["empresas"][i]['corregimiento'],
              //jsonResponse1["empresas"][i]['actividad'],
              //jsonResponse1["empresas"][i]['categeoria'],
              jsonResponse1["empresas"][i]['encuestada'],
              jsonResponse1["empresas"][i]['representante_legal']);
        }
      }

      for (var i = 0; i < jsonResponse1["usuarios"].length; i++) {
        DatabaseUsuarios helper = DatabaseUsuarios.instance;
        Usuario usuario =
            await helper.queryUsuario(jsonResponse1["usuarios"][i]['username']);
        if (usuario == null) {
          _saveUsuario(
            jsonResponse1["usuarios"][i]['username'],
            "sin token",
          );
        }
      }


      for (var i = 0; i < jsonResponse1["preguntas"].length; i++) {
        DatabasePreguntasEmpresas preguntas =
            DatabasePreguntasEmpresas.instance;
        PreguntasEmpresas empresa =
            await preguntas.queryPregunta(jsonResponse1["preguntas"][i]['id']);
        if (empresa == null) {
          _savePreguntas(
              jsonResponse1["preguntas"][i]['id'],
              jsonResponse1["preguntas"][i]['pregunta'],
              jsonResponse1["preguntas"][i]['categoria_nombre'],
              jsonResponse1["preguntas"][i]['help_text']);
        } else {
          _updatePreguntas(
              jsonResponse1["preguntas"][i]['id'],
              jsonResponse1["preguntas"][i]['pregunta'],
              jsonResponse1["preguntas"][i]['categoria_nombre'],
              jsonResponse1["preguntas"][i]['help_text']);
        }
      }

      for (var i = 0; i < jsonResponse1["corregimientos"].length; i++) {
        DatabaseCorregimientos corregimientos = DatabaseCorregimientos.instance;
        Corregimientos corregimiento = await corregimientos
            .queryCorregimientosFirst(jsonResponse1["corregimientos"][i]['id']);
        if (corregimiento == null) {
          _saveCorregimientos(jsonResponse1["corregimientos"][i]['id'],
              jsonResponse1["corregimientos"][i]['nombre']);
        } else {
          _updateCorregimientos(jsonResponse1["corregimientos"][i]['id'],
              jsonResponse1["corregimientos"][i]['nombre']);
        }
      }

      for (var i = 0; i < jsonResponse1["veredas"].length; i++) {
        DatabaseVeredas veredas = DatabaseVeredas.instance;
        Veredas vereda =
            await veredas.queryVeredasFirst(jsonResponse1["veredas"][i]['id']);
        if (vereda == null) {
          _saveVeredas(jsonResponse1["veredas"][i]['id'],
              jsonResponse1["veredas"][i]['nombre']);
        } else {
          _updateVeredas(jsonResponse1["veredas"][i]['id'],
              jsonResponse1["veredas"][i]['nombre']);
        }
      }

      for (var i = 0; i < jsonResponse1["callejon"].length; i++) {
        DatabaseCallejon callejones = DatabaseCallejon.instance;
        Callejon callejon =
            await callejones.queryCallejonFirst(jsonResponse1["callejon"][i]['id']);
        if (callejon == null) {
          _saveCallejon(jsonResponse1["callejon"][i]['id'],
              jsonResponse1["callejon"][i]['nombre']);
        } else {
          _updateCallejon(jsonResponse1["callejon"][i]['id'],
              jsonResponse1["callejon"][i]['nombre']);
        }
      }

      for (var i = 0; i < jsonResponse1["barrios"].length; i++) {
        DatabaseBarrios barrios = DatabaseBarrios.instance;
        Barrios barrio =
            await barrios.queryBarriosFirst(jsonResponse1["barrios"][i]['id']);
        if (barrio == null) {
          _saveBarrios(jsonResponse1["barrios"][i]['id'],
              jsonResponse1["barrios"][i]['nombre']);
        } else {
          _updateBarrios(jsonResponse1["barrios"][i]['id'],
              jsonResponse1["barrios"][i]['nombre']);
        }
      }

      for (var i = 0; i < jsonResponse1["tipo_identificacion"].length; i++) {
        DatabaseIndenfiticacion tipoIdentificaciones =
            DatabaseIndenfiticacion.instance;
        Indenfiticacion tipoIdentificacion =
            await tipoIdentificaciones.queryIndenfiticacionFirst(
                jsonResponse1["tipo_identificacion"][i]['id']);
        if (tipoIdentificacion == null) {
          _saveIndenfiticacion(jsonResponse1["tipo_identificacion"][i]['id'],
              jsonResponse1["tipo_identificacion"][i]['nombre']);
        } else {
          _updateIndenfiticacion(jsonResponse1["tipo_identificacion"][i]['id'],
              jsonResponse1["tipo_identificacion"][i]['nombre']);
        }
      }

      for (var i = 0; i < jsonResponse1["sexos"].length; i++) {
        DatabaseSexo sexos = DatabaseSexo.instance;
        Sexo sexo = await sexos.querySexoFirst(jsonResponse1["sexos"][i]['id']);
        if (sexo == null) {
          _saveSexo(jsonResponse1["sexos"][i]['id'],
              jsonResponse1["sexos"][i]['nombre']);
        } else {
          _updateSexo(jsonResponse1["sexos"][i]['id'],
              jsonResponse1["sexos"][i]['nombre']);
        }
      }

      for (var i = 0; i < jsonResponse1["etnias"].length; i++) {
        DatabaseEtnias etnias = DatabaseEtnias.instance;
        Etnias etnia =
            await etnias.queryEtniasFirst(jsonResponse1["etnias"][i]['id']);
        if (etnia == null) {
          _saveEtnias(jsonResponse1["etnias"][i]['id'],
              jsonResponse1["etnias"][i]['nombre']);
        } else {
          _updateEtnias(jsonResponse1["etnias"][i]['id'],
              jsonResponse1["etnias"][i]['nombre']);
        }
      }

      for (var i = 0; i < jsonResponse1["grupos_poblacionales"].length; i++) {
        DatabasePoblacional gruposPoblacionales = DatabasePoblacional.instance;
        Poblacional gruposPoblacional = await gruposPoblacionales
            .queryPoblacionalFirst(jsonResponse1["grupos_poblacionales"][i]['id']);
        if (gruposPoblacional == null) {
          _savePoblacional(jsonResponse1["grupos_poblacionales"][i]['id'],
              jsonResponse1["grupos_poblacionales"][i]['nombre']);
        } else {
          _updatePoblacional(jsonResponse1["grupos_poblacionales"][i]['id'],
              jsonResponse1["grupos_poblacionales"][i]['nombre']);
        }
      }

      for (var i = 0; i < jsonResponse1["personas_vulnerables"].length; i++) {
        DatabaseVulnerables gruposVulnerables = DatabaseVulnerables.instance;
        Vulnerables gruposVulnerable = await gruposVulnerables
            .queryVulnerablesFirst(jsonResponse1["personas_vulnerables"][i]['id']);
        if (gruposVulnerable == null) {
          _saveVulnerables(jsonResponse1["personas_vulnerables"][i]['id'],
              jsonResponse1["personas_vulnerables"][i]['nombre']);
        } else {
          _updateVulnerables(jsonResponse1["personas_vulnerables"][i]['id'],
              jsonResponse1["personas_vulnerables"][i]['nombre']);
        }
      }

      for (var i = 0; i < jsonResponse1["ocupaciones"].length; i++) {
        DatabaseOcupaciones gruposOcupaciones = DatabaseOcupaciones.instance;
        Ocupaciones gruposOcupacion = await gruposOcupaciones
            .queryOcupacionesFirst(jsonResponse1["ocupaciones"][i]['id']);
        if (gruposOcupacion == null) {
          _saveOcupaciones(jsonResponse1["ocupaciones"][i]['id'],
              jsonResponse1["ocupaciones"][i]['nombre']);
        } else {
          _updateOcupaciones(jsonResponse1["ocupaciones"][i]['id'],
              jsonResponse1["ocupaciones"][i]['nombre']);
        }
      }

      for (var i = 0; i < jsonResponse1["departamentos"].length; i++) {
        DatabaseDepartamentos departamentos = DatabaseDepartamentos.instance;
        Departamentos departamento = await departamentos
            .queryDepartamentosFirst(jsonResponse1["departamentos"][i]['id']);
        if (departamento == null) {
          _saveDepartamentos(jsonResponse1["departamentos"][i]['id'],
              jsonResponse1["departamentos"][i]['nombre']);
        } else {
          _updateDepartamentos(jsonResponse1["departamentos"][i]['id'],
              jsonResponse1["departamentos"][i]['nombre']);
        }
      }

      for (var i = 0; i < jsonResponse1["municipios"].length; i++) {
        DatabaseMunicipios municipios = DatabaseMunicipios.instance;
        Municipios municipio = await municipios
            .queryMunicipiosFirst(jsonResponse1["municipios"][i]['id']);
        if (municipio == null) {
          _saveMunicipios(jsonResponse1["municipios"][i]['id'],
          jsonResponse1["municipios"][i]['codigo_departamento'],
              jsonResponse1["municipios"][i]['nombre']);
        } else {
          _updateMunicipios(jsonResponse1["municipios"][i]['id'],
          jsonResponse1["municipios"][i]['codigo_departamento'],
              jsonResponse1["municipios"][i]['nombre']);
        }
      }

      for (var i = 0; i < jsonResponse1["tipo_trabajos"].length; i++) {
        DatabaseTrabajos tipoTrabajos = DatabaseTrabajos.instance;
        Trabajos tipoTrabajo = await tipoTrabajos
            .queryTrabajosFirst(jsonResponse1["tipo_trabajos"][i]['id']);
        if (tipoTrabajo == null) {
          _saveTrabajos(jsonResponse1["tipo_trabajos"][i]['id'],
              jsonResponse1["tipo_trabajos"][i]['nombre']);
        } else {
          _updateTrabajos(jsonResponse1["tipo_trabajos"][i]['id'],
              jsonResponse1["tipo_trabajos"][i]['nombre']);
        }
      }

      for (var i = 0; i < jsonResponse1["eps"].length; i++) {
        DatabaseEps tipoEps = DatabaseEps.instance;
        Eps tiposEps = await tipoEps.queryEpsFirst(jsonResponse1["eps"][i]['id']);
        if (tiposEps == null) {
          _saveEps(
              jsonResponse1["eps"][i]['id'], jsonResponse1["eps"][i]['nombre']);
        } else {
          _updateEps(
              jsonResponse1["eps"][i]['id'], jsonResponse1["eps"][i]['nombre']);
        }
      }

      for (var i = 0; i < jsonResponse1["dificultad_eps"].length; i++) {
        DatabaseDificultadEps dificultadEps = DatabaseDificultadEps.instance;
        DificultadEps dificultadesEps = await dificultadEps
            .queryDificultadEpsFirst(jsonResponse1["dificultad_eps"][i]['id']);
        if (dificultadesEps == null) {
          _saveDificultadEps(jsonResponse1["dificultad_eps"][i]['id'],
              jsonResponse1["dificultad_eps"][i]['nombre']);
        } else {
          _updateDificultadEps(jsonResponse1["dificultad_eps"][i]['id'],
              jsonResponse1["dificultad_eps"][i]['nombre']);
        }
      }

      for (var i = 0; i < jsonResponse1["salud_mental"].length; i++) {
        DatabaseSaludMental saludMental = DatabaseSaludMental.instance;
        SaludMental saludMentales = await saludMental
            .querySaludMentalFirst(jsonResponse1["salud_mental"][i]['id']);
        if (saludMentales == null) {
          _saveSaludMental(jsonResponse1["salud_mental"][i]['id'],
              jsonResponse1["salud_mental"][i]['nombre']);
        } else {
          _updateSaludMental(jsonResponse1["salud_mental"][i]['id'],
              jsonResponse1["salud_mental"][i]['nombre']);
        }
      }

      setState(() {
        cargando = false;
      });
      _sincronizarBaseDatos();
    }
  }

  _save(idEmpresa, nit, nombre, telefono, correo, direccion, corregimiento,
//actividad, categoria, 
      encuestada, representante) async {
    Empresa empresa = Empresa();

    empresa.nit = nit.toString();
    empresa.idEmpresa = idEmpresa;
    empresa.nombre = nombre;
    empresa.telefono = telefono;
    empresa.correo = correo;
    empresa.direccion = direccion;
    empresa.corregimiento = corregimiento;
    //empresa.actividad = actividad;
    //empresa.categoria = categoria;
    empresa.encuestada = encuestada.toString();
    empresa.representante = representante;

    DatabaseHelper helper = DatabaseHelper.instance;
    int id = await helper.insert(empresa);
  }

  _update(idEmpresa, nit, nombre, telefono, correo, direccion, corregimiento,
      //actividad, categoria, 
      encuestada, representante) async {
    Empresa empresa = Empresa();

    empresa.nit = nit.toString();
    empresa.idEmpresa = idEmpresa;
    empresa.nombre = nombre;
    empresa.telefono = telefono;
    empresa.correo = correo;
    empresa.direccion = direccion;
    empresa.corregimiento = corregimiento;
    //empresa.actividad = actividad;
    //empresa.categoria = categoria;
    empresa.encuestada = encuestada.toString();
    empresa.representante = representante;

    DatabaseHelper helper = DatabaseHelper.instance;
    int id = await helper.update(empresa);
  }

  _saveUsuario(nombre, token) async {
    Usuario usuario = Usuario();

    usuario.nombre = nombre;
    usuario.token = token;

    DatabaseUsuarios helper = DatabaseUsuarios.instance;
    int id = await helper.insertUsuario(usuario);
  }

  _updateUsuario(nombre, token) async {
    Usuario usuario = Usuario();

    usuario.nombre = nombre;
    usuario.token = token;

    DatabaseUsuarios helper = DatabaseUsuarios.instance;
    await helper.updateUsuario(usuario);
  }

  _savePreguntas(idPreguntasSinc, pregunta, categoria, textoAyuda) async {
    PreguntasEmpresas preguntas = PreguntasEmpresas();

    preguntas.idPreguntas = idPreguntasSinc;
    preguntas.preguntas = pregunta;
    preguntas.categoria = categoria;
    preguntas.infoPreguntas = textoAyuda;

    DatabasePreguntasEmpresas helper = DatabasePreguntasEmpresas.instance;
    int id = await helper.insertPregunta(preguntas);
  }

  _updatePreguntas(idPreguntasSinc, pregunta, categoria, textoAyuda) async {
    PreguntasEmpresas preguntas = PreguntasEmpresas();

    preguntas.idPreguntas = idPreguntasSinc;
    preguntas.preguntas = pregunta;
    preguntas.categoria = categoria;
    preguntas.infoPreguntas = textoAyuda;

    DatabasePreguntasEmpresas helper = DatabasePreguntasEmpresas.instance;
    int id = await helper.updatePregunta(preguntas);
  }

  _saveCorregimientos(idCorregimientos, nombre) async {
    Corregimientos corregimientos = Corregimientos();

    corregimientos.idCorregimientos = idCorregimientos;
    corregimientos.nombre = nombre;

    DatabaseCorregimientos helper = DatabaseCorregimientos.instance;
    await helper.insertCorregimientos(corregimientos);
  }

  _updateCorregimientos(idCorregimientos, nombre) async {
    Corregimientos corregimientos = Corregimientos();

    corregimientos.idCorregimientos = idCorregimientos;
    corregimientos.nombre = nombre;

    DatabaseCorregimientos helper = DatabaseCorregimientos.instance;
    await helper.updateCorregimientos(corregimientos);
  }

  _saveVeredas(idVeredas, nombre) async {
    Veredas veredas = Veredas();

    veredas.idVeredas = idVeredas;
    veredas.nombre = nombre;

    DatabaseVeredas helper = DatabaseVeredas.instance;
    await helper.insertVeredas(veredas);
  }

  _updateVeredas(idVeredas, nombre) async {
    Veredas veredas = Veredas();

    veredas.idVeredas = idVeredas;
    veredas.nombre = nombre;

    DatabaseVeredas helper = DatabaseVeredas.instance;
    await helper.updateVeredas(veredas);
  }

  _saveCallejon(idCallejon, nombre) async {
    Callejon callejon = Callejon();

    callejon.idCallejon = idCallejon;
    callejon.nombre = nombre;

    DatabaseCallejon helper = DatabaseCallejon.instance;
    await helper.insertCallejon(callejon);
  }

  _updateCallejon(idCallejon, nombre) async {
    Callejon callejon = Callejon();

    callejon.idCallejon = idCallejon;
    callejon.nombre = nombre;

    DatabaseCallejon helper = DatabaseCallejon.instance;
    await helper.updateCallejon(callejon);
  }

  _saveBarrios(idBarrios, nombre) async {
    Barrios barrios = Barrios();

    barrios.idBarrios = idBarrios;
    barrios.nombre = nombre;

    DatabaseBarrios helper = DatabaseBarrios.instance;
    await helper.insertBarrios(barrios);
  }

  _updateBarrios(idBarrios, nombre) async {
    Barrios barrios = Barrios();

    barrios.idBarrios = idBarrios;
    barrios.nombre = nombre;

    DatabaseBarrios helper = DatabaseBarrios.instance;
    await helper.updateBarrios(barrios);
  }

  _saveIndenfiticacion(idIndenfiticacion, nombre) async {
    Indenfiticacion indenfiticacion = Indenfiticacion();

    indenfiticacion.idIndenfiticacion = idIndenfiticacion;
    indenfiticacion.nombre = nombre;

    DatabaseIndenfiticacion helper = DatabaseIndenfiticacion.instance;
    await helper.insertIndenfiticacion(indenfiticacion);
  }

  _updateIndenfiticacion(idIndenfiticacion, nombre) async {
    Indenfiticacion indenfiticacion = Indenfiticacion();

    indenfiticacion.idIndenfiticacion = idIndenfiticacion;
    indenfiticacion.nombre = nombre;

    DatabaseIndenfiticacion helper = DatabaseIndenfiticacion.instance;
    await helper.updateIndenfiticacion(indenfiticacion);
  }

  _saveSexo(idSexo, nombre) async {
    Sexo sexo = Sexo();

    sexo.idSexo = idSexo;
    sexo.nombre = nombre;

    DatabaseSexo helper = DatabaseSexo.instance;
    await helper.insertSexo(sexo);
  }

  _updateSexo(idSexo, nombre) async {
    Sexo sexo = Sexo();

    sexo.idSexo = idSexo;
    sexo.nombre = nombre;

    DatabaseSexo helper = DatabaseSexo.instance;
    await helper.updateSexo(sexo);
  }

  _saveEtnias(idEtnias, nombre) async {
    Etnias etnias = Etnias();

    etnias.idEtnias = idEtnias;
    etnias.nombre = nombre;

    DatabaseEtnias helper = DatabaseEtnias.instance;
    await helper.insertEtnias(etnias);
  }

  _updateEtnias(idEtnias, nombre) async {
    Etnias etnias = Etnias();

    etnias.idEtnias = idEtnias;
    etnias.nombre = nombre;

    DatabaseEtnias helper = DatabaseEtnias.instance;
    await helper.updateEtnias(etnias);
  }

  _savePoblacional(idPoblacional, nombre) async {
    Poblacional poblacional = Poblacional();

    poblacional.idPoblacional = idPoblacional;
    poblacional.nombre = nombre;

    DatabasePoblacional helper = DatabasePoblacional.instance;
    await helper.insertPoblacional(poblacional);
  }

  _updatePoblacional(idPoblacional, nombre) async {
    Poblacional poblacional = Poblacional();

    poblacional.idPoblacional = idPoblacional;
    poblacional.nombre = nombre;

    DatabasePoblacional helper = DatabasePoblacional.instance;
    await helper.updatePoblacional(poblacional);
  }

  _saveVulnerables(idVulnerables, nombre) async {
    Vulnerables vulnerables = Vulnerables();

    vulnerables.idVulnerables = idVulnerables;
    vulnerables.nombre = nombre;

    DatabaseVulnerables helper = DatabaseVulnerables.instance;
    await helper.insertVulnerables(vulnerables);
  }

  _updateVulnerables(idVulnerables, nombre) async {
    Vulnerables vulnerables = Vulnerables();

    vulnerables.idVulnerables = idVulnerables;
    vulnerables.nombre = nombre;

    DatabaseVulnerables helper = DatabaseVulnerables.instance;
    await helper.updateVulnerables(vulnerables);
  }

  _saveOcupaciones(idOcupaciones, nombre) async {
    Ocupaciones ocupaciones = Ocupaciones();

    ocupaciones.idOcupaciones = idOcupaciones;
    ocupaciones.nombre = nombre;

    DatabaseOcupaciones helper = DatabaseOcupaciones.instance;
    await helper.insertOcupaciones(ocupaciones);
  }

  _updateOcupaciones(idOcupaciones, nombre) async {
    Ocupaciones ocupaciones = Ocupaciones();

    ocupaciones.idOcupaciones = idOcupaciones;
    ocupaciones.nombre = nombre;

    DatabaseOcupaciones helper = DatabaseOcupaciones.instance;
    await helper.updateOcupaciones(ocupaciones);
  }

  _saveDepartamentos(idDepartamentos, nombre) async {
    Departamentos departamentos = Departamentos();

    departamentos.idDepartamentos = idDepartamentos;
    departamentos.nombre = nombre;

    DatabaseDepartamentos helper = DatabaseDepartamentos.instance;
    await helper.insertDepartamentos(departamentos);
  }

  _updateDepartamentos(idDepartamentos, nombre) async {
    Departamentos departamentos = Departamentos();

    departamentos.idDepartamentos = idDepartamentos;
    departamentos.nombre = nombre;

    DatabaseDepartamentos helper = DatabaseDepartamentos.instance;
    await helper.updateDepartamentos(departamentos);
  }

  _saveMunicipios(idMunicipios, codigo, nombre) async {
    Municipios municipios = Municipios();

    municipios.idMunicipios = idMunicipios;
    municipios.codigoDepartamento = codigo;
    municipios.nombre = nombre;

    DatabaseMunicipios helper = DatabaseMunicipios.instance;
    await helper.insertMunicipios(municipios);
  }

  _updateMunicipios(idMunicipios, codigo,  nombre) async {
    Municipios municipios = Municipios();

    municipios.idMunicipios = idMunicipios;
    municipios.codigoDepartamento = codigo;
    municipios.nombre = nombre;

    DatabaseMunicipios helper = DatabaseMunicipios.instance;
    await helper.updateMunicipios(municipios);
  }

  _saveTrabajos(idTrabajos, nombre) async {
    Trabajos trabajos = Trabajos();

    trabajos.idTrabajos = idTrabajos;
    trabajos.nombre = nombre;

    DatabaseTrabajos helper = DatabaseTrabajos.instance;
    await helper.insertTrabajos(trabajos);
  }

  _updateTrabajos(idTrabajos, nombre) async {
    Trabajos trabajos = Trabajos();

    trabajos.idTrabajos = idTrabajos;
    trabajos.nombre = nombre;

    DatabaseTrabajos helper = DatabaseTrabajos.instance;
    await helper.updateTrabajos(trabajos);
  }

  _saveEps(idEps, nombre) async {
    Eps eps = Eps();

    eps.idEps = idEps;
    eps.nombre = nombre;

    DatabaseEps helper = DatabaseEps.instance;
    await helper.insertEps(eps);
  }

  _updateEps(idEps, nombre) async {
    Eps eps = Eps();

    eps.idEps = idEps;
    eps.nombre = nombre;

    DatabaseEps helper = DatabaseEps.instance;
    await helper.updateEps(eps);
  }

  _saveDificultadEps(idDificultadEps, nombre) async {
    DificultadEps dificultadEps = DificultadEps();

    dificultadEps.idDificultadEps = idDificultadEps;
    dificultadEps.nombre = nombre;

    DatabaseDificultadEps helper = DatabaseDificultadEps.instance;
    await helper.insertDificultadEps(dificultadEps);
  }

  _updateDificultadEps(idDificultadEps, nombre) async {
    DificultadEps dificultadEps = DificultadEps();

    dificultadEps.idDificultadEps = idDificultadEps;
    dificultadEps.nombre = nombre;

    DatabaseDificultadEps helper = DatabaseDificultadEps.instance;
    await helper.updateDificultadEps(dificultadEps);
  }

  _saveSaludMental(idSaludMental, nombre) async {
    SaludMental saludMental = SaludMental();

    saludMental.idSaludMental = idSaludMental;
    saludMental.nombre = nombre;

    DatabaseSaludMental helper = DatabaseSaludMental.instance;
    await helper.insertSaludMental(saludMental);
  }

  _updateSaludMental(idSaludMental, nombre) async {
    SaludMental saludMental = SaludMental();

    saludMental.idSaludMental = idSaludMental;
    saludMental.nombre = nombre;

    DatabaseSaludMental helper = DatabaseSaludMental.instance;
    await helper.updateSaludMental(saludMental);
  }

  Future<void> sinInternet() async {
    return showDialog<void>(
      context: context,
      barrierDismissible: false, // user must tap button!
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text('No estas conectado a internet'),
          content: SingleChildScrollView(
            child: ListBody(
              children: <Widget>[
                Text(
                    'Debes estar conectado a internet para poder sincronizar la base de datos'),
              ],
            ),
          ),
          actions: <Widget>[
            FlatButton(
              child: Text('Ok'),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
          ],
        );
      },
    );
  }

  Future<void> usuarioNoexiste() async {
    return showDialog<void>(
      context: context,
      barrierDismissible: false, // user must tap button!
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text('El usuario no existe'),
          content: SingleChildScrollView(
            child: ListBody(
              children: <Widget>[
                Text(
                    'Verifica los datos de usuario y contraseña, o sincroniza la base de datos'),
              ],
            ),
          ),
          actions: <Widget>[
            FlatButton(
              child: Text('Ok'),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
          ],
        );
      },
    );
  }

  Future<void> _sincronizarBaseDatos() async {
    return showDialog<void>(
      context: context,
      barrierDismissible: false, // user must tap button!
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text('Listo'),
          content: SingleChildScrollView(
            child: ListBody(
              children: <Widget>[
                Text('Ya se sincronizo la base de datos'),
              ],
            ),
          ),
          actions: <Widget>[
            FlatButton(
              child: Text('Ok'),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
          ],
        );
      },
    );
  }
  Future<bool> _onBackPressed() {
    return showDialog(
          context: context,
          builder: (context) => new AlertDialog(
            title: new Text('Salir de la aplicación'),
            content: new Text('¿Quiere salir de la aplicación?'),
            actions: <Widget>[
              new FlatButton(
                child: new Text("Si"),
                onPressed: () {
                  Navigator.of(context).pop(true);
                },
              ),
              new FlatButton(
                child: new Text("No"),
                onPressed: () {
                  Navigator.of(context).pop(false);
                },
              ),
              SizedBox(height: 16),
            ],
          ),
        ) ??
        false;
  }
}