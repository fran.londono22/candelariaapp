import 'dart:async';
import 'package:encuesta/db/database_Barrios.dart';
import 'package:encuesta/db/database_Callejon.dart';
import 'package:encuesta/db/database_Corregimientos.dart';
import 'package:encuesta/db/database_Departamentos.dart';
import 'package:encuesta/db/database_DificultadEps.dart';
import 'package:encuesta/db/database_Eps.dart';
import 'package:encuesta/db/database_Etnias.dart';
import 'package:encuesta/db/database_Indenfiticacion.dart';
import 'package:encuesta/db/database_Municipios.dart';
import 'package:encuesta/db/database_Ocupaciones.dart';
import 'package:encuesta/db/database_Poblacional.dart';
import 'package:encuesta/db/database_SaludMental.dart';
import 'package:encuesta/db/database_Sexo.dart';
import 'package:encuesta/db/database_Trabajos.dart';
import 'package:encuesta/db/database_Veredas.dart';
import 'package:encuesta/db/database_Vulnerables.dart';
import 'package:flutter/material.dart';
import 'buscarEmpresa.dart';
import 'package:latlong/latlong.dart';
import 'package:flutter_map/flutter_map.dart';
import 'package:geolocator/geolocator.dart';
import 'package:intl/intl.dart';
import 'db/database_EncuestasCiudadanos.dart';
import 'login.dart';
import 'package:percent_indicator/percent_indicator.dart';

class EncuestaCiudadanoPage extends StatefulWidget {
  EncuestaCiudadanoPage({Key key}) : super(key: key);

  @override
  _EncuestaCiudadanoPage createState() => _EncuestaCiudadanoPage();
}

class _EncuestaCiudadanoPage extends State<EncuestaCiudadanoPage> {

 double _progress = 0;

  startTimer() {
    new Timer.periodic(
      Duration(seconds: 1),
      (Timer timer) => setState(
        () {
          if (_progress == 1) {
            timer.cancel();
          } else {
            _progress += 0.2;
          }
        },
      ),
    );
  }








  bool mapa = false;
  final _nombreDelEncuestadoController = TextEditingController();
  final _apellidosDelEncuestadoController = TextEditingController();
  String dropdownValueIdentificacion = 'Tipo de identificación';
  final _numeroDeIdentificacionController = TextEditingController();
  final _telefonoController = TextEditingController();
  final _direccionController = TextEditingController();
  final _frecuenciaMensualController = TextEditingController();
  final _lugarFrecuentaController = TextEditingController();
  final _motivoVisitaController = TextEditingController();
  final _tiempoVisitaController = TextEditingController();
  String dropdownValueDepartamento = 'Departamento';
  String dropdownValueMunicipio = 'Municipio';
  String dropdownValueCorregimiento = 'Corregimiento';
  String dropdownValueVereda = 'Vereda';
  String dropdownValueCallejon = 'Callejón';
  String dropdownValueBarrio = 'Barrio';
  String dropdownValueSexo = 'Sexo';
  DateTime fechaNac;
  String fechaView = "";
  String dropdownValueEtnia = 'Grupo étnico';
  String dropdownValueGrupo = 'Grupo de población diferencial';
  String dropdownValueVulnerabilidad =
      'Personas con mayor vulnerabilidad para infectarse';
  String dropdownValueOcupacion = 'Ocupación';
  String dropdownValueTrabajo = 'Tipo de trabajador';
  final _nombreEmpresaController = TextEditingController();
  final _direccionEmpresaController = TextEditingController();
  String dropdownValueEPS = 'Entidades Administradoras de Plan de Beneficio';
  final _nombreOtraEPSController = TextEditingController();
  String dropdownValueDificultadEPS =
      'Dificultades en la prestación de servicios de salud';
  String dropdownValueSaludMental = 'Factores de riesgo en salud mental';
  int identificacion = 0;
  int departamento = 0;
  int municipio = 0;
  int corregimiento = 0;
  int vereda = 0;
  int callejon = 0;
  int barrio = 0;
  int etnia = 0;
  int sexo = 0;
  int grupo = 0;
  int vulnerable = 0;
  int ocupacion = 0;
  int trabajador = 0;
  int eps = 0;
  int dificultadEps = 0;
  int saludMental = 0;

  int radioValueViveCandelaria = -1;
  bool viveCandelaria;

  int radioValueTrabajaCandelaria = -1;
  bool trabajaCandelaria;

  int radioValueDiabetes = -1;
  bool diabetes;
  int radioValueCardiovascular = -1;
  bool cardiovascular;
  int radioValueCancer = -1;
  bool cancer;
  int radioValueRenal = -1;
  bool renal;
  int radioValueAsma = -1;
  bool asma;
  int radioValuePulmonar = -1;
  bool pulmonar;
  int radioValueImunusupresion = -1;
  bool imunusupresion;
  int radioValueObesidad = -1;
  bool obesidad;
  int radioValueDesnutricion = -1;
  bool desnutricion;
  int radioValueNeurologico = -1;
  bool neurologico;
  int radioValueTabaquismo = -1;
  bool tabaquismo;
  int radioValueAlcoholismo = -1;
  bool alcoholismo;

  int radioValueTos = -1;
  bool tos;
  int radioValueDificultadRespiratoria = -1;
  bool dificultadRespiratoria;
  int radioValueDolorGarganta = -1;
  bool dolorGarganta;
  int radioValueFiebre = -1;
  bool fiebre;
  int radioValueFatiga = -1;
  bool fatiga;
  int radioValueMalestar = -1;
  bool malestar;
  int radioValueGusto = -1;
  bool gusto;
  int radioValueOlfato = -1;
  bool olfato;

  int radioValueNasal = -1;
  bool nasal;
  DateTime fechaSint;
  String fechaSintView = "";

  int radioValueDesplazamiento = -1;
  bool desplazamiento;

  final _lugarViajeController = TextEditingController();
  int radioValueContacto = -1;
  bool contacto;
  final _lugarContactoController = TextEditingController();
  int radioValueTemperatura = -1;
  bool temperatura;
  final _resultadoTempController = TextEditingController();
  int radioValueCasoSospechoso = -1;
  bool casoSospechoso;
  int radioValueAnticuerpos = -1;
  bool anticuerpos;
  int radioValueAntigenos = -1;
  bool antigenos;
  int radioValueMolecular = -1;
  bool molecular;

  Placemark place;
  double lat;
  double lon;

  int esCovid = 0;

  List definiciones = [
    "Nombre completo de la persona que proporciona la información al encuestado",
    "Refiere al tipo de documento de identificación del encuestado",
    "Número exacto del documento de identificación  del encuestado (sin puntos o comas)",
    "Número telefónico del encuestado",
    "Nomenclatura o indicación de la ubicación de la vivienda del encuestado",
    "Unidad administrativa y política en que se dividen la República de  Colombia.",
    "Unidad administrativa y política en que se dividen los departamentos en Colombia.",
    "Unidad administrativa y política en que se dividen los municipios en Colombia.",
    "Las veredas comprenden principalmente zonas rurales, aunque en ocasiones puede contener un centro microurbano.",
    "Vía rural entre dos hileras de casas, fincas o asentamientos",
    "Cada una de las partes en que se divide el centro poblado o corregimiento",
    "Desde la perspectiva biológica, el sexo se refiere a las características genéticas, endocrinas y morfológicas del cuerpo.",
    "Fecha de nacimimiento de la persona encuestada",
    "Autorreconocimiento cultural (costumbres y tradiciones) o por los rasgos físicos.",
    "Es un enfoque de clasificación que búsca  analizar, actuar, valorar y garantizar el desarrollo de una población, basada en sus características diferenciales desde una perspectiva de equidad y diversidad.",
    "Personas que por su ocupación podrían tener una mayor exposición  al riesgo de contagio por COVID-19",
    "Actividad en la que el encuestado ocupó la mayor parte del tiempo la semana pasada",
    "Tipo de trabajador",
    "Nombre de la empresa u organización en la que trabaja el encuestado",
    "Nomenclatura o indicación de la ubicación de la empresa o lugar de trabajo del encuestado",
    "Se entiende por Entidades Administradoras de Planes de Beneficios de Salud (EAPB) las Entidades Promotoras de Salud (EPS) del Régimen Contributivo y Subsidiado.",
    "Nombre de la EPS que no se encuentra en el listado anterior",
    "Es el reporte por parte del usuario de dificultades o barreras en para  acceder a los servicios de salud",
    "El encuestado tiene un diagnóstico de Diabetes",
    "El encuestado tiene un diagnóstico de Enfermedad cardiovascular por ejemplo: la hipertensión arterial, el accidente cerebrovascular o el infarto de miocardio etc)",
    "El encuestado tiene o ha tenido un diagnóstico de cancer de cualquier tipo",
    "El encuestado tiene o ha tenido un diagnóstico de enfermedad renal",
    "El encuestado tieneun diagnóstico de Asma",
    "El encuestado tiene un diagnóstico de Enfermedad Pulmonar Obstructiva Crónica",
    "El encuestado tiene un diagnóstico de inmunosupresión o se realiza  en un tratamiento médico que lo produce",
    "El encuestado tiene un diagnóstico de Obesidad",
    "El encuestado tiene un diagnóstico de desnutrición",
    "El encuestado tiene un diagnóstico de Trastorno neurológico crónico (Por ejemplo: epilepsia, la enfermedad de Alzheimer y otras demencias, enfermedades cerebrovasculares, la esclerosis múltiple, la enfermedad de Parkinson, las infecciones neurológicas, los tumores cerebrales etc.)",
    "El encuestado es consumidor habitual de cigarrillo o productos derivados del tabaco",
    "El encuestado tiene un diagnóstico de enfermedad causada por el consumo abusivo de bebidas alcohólicas y por la adicción que crea este hábito.",
    "Los factores de riesgo están asociados con una mayor probabilidad de aparición de la enfermedad, mayor gravedad y mayor duración de los principales problemas de salud",
    "El encuestado ha tenido tos",
    "El encuestado ha presentado dificultad para respirar, sensación de ahogo u opresión en el pecho",
    "El encuestado ha presentado dolor de garganta",
    "El encuestado ha presentado fiebre cuantificada mayor a 38°C",
    "El encuestado ha presentado cansancio extremo e incapacidad para funcionar debido a la falta de energía.",
    "El encuestado ha presentado sensación general de malestar, dolor difuso o falta de interés en las actividades.",
    "El encuestado ha presentado disminución en la capacidad de percibir los diferentes sabores",
    "El encuestado ha presentado disminución en la capacidad de percibir  diferentes olores",
    "El encuestado ha presentado drenaje de un líquido claro o moco espeso proveniente de la nariz y las fosas nasales",
    "Es la fecha en que el encuestado comenzó a presentar el primer singno o síntoma",
    "El encuestado ha realizado viajes nacionales o internacionales durante los últimos 14 días",
    "Lugar a dónde el encuestado viajó",
    "El encuestado tuvo contacto estrecho o cercano con un caso confirmado o probable de infección por COVID-19",
    "Refiere al lugar dónde se produjo el contacto con un caso confirmado o probable de infección por COVID-19",
    "Refiere al resultado de la prueba de temperatura realizada durante la encuesta",
    "El encuestado cumple con los criterios de presencia de: signos y síntomas, Antecedentes de exposición y/o vulnerabilidad para definirlo como caso sospechoso de COVID-19",
    "Es un inmunoensayo cromatográfico para la detección cualitativa de anticuerpos IgG e IgM contra COVID-19 en muestras de sangre entera, suero o plasma humano. ",
    "Es una prueba para la COVID-19 detecta ciertas proteínas en el virus. Se usa un hisopo para tomar una muestra del fluido de la nariz o la garganta, pueden dar resultados en minutos.",
    "La RT-PCR es un tipo de muestra diagnóstica que funciona mediante la toma de una muestra extraída de un exudado nasofaríngeo; este material permite detectar el ARN, o material genético, del virus presente en la muestra."
  ];

  final Geolocator geolocator = Geolocator()..forceAndroidLocationManager;

  @override
  void initState() {
    super.initState();
    sincronizarValores();
  }

  List ident = ['Tipo de identificación'];
  List depart = ['Departamento'];
  List municip = ['Municipio'];
  List corregi = ['Corregimiento'];
  List vered = ['Vereda'];
  List callej = ['Callejón'];
  List barri = ['Barrio'];
  List sex = ['Sexo'];
  List etn = ['Grupo étnico'];
  List pobla = ['Grupo de población diferencial'];
  List vulne = ['Personas con mayor vulnerabilidad para infectarse'];
  List ocup = ['Ocupación'];
  List trabaj = ['Tipo de trabajador'];
  List ep = ["Entidades Administradoras de Plan de Beneficio"];
  List difi = ["Dificultades en la prestación de servicios de salud"];
  List mental = ["Factores de riesgo en salud mental"];

  getMunicipio(codigoDepartamento) async {
    municip = ['Municipio'];
    dropdownValueMunicipio = 'Municipio';

    DatabaseMunicipios municipiosSinc = DatabaseMunicipios.instance;
  int contarMunicipios = await municipiosSinc.getCountMunicipios();
    for (var i = 1; i <= contarMunicipios; i++) {
    Municipios getMunicipios =
        await municipiosSinc.queryMunicipios(i, codigoDepartamento);
    if (getMunicipios != null) {
      setState(() {
        municip.add(getMunicipios.nombre);
      });
    }
  }
  
  }

  sincronizarValores() async {
    DatabaseIndenfiticacion identificacionSinc =
        DatabaseIndenfiticacion.instance;
    int contarIdentificacion =
        await identificacionSinc.getCountIndenfiticacion();
    for (var i = 1; i <= contarIdentificacion; i++) {
      Indenfiticacion getIndenfiticacion =
          await identificacionSinc.queryIndenfiticacion(i);
      if (getIndenfiticacion != null) {
        setState(() {
          ident.add(getIndenfiticacion.nombre);
        });
      }
    }

    DatabaseDepartamentos departamentosSinc = DatabaseDepartamentos.instance;
    int contarDepartamentos = await departamentosSinc.getCountDepartamentos();
    for (var i = 1; i <= contarDepartamentos; i++) {
      Departamentos getDepartamentos =
          await departamentosSinc.queryDepartamentos(i);
      if (getDepartamentos != null) {
        setState(() {
          depart.add(getDepartamentos.nombre);
        });
      }
    }

    DatabaseCorregimientos corregimientosSinc = DatabaseCorregimientos.instance;
    int contarCorregimientos =
        await corregimientosSinc.getCountCorregimientos();
    for (var i = 1; i <= contarCorregimientos; i++) {
      Corregimientos getCorregimientos =
          await corregimientosSinc.queryCorregimientos(i);
      if (getCorregimientos != null) {
        setState(() {
          corregi.add(getCorregimientos.nombre);
        });
      }
    }

    DatabaseVeredas veredasSinc = DatabaseVeredas.instance;
    int contarVeredas = await veredasSinc.getCountVeredas();
    for (var i = 1; i <= contarVeredas; i++) {
      Veredas getVeredas = await veredasSinc.queryVeredas(i);
      if (getVeredas != null) {
        setState(() {
          vered.add(getVeredas.nombre);
        });
      }
    }

    DatabaseCallejon callejonSinc = DatabaseCallejon.instance;
    int contarCallejon = await callejonSinc.getCountCallejon();
    for (var i = 1; i <= contarCallejon; i++) {
      Callejon getCallejon = await callejonSinc.queryCallejon(i);
      if (getCallejon != null) {
        setState(() {
          callej.add(getCallejon.nombre);
        });
      }
    }

    DatabaseBarrios barriosSinc = DatabaseBarrios.instance;
    int contarBarrios = await barriosSinc.getCountBarrios();
    for (var i = 1; i <= contarBarrios; i++) {
      Barrios getBarrio = await barriosSinc.queryBarrios(i);
      if (getBarrio != null) {
        setState(() {
          barri.add(getBarrio.nombre);
        });
      }
    }

    DatabaseSexo sexoSinc = DatabaseSexo.instance;
    int contarSexo = await sexoSinc.getCountSexo();
    for (var i = 1; i <= contarSexo; i++) {
      Sexo getSexo = await sexoSinc.querySexo(i);
      if (getSexo != null) {
        setState(() {
          sex.add(getSexo.nombre);
        });
      }
    }

    DatabaseEtnias etniaSinc = DatabaseEtnias.instance;
    int contarEtnias = await etniaSinc.getCountEtnias();
    for (var i = 1; i <= contarEtnias; i++) {
      Etnias getEtnias = await etniaSinc.queryEtnias(i);
      if (getEtnias != null) {
        setState(() {
          etn.add(getEtnias.nombre);
        });
      }
    }

    DatabasePoblacional poblacionalSinc = DatabasePoblacional.instance;
    int contarPoblacional = await poblacionalSinc.getCountPoblacional();
    for (var i = 1; i <= contarPoblacional; i++) {
      Poblacional getPoblacional = await poblacionalSinc.queryPoblacional(i);
      if (getPoblacional != null) {
        setState(() {
          pobla.add(getPoblacional.nombre);
        });
      }
    }

    DatabaseVulnerables vulnerableSinc = DatabaseVulnerables.instance;
    int contarVulnerables = await vulnerableSinc.getCountVulnerables();
    for (var i = 1; i <= contarVulnerables; i++) {
      Vulnerables getVulnerable = await vulnerableSinc.queryVulnerables(i);
      if (getVulnerable != null) {
        setState(() {
          vulne.add(getVulnerable.nombre);
        });
      }
    }

    DatabaseOcupaciones ocupacionSinc = DatabaseOcupaciones.instance;
    int contarOcupaciones = await ocupacionSinc.getCountOcupaciones();
    for (var i = 1; i <= contarOcupaciones; i++) {
      Ocupaciones getOcupaciones = await ocupacionSinc.queryOcupaciones(i);
      if (getOcupaciones != null) {
        setState(() {
          ocup.add(getOcupaciones.nombre);
        });
      }
    }

    DatabaseTrabajos trabajosSinc = DatabaseTrabajos.instance;
    int contarTrabajos = await trabajosSinc.getCountTrabajos();
    for (var i = 1; i <= contarTrabajos; i++) {
      Trabajos getTrabajos = await trabajosSinc.queryTrabajos(i);
      if (getTrabajos != null) {
        setState(() {
          trabaj.add(getTrabajos.nombre);
        });
      }
    }

    DatabaseEps epsSinc = DatabaseEps.instance;
    int contarEps = await epsSinc.getCountEps();
    for (var i = 1; i <= contarEps; i++) {
      Eps getEps = await epsSinc.queryEps(i);
      if (getEps != null) {
        setState(() {
          ep.add(getEps.nombre);
        });
      }
    }

    DatabaseDificultadEps dificultadEpsSinc = DatabaseDificultadEps.instance;
    int contarDificultades = await dificultadEpsSinc.getCountDificultadEps();
    for (var i = 1; i <= contarDificultades; i++) {
      DificultadEps getDificultades =
          await dificultadEpsSinc.queryDificultadEps(i);
      if (getDificultades != null) {
        setState(() {
          difi.add(getDificultades.nombre);
        });
      }
    }

    DatabaseSaludMental saludMentalSinc = DatabaseSaludMental.instance;
    int contarSaludMental = await saludMentalSinc.getCountSaludMental();
    for (var i = 1; i <= contarSaludMental; i++) {
      SaludMental getSaludMental = await saludMentalSinc.querySaludMental(i);
      if (getSaludMental != null) {
        setState(() {
          mental.add(getSaludMental.nombre);
        });
      }
    }
  }

  void _handleRadioValueChangeViveCandelaria(int value) {
    setState(() {
      radioValueViveCandelaria = value;

      switch (radioValueViveCandelaria) {
        case 0:
          viveCandelaria = false;
          break;
        case 1:
          viveCandelaria = true;
          break;
      }
    });
  }

  void _handleRadioValueChangeTrabajaCandelaria(int value) {
    setState(() {
      radioValueTrabajaCandelaria = value;

      switch (radioValueTrabajaCandelaria) {
        case 0:
          trabajaCandelaria = false;
          break;
        case 1:
          trabajaCandelaria = true;
          break;
      }
    });
  }

  void _handleRadioValueChangeDiabetes(int value) {
    setState(() {
      radioValueDiabetes = value;

      switch (radioValueDiabetes) {
        case 0:
          diabetes = false;
          break;
        case 1:
          diabetes = true;
          break;
      }
    });
  }

  void _handleRadioValueChangeCardiovascular(int value) {
    setState(() {
      radioValueCardiovascular = value;

      switch (radioValueCardiovascular) {
        case 0:
          cardiovascular = false;
          break;
        case 1:
          cardiovascular = true;
          break;
      }
    });
  }

  void _handleRadioValueChangeCancer(int value) {
    setState(() {
      radioValueCancer = value;

      switch (radioValueCancer) {
        case 0:
          cancer = false;
          break;
        case 1:
          cancer = true;
          break;
      }
    });
  }

  void _handleRadioValueChangeRenal(int value) {
    setState(() {
      radioValueRenal = value;

      switch (radioValueRenal) {
        case 0:
          renal = false;
          break;
        case 1:
          renal = true;
          break;
      }
    });
  }

  void _handleRadioValueChangeAsma(int value) {
    setState(() {
      radioValueAsma = value;

      switch (radioValueAsma) {
        case 0:
          asma = false;
          break;
        case 1:
          asma = true;
          break;
      }
    });
  }

  void _handleRadioValueChangePulmonar(int value) {
    setState(() {
      radioValuePulmonar = value;

      switch (radioValuePulmonar) {
        case 0:
          pulmonar = false;
          break;
        case 1:
          pulmonar = true;
          break;
      }
    });
  }

  void _handleRadioValueChangeImunusupresion(int value) {
    setState(() {
      radioValueImunusupresion = value;

      switch (radioValueImunusupresion) {
        case 0:
          imunusupresion = false;
          break;
        case 1:
          imunusupresion = true;
          break;
      }
    });
  }

  void _handleRadioValueChangeObesidad(int value) {
    setState(() {
      radioValueObesidad = value;

      switch (radioValueObesidad) {
        case 0:
          obesidad = false;
          break;
        case 1:
          obesidad = true;
          break;
      }
    });
  }

  void _handleRadioValueChangeDesnutricion(int value) {
    setState(() {
      radioValueDesnutricion = value;

      switch (radioValueDesnutricion) {
        case 0:
          desnutricion = false;
          break;
        case 1:
          desnutricion = true;
          break;
      }
    });
  }

  void _handleRadioValueChangeNeurologico(int value) {
    setState(() {
      radioValueNeurologico = value;

      switch (radioValueNeurologico) {
        case 0:
          neurologico = false;
          break;
        case 1:
          neurologico = true;
          break;
      }
    });
  }

  void _handleRadioValueChangeTabaquismo(int value) {
    setState(() {
      radioValueTabaquismo = value;

      switch (radioValueTabaquismo) {
        case 0:
          tabaquismo = false;
          break;
        case 1:
          tabaquismo = true;
          break;
      }
    });
  }

  void _handleRadioValueChangeAlcoholismo(int value) {
    setState(() {
      radioValueAlcoholismo = value;

      switch (radioValueAlcoholismo) {
        case 0:
          alcoholismo = false;
          break;
        case 1:
          alcoholismo = true;
          break;
      }
    });
  }

  void _handleRadioValueChangeTos(int value) {
    setState(() {
      radioValueTos = value;

      switch (radioValueTos) {
        case 0:
          tos = false;
          break;
        case 1:
          tos = true;
          break;
      }
    });
  }

  void _handleRadioValueChangeDificultadResp(int value) {
    setState(() {
      radioValueDificultadRespiratoria = value;

      switch (radioValueDificultadRespiratoria) {
        case 0:
          dificultadRespiratoria = false;
          break;
        case 1:
          dificultadRespiratoria = true;
          break;
      }
    });
  }

  void _handleRadioValueChangeDolorGarg(int value) {
    setState(() {
      radioValueDolorGarganta = value;

      switch (radioValueDolorGarganta) {
        case 0:
          dolorGarganta = false;
          break;
        case 1:
          dolorGarganta = true;
          break;
      }
    });
  }

  void _handleRadioValueChangeFiebre(int value) {
    setState(() {
      radioValueFiebre = value;

      switch (radioValueFiebre) {
        case 0:
          fiebre = false;
          break;
        case 1:
          fiebre = true;
          break;
      }
    });
  }

  void _handleRadioValueChangeFatiga(int value) {
    setState(() {
      radioValueFatiga = value;

      switch (radioValueFatiga) {
        case 0:
          fatiga = false;
          break;
        case 1:
          fatiga = true;
          break;
      }
    });
  }

  void _handleRadioValueChangeMalestar(int value) {
    setState(() {
      radioValueMalestar = value;

      switch (radioValueMalestar) {
        case 0:
          malestar = false;
          break;
        case 1:
          malestar = true;
          break;
      }
    });
  }

  void _handleRadioValueChangeGusto(int value) {
    setState(() {
      radioValueGusto = value;

      switch (radioValueGusto) {
        case 0:
          gusto = false;
          break;
        case 1:
          gusto = true;
          break;
      }
    });
  }

  void _handleRadioValueChangeOlfato(int value) {
    setState(() {
      radioValueOlfato = value;

      switch (radioValueOlfato) {
        case 0:
          olfato = false;
          break;
        case 1:
          olfato = true;
          break;
      }
    });
  }

  void _handleRadioValueChangeNasal(int value) {
    setState(() {
      radioValueNasal = value;

      switch (radioValueNasal) {
        case 0:
          nasal = false;
          break;
        case 1:
          nasal = true;
          break;
      }
    });
  }

  void _handleRadioValueChangeDesplazamiento(int value) {
    setState(() {
      radioValueDesplazamiento = value;

      switch (radioValueDesplazamiento) {
        case 0:
          desplazamiento = false;
          break;
        case 1:
          desplazamiento = true;
          break;
      }
    });
  }

  void _handleRadioValueChangeContacto(int value) {
    setState(() {
      radioValueContacto = value;

      switch (radioValueContacto) {
        case 0:
          contacto = false;
          break;
        case 1:
          contacto = true;
          break;
      }
    });
  }

  void _handleRadioValueChangeTemperatura(int value) {
    setState(() {
      radioValueTemperatura = value;

      switch (radioValueTemperatura) {
        case 0:
          temperatura = false;
          break;
        case 1:
          temperatura = true;
          break;
      }
    });
  }

  void _handleRadioValueChangecasoSos(int value) {
    setState(() {
      radioValueCasoSospechoso = value;

      switch (radioValueCasoSospechoso) {
        case 0:
          casoSospechoso = false;
          break;
        case 1:
          casoSospechoso = true;
          break;
      }
    });
  }

  void _handleRadioValueChangeAnticuerpos(int value) {
    setState(() {
      radioValueAnticuerpos = value;

      switch (radioValueAnticuerpos) {
        case 0:
          anticuerpos = false;
          break;
        case 1:
          anticuerpos = true;
          break;
      }
    });
  }

  void _handleRadioValueChangeAntigenos(int value) {
    setState(() {
      radioValueAntigenos = value;

      switch (radioValueAntigenos) {
        case 0:
          antigenos = false;
          break;
        case 1:
          antigenos = true;
          break;
      }
    });
  }

  void _handleRadioValueChangeMoleculares(int value) {
    setState(() {
      radioValueMolecular = value;

      switch (radioValueMolecular) {
        case 0:
          molecular = false;
          break;
        case 1:
          molecular = true;
          break;
      }
    });
  }

  int indexFormularios = 0;

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
        onWillPop: _onBackPressed,
        child: Scaffold(
          body: SafeArea(
              child: Container(
            color: Color.fromRGBO(255, 255, 255, 1),
            child: ListView(
              children: <Widget>[
                widgetTitulo(),
                if (mapa == true) widgetMapa(),
                if (mapa == false) widgetFormulario(),
                widgetBotones(),
                widgetProgress()
              ],
            ),
          )),
        ));
  }

  widgetTitulo() {
    return Container(
        margin: const EdgeInsets.only(top: 5.0, bottom: 50.0),
        child: RichText(
          textAlign: TextAlign.center,
          text: TextSpan(
            text:
                "SECRETARÍA DE SALUD PÚBLICA MUNICIPAL \n BÚSQUEDA COMUNITARIA DE CASOS SOSPECHOSOS DE COVID-19",
            style:
                TextStyle(color: Color.fromRGBO(193, 29, 27, 1), fontSize: 20),
          ),
        ));
  }

  widgetFormulario() {
    //Informacion encuestado
    if (indexFormularios == 0)
      return Container(
          child:
              Column(crossAxisAlignment: CrossAxisAlignment.start, children: <
                  Widget>[
        RichText(
          textAlign: TextAlign.center,
          text: TextSpan(
            text: "Información del encuestado",
            style:
                TextStyle(color: Color.fromRGBO(193, 29, 27, 1), fontSize: 20),
          ),
        ),
        Text(definiciones[0]),
        Container(
            padding: const EdgeInsets.only(bottom: 20.0, right: 10.0),
            child: TextFormField(
              controller: _nombreDelEncuestadoController,
              decoration: const InputDecoration(
                hintText: 'Nombre del encuestado',
                border: const OutlineInputBorder(),
                filled: true,
                fillColor: Colors.white,
              ),
            )),
        Container(
            padding: const EdgeInsets.only(bottom: 20.0, right: 10.0),
            child: TextFormField(
              controller: _apellidosDelEncuestadoController,
              decoration: const InputDecoration(
                hintText: 'Apellidos del encuestado',
                border: const OutlineInputBorder(),
                filled: true,
                fillColor: Colors.white,
              ),
            )),
        Text(definiciones[1]),
        Container(
            height: 60.0,
            padding: const EdgeInsets.only(left: 10.0, top: 5.0, right: 10.0),
            margin: const EdgeInsets.only(bottom: 20.0, right: 10.0),
            decoration: const BoxDecoration(
              border: Border(
                top: BorderSide(
                  width: 1,
                  color: Color.fromRGBO(120, 120, 120, 1),
                ),
                left: BorderSide(
                  width: 1,
                  color: Color.fromRGBO(120, 120, 120, 1),
                ),
                right: BorderSide(
                  width: 1,
                  color: Color.fromRGBO(120, 120, 120, 1),
                ),
                bottom: BorderSide(
                  width: 1,
                  color: Color.fromRGBO(120, 120, 120, 1),
                ),
              ),
              borderRadius: BorderRadius.all(
                  Radius.circular(5.0) //         <--- border radius here
                  ),
            ),
            child: DropdownButton<String>(
              isExpanded: true,
              value: dropdownValueIdentificacion,
              icon: Icon(Icons.arrow_downward),
              iconSize: 30,
              style: TextStyle(
                  color: Color.fromRGBO(120, 120, 120, 1), fontSize: 15),
              onChanged: (String newValue) async {
                DatabaseIndenfiticacion identificacionSinc =
                    DatabaseIndenfiticacion.instance;
                Indenfiticacion getIndenfiticacion =
                    await identificacionSinc.queryIndenfiticacionId(newValue);
                if (getIndenfiticacion != null) {
                  setState(() {
                    dropdownValueIdentificacion = newValue;
                    identificacion = getIndenfiticacion.idIndenfiticacion;
                  });
                }
              },
              items: <String>[
                for (var i = 0; i < ident.length; i++) ident[i],
              ].map<DropdownMenuItem<String>>((String value) {
                return DropdownMenuItem<String>(
                  value: value,
                  child: Text(value),
                );
              }).toList(),
            )),
        Text(definiciones[2]),
        Container(
            padding: const EdgeInsets.only(bottom: 20.0, right: 10.0),
            child: TextFormField(
              controller: _numeroDeIdentificacionController,
              decoration: const InputDecoration(
                hintText: 'Número de Identificacion',
                border: const OutlineInputBorder(),
                filled: true,
                fillColor: Colors.white,
              ),
              keyboardType: TextInputType.number,
            )),
        Text(definiciones[3]),
        Container(
            padding: const EdgeInsets.only(bottom: 20.0, right: 10.0),
            child: TextFormField(
              controller: _telefonoController,
              decoration: const InputDecoration(
                hintText: 'Telefono',
                border: const OutlineInputBorder(),
                filled: true,
                fillColor: Colors.white,
              ),
            )),
        Column(children: <Widget>[
          Container(
              alignment: Alignment.centerLeft,
              child: RichText(
                textAlign: TextAlign.left,
                text: TextSpan(
                  text: "¿Vive en Candelaria y sus areas de influencia?",
                  style: TextStyle(
                      color: Color.fromRGBO(0, 0, 0, 1), fontSize: 18),
                ),
              )),
          Container(
              child: Row(children: <Widget>[
            new Text(
              'No',
              style: new TextStyle(fontSize: 16.0),
            ),
            new Radio(
              value: 0,
              groupValue: radioValueViveCandelaria,
              onChanged: _handleRadioValueChangeViveCandelaria,
            ),
            new Text(
              'Si',
              style: new TextStyle(fontSize: 16.0),
            ),
            new Radio(
              value: 1,
              groupValue: radioValueViveCandelaria,
              onChanged: _handleRadioValueChangeViveCandelaria,
            ),
          ])),
        ])
      ]));

    //Trabaja en candelaria?
    if (indexFormularios == 1 && viveCandelaria == false)
      return Container(
          child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
            Column(children: <Widget>[
              Container(
                  alignment: Alignment.centerLeft,
                  child: RichText(
                    textAlign: TextAlign.left,
                    text: TextSpan(
                      text: "¿Trabaja en Candelaria?",
                      style: TextStyle(
                          color: Color.fromRGBO(0, 0, 0, 1), fontSize: 18),
                    ),
                  )),
              Container(
                  child: Row(children: <Widget>[
                new Text(
                  'No',
                  style: new TextStyle(fontSize: 16.0),
                ),
                new Radio(
                  value: 0,
                  groupValue: radioValueTrabajaCandelaria,
                  onChanged: _handleRadioValueChangeTrabajaCandelaria,
                ),
                new Text(
                  'Si',
                  style: new TextStyle(fontSize: 16.0),
                ),
                new Radio(
                  value: 1,
                  groupValue: radioValueTrabajaCandelaria,
                  onChanged: _handleRadioValueChangeTrabajaCandelaria,
                ),
              ])),
            ])
          ]));

    //VISITANTES

    if ((indexFormularios == 2 &&
        viveCandelaria == false &&
        trabajaCandelaria == false))
      return Container(
          child:
              Column(crossAxisAlignment: CrossAxisAlignment.start, children: <
                  Widget>[
        Text(definiciones[5]),
        Container(
            height: 60.0,
            padding: const EdgeInsets.only(left: 10.0, top: 5.0, right: 10.0),
            margin: const EdgeInsets.only(bottom: 20.0, right: 10.0),
            decoration: const BoxDecoration(
              border: Border(
                top: BorderSide(
                  width: 1,
                  color: Color.fromRGBO(120, 120, 120, 1),
                ),
                left: BorderSide(
                  width: 1,
                  color: Color.fromRGBO(120, 120, 120, 1),
                ),
                right: BorderSide(
                  width: 1,
                  color: Color.fromRGBO(120, 120, 120, 1),
                ),
                bottom: BorderSide(
                  width: 1,
                  color: Color.fromRGBO(120, 120, 120, 1),
                ),
              ),
              borderRadius: BorderRadius.all(
                  Radius.circular(5.0) //         <--- border radius here
                  ),
            ),
            child: DropdownButton<String>(
              isExpanded: true,
              value: dropdownValueDepartamento,
              icon: Icon(Icons.arrow_downward),
              iconSize: 30,
              style: TextStyle(
                  color: Color.fromRGBO(120, 120, 120, 1), fontSize: 15),
              onChanged: (String newValue) async {
                DatabaseDepartamentos departamentoSinc =
                    DatabaseDepartamentos.instance;
                Departamentos getDepartamentos =
                    await departamentoSinc.queryDepartamentosId(newValue);
                if (getDepartamentos != null) {
                  setState(() {
                    dropdownValueDepartamento = newValue;
                    departamento = getDepartamentos.idDepartamentos;
                    getMunicipio(departamento);
                    _progress = 0;
                    startTimer();
                  });
                }
              },
              items: <String>[
                for (var i = 0; i < depart.length; i++) depart[i],
              ].map<DropdownMenuItem<String>>((String value) {
                return DropdownMenuItem<String>(
                  value: value,
                  child: Text(value),
                );
              }).toList(),
            )),

            if (_progress != 1 && departamento != 0) 
      Padding(
            padding: EdgeInsets.only(bottom:20.0, left: 250.0, right: 250.0 ),
          child: CircularProgressIndicator(
            strokeWidth: 10,
            backgroundColor: Colors.cyanAccent,                                            
            valueColor: new AlwaysStoppedAnimation<Color>(Colors.red),
            value: _progress,
          )),
          if (_progress == 1) 
        Text(definiciones[6]),
        if (_progress == 1) 
        Container(
            height: 60.0,
            padding: const EdgeInsets.only(left: 10.0, top: 5.0, right: 10.0),
            margin: const EdgeInsets.only(bottom: 20.0, right: 10.0),
            decoration: const BoxDecoration(
              border: Border(
                top: BorderSide(
                  width: 1,
                  color: Color.fromRGBO(120, 120, 120, 1),
                ),
                left: BorderSide(
                  width: 1,
                  color: Color.fromRGBO(120, 120, 120, 1),
                ),
                right: BorderSide(
                  width: 1,
                  color: Color.fromRGBO(120, 120, 120, 1),
                ),
                bottom: BorderSide(
                  width: 1,
                  color: Color.fromRGBO(120, 120, 120, 1),
                ),
              ),
              borderRadius: BorderRadius.all(
                  Radius.circular(5.0) //         <--- border radius here
                  ),
            ),
            child: DropdownButton<String>(
              isExpanded: true,
              value: dropdownValueMunicipio,
              icon: Icon(Icons.arrow_downward),
              iconSize: 30,
              style: TextStyle(
                  color: Color.fromRGBO(120, 120, 120, 1), fontSize: 15),
              onChanged: (String newValue) async {
                DatabaseMunicipios municipioSinc = DatabaseMunicipios.instance;
                Municipios getMunicipio =
                    await municipioSinc.queryMunicipiosId(newValue);
                if (getMunicipio != null) {
                  setState(() {
                    dropdownValueMunicipio = newValue;
                    municipio = getMunicipio.idMunicipios;
                    print(municipio);
                  });
                }
              },
              items: <String>[
                for (var i = 0; i < municip.length; i++) municip[i],
              ].map<DropdownMenuItem<String>>((String value) {
                return DropdownMenuItem<String>(
                  value: value,
                  child: Text(value),
                );
              }).toList(),
            )),
        Text("Frecuencia mensual de visita"),
        Container(
            padding: const EdgeInsets.only(bottom: 20.0, right: 10.0),
            child: TextFormField(
              controller: _frecuenciaMensualController,
              decoration: const InputDecoration(
                hintText: 'Número de meses',
                border: const OutlineInputBorder(),
                filled: true,
                fillColor: Colors.white,
              ),
            )),
        Text("Lugar que frecuenta"),
        Container(
            padding: const EdgeInsets.only(bottom: 20.0, right: 10.0),
            child: TextFormField(
              controller: _lugarFrecuentaController,
              decoration: const InputDecoration(
                hintText: 'Lugar que frecuenta',
                border: const OutlineInputBorder(),
                filled: true,
                fillColor: Colors.white,
              ),
            )),
        Text("Motivo de la visita"),
        Container(
            padding: const EdgeInsets.only(bottom: 20.0, right: 10.0),
            child: TextFormField(
              controller: _motivoVisitaController,
              decoration: const InputDecoration(
                hintText: 'Motivo de la visita',
                border: const OutlineInputBorder(),
                filled: true,
                fillColor: Colors.white,
              ),
            )),
        Text("Tiempo estimada en horas en cada visita"),
        Container(
            padding: const EdgeInsets.only(bottom: 20.0, right: 10.0),
            child: TextFormField(
              controller: _tiempoVisitaController,
              decoration: const InputDecoration(
                hintText: 'Horas estimadas',
                border: const OutlineInputBorder(),
                filled: true,
                fillColor: Colors.white,
              ),
              keyboardType: TextInputType.number,
            )),
      ]));

//   TERRITORIO

    if (indexFormularios == 1 && viveCandelaria == true)
      return Container(
          child:
              Column(crossAxisAlignment: CrossAxisAlignment.start, children: <
                  Widget>[
        RichText(
          textAlign: TextAlign.center,
          text: TextSpan(
            text: "Territorio",
            style:
                TextStyle(color: Color.fromRGBO(193, 29, 27, 1), fontSize: 20),
          ),
        ),
        Text(definiciones[4]),
        Container(
            padding: const EdgeInsets.only(bottom: 20.0, right: 10.0),
            child: TextFormField(
              controller: _direccionController,
              decoration: const InputDecoration(
                hintText: 'Direccion',
                border: const OutlineInputBorder(),
                filled: true,
                fillColor: Colors.white,
              ),
            )),
        Text(definiciones[7]),
        Container(
            height: 60.0,
            padding: const EdgeInsets.only(left: 10.0, top: 5.0, right: 10.0),
            margin: const EdgeInsets.only(bottom: 20.0, right: 10.0),
            decoration: const BoxDecoration(
              border: Border(
                top: BorderSide(
                  width: 1,
                  color: Color.fromRGBO(120, 120, 120, 1),
                ),
                left: BorderSide(
                  width: 1,
                  color: Color.fromRGBO(120, 120, 120, 1),
                ),
                right: BorderSide(
                  width: 1,
                  color: Color.fromRGBO(120, 120, 120, 1),
                ),
                bottom: BorderSide(
                  width: 1,
                  color: Color.fromRGBO(120, 120, 120, 1),
                ),
              ),
              borderRadius: BorderRadius.all(
                  Radius.circular(5.0) //         <--- border radius here
                  ),
            ),
            child: DropdownButton<String>(
              isExpanded: true,
              value: dropdownValueCorregimiento,
              icon: Icon(Icons.arrow_downward),
              iconSize: 30,
              style: TextStyle(
                  color: Color.fromRGBO(120, 120, 120, 1), fontSize: 15),
              onChanged: (String newValue) async {
                DatabaseCorregimientos corregimientoSinc =
                    DatabaseCorregimientos.instance;
                Corregimientos getCorregimiento =
                    await corregimientoSinc.queryCorregimientosId(newValue);
                if (getCorregimiento != null) {
                  setState(() {
                    dropdownValueCorregimiento = newValue;
                    corregimiento = getCorregimiento.idCorregimientos;
                  });
                }
              },
              items: <String>[
                for (var i = 0; i < corregi.length; i++) corregi[i],
              ].map<DropdownMenuItem<String>>((String value) {
                return DropdownMenuItem<String>(
                  value: value,
                  child: Text(value),
                );
              }).toList(),
            )),
        Text(definiciones[8]),
        Container(
            height: 60.0,
            padding: const EdgeInsets.only(left: 10.0, top: 5.0, right: 10.0),
            margin: const EdgeInsets.only(bottom: 20.0, right: 10.0),
            decoration: const BoxDecoration(
              border: Border(
                top: BorderSide(
                  width: 1,
                  color: Color.fromRGBO(120, 120, 120, 1),
                ),
                left: BorderSide(
                  width: 1,
                  color: Color.fromRGBO(120, 120, 120, 1),
                ),
                right: BorderSide(
                  width: 1,
                  color: Color.fromRGBO(120, 120, 120, 1),
                ),
                bottom: BorderSide(
                  width: 1,
                  color: Color.fromRGBO(120, 120, 120, 1),
                ),
              ),
              borderRadius: BorderRadius.all(
                  Radius.circular(5.0) //         <--- border radius here
                  ),
            ),
            child: DropdownButton<String>(
              isExpanded: true,
              value: dropdownValueVereda,
              icon: Icon(Icons.arrow_downward),
              iconSize: 30,
              style: TextStyle(
                  color: Color.fromRGBO(120, 120, 120, 1), fontSize: 15),
              onChanged: (String newValue) async {
                DatabaseVeredas veredaSinc = DatabaseVeredas.instance;
                Veredas getVeredas = await veredaSinc.queryVeredasId(newValue);
                if (getVeredas != null) {
                  setState(() {
                    dropdownValueVereda = newValue;
                    vereda = getVeredas.idVeredas;
                  });
                }
              },
              items: <String>[
                for (var i = 0; i < vered.length; i++) vered[i],
              ].map<DropdownMenuItem<String>>((String value) {
                return DropdownMenuItem<String>(
                  value: value,
                  child: Text(value),
                );
              }).toList(),
            )),
        Text(definiciones[9]),
        Container(
            height: 60.0,
            padding: const EdgeInsets.only(left: 10.0, top: 5.0, right: 10.0),
            margin: const EdgeInsets.only(bottom: 20.0, right: 10.0),
            decoration: const BoxDecoration(
              border: Border(
                top: BorderSide(
                  width: 1,
                  color: Color.fromRGBO(120, 120, 120, 1),
                ),
                left: BorderSide(
                  width: 1,
                  color: Color.fromRGBO(120, 120, 120, 1),
                ),
                right: BorderSide(
                  width: 1,
                  color: Color.fromRGBO(120, 120, 120, 1),
                ),
                bottom: BorderSide(
                  width: 1,
                  color: Color.fromRGBO(120, 120, 120, 1),
                ),
              ),
              borderRadius: BorderRadius.all(
                  Radius.circular(5.0) //         <--- border radius here
                  ),
            ),
            child: DropdownButton<String>(
              isExpanded: true,
              value: dropdownValueCallejon,
              icon: Icon(Icons.arrow_downward),
              iconSize: 30,
              style: TextStyle(
                  color: Color.fromRGBO(120, 120, 120, 1), fontSize: 15),
              onChanged: (String newValue) async {
                DatabaseCallejon callejonSinc = DatabaseCallejon.instance;
                Callejon getCallejon =
                    await callejonSinc.queryCallejonId(newValue);
                if (getCallejon != null) {
                  setState(() {
                    dropdownValueCallejon = newValue;
                    callejon = getCallejon.idCallejon;
                  });
                }
              },
              items: <String>[
                for (var i = 0; i < callej.length; i++) callej[i],
              ].map<DropdownMenuItem<String>>((String value) {
                return DropdownMenuItem<String>(
                  value: value,
                  child: Text(value),
                );
              }).toList(),
            )),
        Text(definiciones[10]),
        Container(
            height: 60.0,
            padding: const EdgeInsets.only(left: 10.0, top: 5.0, right: 10.0),
            margin: const EdgeInsets.only(bottom: 20.0, right: 10.0),
            decoration: const BoxDecoration(
              border: Border(
                top: BorderSide(
                  width: 1,
                  color: Color.fromRGBO(120, 120, 120, 1),
                ),
                left: BorderSide(
                  width: 1,
                  color: Color.fromRGBO(120, 120, 120, 1),
                ),
                right: BorderSide(
                  width: 1,
                  color: Color.fromRGBO(120, 120, 120, 1),
                ),
                bottom: BorderSide(
                  width: 1,
                  color: Color.fromRGBO(120, 120, 120, 1),
                ),
              ),
              borderRadius: BorderRadius.all(
                  Radius.circular(5.0) //         <--- border radius here
                  ),
            ),
            child: DropdownButton<String>(
              isExpanded: true,
              value: dropdownValueBarrio,
              icon: Icon(Icons.arrow_downward),
              iconSize: 30,
              style: TextStyle(
                  color: Color.fromRGBO(120, 120, 120, 1), fontSize: 15),
              onChanged: (String newValue) async {
                DatabaseBarrios barrioSinc = DatabaseBarrios.instance;
                Barrios getBarrios = await barrioSinc.queryBarriosId(newValue);
                if (getBarrios != null) {
                  setState(() {
                    dropdownValueBarrio = newValue;
                    barrio = getBarrios.idBarrios;
                  });
                }
              },
              items: <String>[
                for (var i = 0; i < barri.length; i++) barri[i],
              ].map<DropdownMenuItem<String>>((String value) {
                return DropdownMenuItem<String>(
                  value: value,
                  child: Text(value),
                );
              }).toList(),
            )),
      ]));

    /////// OCUPACION
    ///
    ///
    ///

//Ocupacion
    if ((indexFormularios == 2 && viveCandelaria == true && mapa == false) ||
        (indexFormularios == 2 &&
            viveCandelaria == false &&
            trabajaCandelaria == true))
      return Container(
          child:
              Column(crossAxisAlignment: CrossAxisAlignment.start, children: <
                  Widget>[
        RichText(
          textAlign: TextAlign.center,
          text: TextSpan(
            text: "Ocupación",
            style:
                TextStyle(color: Color.fromRGBO(193, 29, 27, 1), fontSize: 20),
          ),
        ),
        Text(definiciones[16]),
        Container(
            height: 60.0,
            padding: const EdgeInsets.only(left: 10.0, top: 5.0, right: 10.0),
            margin: const EdgeInsets.only(bottom: 20.0, right: 10.0),
            decoration: const BoxDecoration(
              border: Border(
                top: BorderSide(
                  width: 1,
                  color: Color.fromRGBO(120, 120, 120, 1),
                ),
                left: BorderSide(
                  width: 1,
                  color: Color.fromRGBO(120, 120, 120, 1),
                ),
                right: BorderSide(
                  width: 1,
                  color: Color.fromRGBO(120, 120, 120, 1),
                ),
                bottom: BorderSide(
                  width: 1,
                  color: Color.fromRGBO(120, 120, 120, 1),
                ),
              ),
              borderRadius: BorderRadius.all(
                  Radius.circular(5.0) //         <--- border radius here
                  ),
            ),
            child: DropdownButton<String>(
              isExpanded: true,
              value: dropdownValueOcupacion,
              icon: Icon(Icons.arrow_downward),
              iconSize: 30,
              style: TextStyle(
                  color: Color.fromRGBO(120, 120, 120, 1), fontSize: 15),
              onChanged: (String newValue) async {
                DatabaseOcupaciones ocupacionSinc =
                    DatabaseOcupaciones.instance;
                Ocupaciones getOcupaciones =
                    await ocupacionSinc.queryOcupacionesId(newValue);
                if (getOcupaciones != null) {
                  setState(() {
                    dropdownValueOcupacion = newValue;
                    ocupacion = getOcupaciones.idOcupaciones;
                  });
                }
              },
              items: <String>[
                for (var i = 0; i < ocup.length; i++) ocup[i],
              ].map<DropdownMenuItem<String>>((String value) {
                return DropdownMenuItem<String>(
                  value: value,
                  child: Text(value),
                );
              }).toList(),
            )),
        Text(definiciones[17]),
        Container(
            height: 60.0,
            padding: const EdgeInsets.only(left: 10.0, top: 5.0, right: 10.0),
            margin: const EdgeInsets.only(bottom: 20.0, right: 10.0),
            decoration: const BoxDecoration(
              border: Border(
                top: BorderSide(
                  width: 1,
                  color: Color.fromRGBO(120, 120, 120, 1),
                ),
                left: BorderSide(
                  width: 1,
                  color: Color.fromRGBO(120, 120, 120, 1),
                ),
                right: BorderSide(
                  width: 1,
                  color: Color.fromRGBO(120, 120, 120, 1),
                ),
                bottom: BorderSide(
                  width: 1,
                  color: Color.fromRGBO(120, 120, 120, 1),
                ),
              ),
              borderRadius: BorderRadius.all(
                  Radius.circular(5.0) //         <--- border radius here
                  ),
            ),
            child: DropdownButton<String>(
              isExpanded: true,
              value: dropdownValueTrabajo,
              icon: Icon(Icons.arrow_downward),
              iconSize: 30,
              style: TextStyle(
                  color: Color.fromRGBO(120, 120, 120, 1), fontSize: 15),
              onChanged: (String newValue) async {
                DatabaseTrabajos trabajosSinc = DatabaseTrabajos.instance;
                Trabajos getTrabajos =
                    await trabajosSinc.queryTrabajosId(newValue);
                if (getTrabajos != null) {
                  setState(() {
                    dropdownValueTrabajo = newValue;
                    trabajador = getTrabajos.idTrabajos;
                  });
                }
              },
              items: <String>[
                for (var i = 0; i < trabaj.length; i++) trabaj[i],
              ].map<DropdownMenuItem<String>>((String value) {
                return DropdownMenuItem<String>(
                  value: value,
                  child: Text(value),
                );
              }).toList(),
            )),
        Text(definiciones[18]),
        Container(
            padding: const EdgeInsets.only(bottom: 20.0, right: 10.0),
            child: TextFormField(
              controller: _nombreEmpresaController,
              decoration: const InputDecoration(
                hintText: 'Empresa',
                border: const OutlineInputBorder(),
                filled: true,
                fillColor: Colors.white,
              ),
            )),
        Text(definiciones[19]),
        Container(
            padding: const EdgeInsets.only(bottom: 20.0, right: 10.0),
            child: TextFormField(
              controller: _direccionEmpresaController,
              decoration: const InputDecoration(
                hintText: 'Dirección de la empresa o lugar de trabajo',
                border: const OutlineInputBorder(),
                filled: true,
                fillColor: Colors.white,
              ),
            )),
      ]));

///////////////////////////
    ///
    ///
    ///
///////////////////////////

//CARACTERISTICAS

    if ((indexFormularios == 3 && viveCandelaria == true) ||
        (indexFormularios == 3 &&
            viveCandelaria == false &&
            trabajaCandelaria == true)) {
      return Container(
          child:
              Column(crossAxisAlignment: CrossAxisAlignment.start, children: <
                  Widget>[
        RichText(
          textAlign: TextAlign.center,
          text: TextSpan(
            text: "Características Sociodemográficas",
            style:
                TextStyle(color: Color.fromRGBO(193, 29, 27, 1), fontSize: 20),
          ),
        ),
        Text(definiciones[11]),
        Container(
            height: 60.0,
            padding: const EdgeInsets.only(left: 10.0, top: 5.0, right: 10.0),
            margin: const EdgeInsets.only(bottom: 20.0, right: 10.0),
            decoration: const BoxDecoration(
              border: Border(
                top: BorderSide(
                  width: 1,
                  color: Color.fromRGBO(120, 120, 120, 1),
                ),
                left: BorderSide(
                  width: 1,
                  color: Color.fromRGBO(120, 120, 120, 1),
                ),
                right: BorderSide(
                  width: 1,
                  color: Color.fromRGBO(120, 120, 120, 1),
                ),
                bottom: BorderSide(
                  width: 1,
                  color: Color.fromRGBO(120, 120, 120, 1),
                ),
              ),
              borderRadius: BorderRadius.all(
                  Radius.circular(5.0) //         <--- border radius here
                  ),
            ),
            child: DropdownButton<String>(
              isExpanded: true,
              value: dropdownValueSexo,
              icon: Icon(Icons.arrow_downward),
              iconSize: 30,
              style: TextStyle(
                  color: Color.fromRGBO(120, 120, 120, 1), fontSize: 15),
              onChanged: (String newValue) async {
                DatabaseSexo sexoSinc = DatabaseSexo.instance;
                Sexo getSexo = await sexoSinc.querySexoId(newValue);
                if (getSexo != null) {
                  setState(() {
                    dropdownValueSexo = newValue;
                    sexo = getSexo.idSexo;
                  });
                }
              },
              items: <String>[
                for (var i = 0; i < sex.length; i++) sex[i],
              ].map<DropdownMenuItem<String>>((String value) {
                return DropdownMenuItem<String>(
                  value: value,
                  child: Text(value),
                );
              }).toList(),
            )),
        Text(definiciones[12]),
        if (fechaNac == null)
          InkWell(
              onTap: () {
                showDatePicker(
                        context: context,
                        initialDate: DateTime.now(),
                        firstDate: DateTime(1920),
                        lastDate: DateTime.now())
                    .then((date) {
                  setState(() {
                    fechaNac = date;
                    fechaView = DateFormat('yyyy-MM-dd').format(fechaNac);
                  });
                });
              },
              child: Container(
                padding: const EdgeInsets.only(left: 10.0, top: 18.0),
                margin: const EdgeInsets.only(bottom: 20.0, right: 10.0),
                width: 730,
                height: 60.0,
                decoration: const BoxDecoration(
                  border: Border(
                    top: BorderSide(width: 0.8, color: Color(0xFFFF000000)),
                    left: BorderSide(width: 0.8, color: Color(0xFFFF000000)),
                    right: BorderSide(width: 0.8, color: Color(0xFFFF000000)),
                    bottom: BorderSide(width: 0.8, color: Color(0xFFFF000000)),
                  ),
                  borderRadius: BorderRadius.all(
                      Radius.circular(5.0) //         <--- border radius here
                      ),
                ),
                child: RichText(
                  textAlign: TextAlign.left,
                  text: TextSpan(
                    text: "Fecha de nacimiento",
                    style: TextStyle(
                        color: Color.fromRGBO(120, 120, 120, 1), fontSize: 18),
                  ),
                ),
              )),
        if (fechaNac != null)
          InkWell(
              onTap: () {
                showDatePicker(
                        context: context,
                        initialDate: DateTime.now(),
                        firstDate: DateTime(1920),
                        lastDate: DateTime.now())
                    .then((date) {
                  setState(() {
                    fechaNac = date;
                    fechaView = DateFormat('yyyy-MM-dd').format(fechaNac);
                  });
                });
              },
              child: Container(
                padding: const EdgeInsets.only(left: 10.0, top: 18.0),
                margin: const EdgeInsets.only(bottom: 20.0, right: 10.0),
                width: 730,
                height: 60.0,
                decoration: const BoxDecoration(
                  border: Border(
                    top: BorderSide(width: 0.8, color: Color(0xFFFF000000)),
                    left: BorderSide(width: 0.8, color: Color(0xFFFF000000)),
                    right: BorderSide(width: 0.8, color: Color(0xFFFF000000)),
                    bottom: BorderSide(width: 0.8, color: Color(0xFFFF000000)),
                  ),
                  borderRadius: BorderRadius.all(
                      Radius.circular(5.0) //         <--- border radius here
                      ),
                ),
                child: RichText(
                  textAlign: TextAlign.left,
                  text: TextSpan(
                    text: fechaView,
                    style: TextStyle(
                        color: Color.fromRGBO(120, 120, 120, 1), fontSize: 18),
                  ),
                ),
              )),
        Text(definiciones[13]),
        Container(
            height: 60.0,
            padding: const EdgeInsets.only(left: 10.0, top: 5.0, right: 10.0),
            margin: const EdgeInsets.only(bottom: 20.0, right: 10.0),
            decoration: const BoxDecoration(
              border: Border(
                top: BorderSide(
                  width: 1,
                  color: Color.fromRGBO(120, 120, 120, 1),
                ),
                left: BorderSide(
                  width: 1,
                  color: Color.fromRGBO(120, 120, 120, 1),
                ),
                right: BorderSide(
                  width: 1,
                  color: Color.fromRGBO(120, 120, 120, 1),
                ),
                bottom: BorderSide(
                  width: 1,
                  color: Color.fromRGBO(120, 120, 120, 1),
                ),
              ),
              borderRadius: BorderRadius.all(
                  Radius.circular(5.0) //         <--- border radius here
                  ),
            ),
            child: DropdownButton<String>(
              isExpanded: true,
              value: dropdownValueEtnia,
              icon: Icon(Icons.arrow_downward),
              iconSize: 30,
              style: TextStyle(
                  color: Color.fromRGBO(120, 120, 120, 1), fontSize: 15),
              onChanged: (String newValue) async {
                DatabaseEtnias etniaSinc = DatabaseEtnias.instance;
                Etnias getEtnias = await etniaSinc.queryEtniasId(newValue);
                if (getEtnias != null) {
                  setState(() {
                    dropdownValueEtnia = newValue;
                    etnia = getEtnias.idEtnias;
                  });
                }
              },
              items: <String>[
                for (var i = 0; i < etn.length; i++) etn[i],
              ].map<DropdownMenuItem<String>>((String value) {
                return DropdownMenuItem<String>(
                  value: value,
                  child: Text(value),
                );
              }).toList(),
            )),
        Text(definiciones[14]),
        Container(
            height: 60.0,
            padding: const EdgeInsets.only(left: 10.0, top: 5.0, right: 10.0),
            margin: const EdgeInsets.only(bottom: 20.0, right: 10.0),
            decoration: const BoxDecoration(
              border: Border(
                top: BorderSide(
                  width: 1,
                  color: Color.fromRGBO(120, 120, 120, 1),
                ),
                left: BorderSide(
                  width: 1,
                  color: Color.fromRGBO(120, 120, 120, 1),
                ),
                right: BorderSide(
                  width: 1,
                  color: Color.fromRGBO(120, 120, 120, 1),
                ),
                bottom: BorderSide(
                  width: 1,
                  color: Color.fromRGBO(120, 120, 120, 1),
                ),
              ),
              borderRadius: BorderRadius.all(
                  Radius.circular(5.0) //         <--- border radius here
                  ),
            ),
            child: DropdownButton<String>(
              isExpanded: true,
              value: dropdownValueGrupo,
              icon: Icon(Icons.arrow_downward),
              iconSize: 30,
              style: TextStyle(
                  color: Color.fromRGBO(120, 120, 120, 1), fontSize: 15),
              onChanged: (String newValue) async {
                DatabasePoblacional poblacionalSinc =
                    DatabasePoblacional.instance;
                Poblacional getPoblacional =
                    await poblacionalSinc.queryPoblacionalId(newValue);
                if (getPoblacional != null) {
                  setState(() {
                    dropdownValueGrupo = newValue;
                    grupo = getPoblacional.idPoblacional;
                  });
                }
              },
              items: <String>[
                for (var i = 0; i < pobla.length; i++) pobla[i],
              ].map<DropdownMenuItem<String>>((String value) {
                return DropdownMenuItem<String>(
                  value: value,
                  child: Text(value),
                );
              }).toList(),
            )),
      ]));
    }

    ////
    ///
    ///
    ///
    ///VULNERABLE
    if ((indexFormularios == 4 && viveCandelaria == true) ||
        (indexFormularios == 4 &&
            viveCandelaria == false &&
            trabajaCandelaria == true)) {
      return Container(
          child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
            RichText(
              textAlign: TextAlign.center,
              text: TextSpan(
                text: "Personas con mayor vulnerabilidad para infectarse",
                style: TextStyle(
                    color: Color.fromRGBO(193, 29, 27, 1), fontSize: 20),
              ),
            ),
            Text(definiciones[15]),
            Container(
                height: 60.0,
                padding:
                    const EdgeInsets.only(left: 10.0, top: 5.0, right: 10.0),
                margin: const EdgeInsets.only(bottom: 20.0, right: 10.0),
                decoration: const BoxDecoration(
                  border: Border(
                    top: BorderSide(
                      width: 1,
                      color: Color.fromRGBO(120, 120, 120, 1),
                    ),
                    left: BorderSide(
                      width: 1,
                      color: Color.fromRGBO(120, 120, 120, 1),
                    ),
                    right: BorderSide(
                      width: 1,
                      color: Color.fromRGBO(120, 120, 120, 1),
                    ),
                    bottom: BorderSide(
                      width: 1,
                      color: Color.fromRGBO(120, 120, 120, 1),
                    ),
                  ),
                  borderRadius: BorderRadius.all(
                      Radius.circular(5.0) //         <--- border radius here
                      ),
                ),
                child: DropdownButton<String>(
                  isExpanded: true,
                  value: dropdownValueVulnerabilidad,
                  icon: Icon(Icons.arrow_downward),
                  iconSize: 30,
                  style: TextStyle(
                      color: Color.fromRGBO(120, 120, 120, 1), fontSize: 15),
                  onChanged: (String newValue) async {
                    DatabaseVulnerables vulnerableSinc =
                        DatabaseVulnerables.instance;
                    Vulnerables getVulnerables =
                        await vulnerableSinc.queryVulnerablesId(newValue);
                    if (getVulnerables != null) {
                      setState(() {
                        dropdownValueVulnerabilidad = newValue;
                        vulnerable = getVulnerables.idVulnerables;
                      });
                    }
                  },
                  items: <String>[
                    for (var i = 0; i < vulne.length; i++) vulne[i],
                  ].map<DropdownMenuItem<String>>((String value) {
                    return DropdownMenuItem<String>(
                      value: value,
                      child: Text(value),
                    );
                  }).toList(),
                )),
          ]));
    }

    /////////////////////
    ///
    ///
    ////////////////////

    ///Aseguramiento en salud
    if ((indexFormularios == 5 && viveCandelaria == true) ||
        (indexFormularios == 5 &&
            viveCandelaria == false &&
            trabajaCandelaria == true)) {
      return Container(
          child:
              Column(crossAxisAlignment: CrossAxisAlignment.start, children: <
                  Widget>[
        RichText(
          textAlign: TextAlign.center,
          text: TextSpan(
            text: "Aseguramiento en salud",
            style:
                TextStyle(color: Color.fromRGBO(193, 29, 27, 1), fontSize: 20),
          ),
        ),
        Text(definiciones[20]),
        Container(
            height: 60.0,
            padding: const EdgeInsets.only(left: 10.0, top: 5.0, right: 10.0),
            margin: const EdgeInsets.only(bottom: 20.0, right: 10.0),
            decoration: const BoxDecoration(
              border: Border(
                top: BorderSide(
                  width: 1,
                  color: Color.fromRGBO(120, 120, 120, 1),
                ),
                left: BorderSide(
                  width: 1,
                  color: Color.fromRGBO(120, 120, 120, 1),
                ),
                right: BorderSide(
                  width: 1,
                  color: Color.fromRGBO(120, 120, 120, 1),
                ),
                bottom: BorderSide(
                  width: 1,
                  color: Color.fromRGBO(120, 120, 120, 1),
                ),
              ),
              borderRadius: BorderRadius.all(
                  Radius.circular(5.0) //         <--- border radius here
                  ),
            ),
            child: DropdownButton<String>(
              isExpanded: true,
              value: dropdownValueEPS,
              icon: Icon(Icons.arrow_downward),
              iconSize: 30,
              style: TextStyle(
                  color: Color.fromRGBO(120, 120, 120, 1), fontSize: 15),
              onChanged: (String newValue) async {
                DatabaseEps epsSinc = DatabaseEps.instance;
                Eps getEps = await epsSinc.queryEpsId(newValue);
                if (getEps != null) {
                  setState(() {
                    dropdownValueEPS = newValue;
                    eps = getEps.idEps;
                  });
                }
              },
              items: <String>[
                for (var i = 0; i < ep.length; i++) ep[i],
              ].map<DropdownMenuItem<String>>((String value) {
                return DropdownMenuItem<String>(
                  value: value,
                  child: Text(value),
                );
              }).toList(),
            )),
        if (eps == 25) Text(definiciones[21]),
        if (eps == 25)
          Container(
              padding: const EdgeInsets.only(bottom: 20.0, right: 10.0),
              child: TextFormField(
                controller: _nombreOtraEPSController,
                decoration: const InputDecoration(
                  hintText: 'Otra EAPB',
                  border: const OutlineInputBorder(),
                  filled: true,
                  fillColor: Colors.white,
                ),
              )),
        Text(definiciones[22]),
        Container(
            height: 60.0,
            padding: const EdgeInsets.only(left: 10.0, top: 5.0, right: 10.0),
            margin: const EdgeInsets.only(bottom: 20.0, right: 10.0),
            decoration: const BoxDecoration(
              border: Border(
                top: BorderSide(
                  width: 1,
                  color: Color.fromRGBO(120, 120, 120, 1),
                ),
                left: BorderSide(
                  width: 1,
                  color: Color.fromRGBO(120, 120, 120, 1),
                ),
                right: BorderSide(
                  width: 1,
                  color: Color.fromRGBO(120, 120, 120, 1),
                ),
                bottom: BorderSide(
                  width: 1,
                  color: Color.fromRGBO(120, 120, 120, 1),
                ),
              ),
              borderRadius: BorderRadius.all(
                  Radius.circular(5.0) //         <--- border radius here
                  ),
            ),
            child: DropdownButton<String>(
              isExpanded: true,
              value: dropdownValueDificultadEPS,
              icon: Icon(Icons.arrow_downward),
              iconSize: 30,
              style: TextStyle(
                  color: Color.fromRGBO(120, 120, 120, 1), fontSize: 15),
              onChanged: (String newValue) async {
                DatabaseDificultadEps dificultadSinc =
                    DatabaseDificultadEps.instance;
                DificultadEps getDificultad =
                    await dificultadSinc.queryDificultadEpsId(newValue);
                if (getDificultad != null) {
                  setState(() {
                    dropdownValueDificultadEPS = newValue;
                    dificultadEps = getDificultad.idDificultadEps;
                  });
                }
              },
              items: <String>[
                for (var i = 0; i < difi.length; i++) difi[i],
              ].map<DropdownMenuItem<String>>((String value) {
                return DropdownMenuItem<String>(
                  value: value,
                  child: Text(value),
                );
              }).toList(),
            )),
      ]));
    }

    /////////////////////
    ///
    ///
    ////////////////////

//Factor de riesgo
    if ((indexFormularios == 6 && viveCandelaria == true) ||
        (indexFormularios == 6 &&
            viveCandelaria == false &&
            trabajaCandelaria == true)) {
      return Center(
          child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
            RichText(
              textAlign: TextAlign.center,
              text: TextSpan(
                text: "Factores de riesgo de enfermar gravemente de COVID-19",
                style: TextStyle(
                    color: Color.fromRGBO(193, 29, 27, 1), fontSize: 20),
              ),
            ),
            Text("Diabetes"),
            Container(
                child: Row(children: <Widget>[
              new Text(
                'No',
                style: new TextStyle(fontSize: 16.0),
              ),
              new Radio(
                value: 0,
                groupValue: radioValueDiabetes,
                onChanged: _handleRadioValueChangeDiabetes,
              ),
              new Text(
                'Si',
                style: new TextStyle(fontSize: 16.0),
              ),
              new Radio(
                value: 1,
                groupValue: radioValueDiabetes,
                onChanged: _handleRadioValueChangeDiabetes,
              ),
              /* new Text(
          'NO APLICA',
          style: new TextStyle(fontSize: 16.0),
        ),
        new Radio(
          value: 2,
          groupValue: _radioValue,
          onChanged: _handleRadioValueChange,
        ),*/
            ])),
            Container(
                alignment: Alignment.centerLeft,
                child: RichText(
                  textAlign: TextAlign.left,
                  text: TextSpan(
                    text: definiciones[23],
                    style: TextStyle(
                        color: Color.fromRGBO(0, 0, 0, 1), fontSize: 18),
                  ),
                )),
          ]));
    }

    ///////////
    ///
    ///
    ///////////

    if ((indexFormularios == 7 && viveCandelaria == true) ||
        (indexFormularios == 7 &&
            viveCandelaria == false &&
            trabajaCandelaria == true)) {
      return Center(
          child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
            RichText(
              textAlign: TextAlign.center,
              text: TextSpan(
                text: "Factores de riesgo de enfermar gravemente de COVID-19",
                style: TextStyle(
                    color: Color.fromRGBO(193, 29, 27, 1), fontSize: 20),
              ),
            ),
            Text("Enfermedad cardiovascular"),
            Container(
                child: Row(children: <Widget>[
              new Text(
                'No',
                style: new TextStyle(fontSize: 16.0),
              ),
              new Radio(
                value: 0,
                groupValue: radioValueCardiovascular,
                onChanged: _handleRadioValueChangeCardiovascular,
              ),
              new Text(
                'Si',
                style: new TextStyle(fontSize: 16.0),
              ),
              new Radio(
                value: 1,
                groupValue: radioValueCardiovascular,
                onChanged: _handleRadioValueChangeCardiovascular,
              ),
              /* new Text(
          'NO APLICA',
          style: new TextStyle(fontSize: 16.0),
        ),
        new Radio(
          value: 2,
          groupValue: _radioValue,
          onChanged: _handleRadioValueChange,
        ),*/
            ])),
            Container(
                alignment: Alignment.centerLeft,
                child: RichText(
                  textAlign: TextAlign.left,
                  text: TextSpan(
                    text: definiciones[24],
                    style: TextStyle(
                        color: Color.fromRGBO(0, 0, 0, 1), fontSize: 18),
                  ),
                )),
          ]));
    }

    ///////////
    ///
    ///
    ///////////

    if ((indexFormularios == 8 && viveCandelaria == true) ||
        (indexFormularios == 8 &&
            viveCandelaria == false &&
            trabajaCandelaria == true)) {
      return Center(
          child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
            RichText(
              textAlign: TextAlign.center,
              text: TextSpan(
                text: "Factores de riesgo de enfermar gravemente de COVID-19",
                style: TextStyle(
                    color: Color.fromRGBO(193, 29, 27, 1), fontSize: 20),
              ),
            ),
            Text("Cáncer"),
            Container(
                child: Row(children: <Widget>[
              new Text(
                'No',
                style: new TextStyle(fontSize: 16.0),
              ),
              new Radio(
                value: 0,
                groupValue: radioValueCancer,
                onChanged: _handleRadioValueChangeCancer,
              ),
              new Text(
                'Si',
                style: new TextStyle(fontSize: 16.0),
              ),
              new Radio(
                value: 1,
                groupValue: radioValueCancer,
                onChanged: _handleRadioValueChangeCancer,
              ),
              /* new Text(
          'NO APLICA',
          style: new TextStyle(fontSize: 16.0),
        ),
        new Radio(
          value: 2,
          groupValue: _radioValue,
          onChanged: _handleRadioValueChange,
        ),*/
            ])),
            Container(
                alignment: Alignment.centerLeft,
                child: RichText(
                  textAlign: TextAlign.left,
                  text: TextSpan(
                    text: definiciones[25],
                    style: TextStyle(
                        color: Color.fromRGBO(0, 0, 0, 1), fontSize: 18),
                  ),
                )),
          ]));
    }

    ///////////
    ///
    ///
    ///////////

    if ((indexFormularios == 9 && viveCandelaria == true) ||
        (indexFormularios == 9 &&
            viveCandelaria == false &&
            trabajaCandelaria == true)) {
      return Center(
          child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
            RichText(
              textAlign: TextAlign.center,
              text: TextSpan(
                text: "Factores de riesgo de enfermar gravemente de COVID-19",
                style: TextStyle(
                    color: Color.fromRGBO(193, 29, 27, 1), fontSize: 20),
              ),
            ),
            Text("Enfermedad renal crónica"),
            Container(
                child: Row(children: <Widget>[
              new Text(
                'No',
                style: new TextStyle(fontSize: 16.0),
              ),
              new Radio(
                value: 0,
                groupValue: radioValueRenal,
                onChanged: _handleRadioValueChangeRenal,
              ),
              new Text(
                'Si',
                style: new TextStyle(fontSize: 16.0),
              ),
              new Radio(
                value: 1,
                groupValue: radioValueRenal,
                onChanged: _handleRadioValueChangeRenal,
              ),
              /* new Text(
          'NO APLICA',
          style: new TextStyle(fontSize: 16.0),
        ),
        new Radio(
          value: 2,
          groupValue: _radioValue,
          onChanged: _handleRadioValueChange,
        ),*/
            ])),
            Container(
                alignment: Alignment.centerLeft,
                child: RichText(
                  textAlign: TextAlign.left,
                  text: TextSpan(
                    text: definiciones[26],
                    style: TextStyle(
                        color: Color.fromRGBO(0, 0, 0, 1), fontSize: 18),
                  ),
                )),
          ]));
    }

    ///////////
    ///
    ///
    ///////////

    if ((indexFormularios == 10 && viveCandelaria == true) ||
        (indexFormularios == 10 &&
            viveCandelaria == false &&
            trabajaCandelaria == true)) {
      return Center(
          child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
            RichText(
              textAlign: TextAlign.center,
              text: TextSpan(
                text: "Factores de riesgo de enfermar gravemente de COVID-19",
                style: TextStyle(
                    color: Color.fromRGBO(193, 29, 27, 1), fontSize: 20),
              ),
            ),
            Text("Asma"),
            Container(
                child: Row(children: <Widget>[
              new Text(
                'No',
                style: new TextStyle(fontSize: 16.0),
              ),
              new Radio(
                value: 0,
                groupValue: radioValueAsma,
                onChanged: _handleRadioValueChangeAsma,
              ),
              new Text(
                'Si',
                style: new TextStyle(fontSize: 16.0),
              ),
              new Radio(
                value: 1,
                groupValue: radioValueAsma,
                onChanged: _handleRadioValueChangeAsma,
              ),
              /* new Text(
          'NO APLICA',
          style: new TextStyle(fontSize: 16.0),
        ),
        new Radio(
          value: 2,
          groupValue: _radioValue,
          onChanged: _handleRadioValueChange,
        ),*/
            ])),
            Container(
                alignment: Alignment.centerLeft,
                child: RichText(
                  textAlign: TextAlign.left,
                  text: TextSpan(
                    text: definiciones[27],
                    style: TextStyle(
                        color: Color.fromRGBO(0, 0, 0, 1), fontSize: 18),
                  ),
                )),
          ]));
    }

    ///////////
    ///
    ///
    ///////////

    if ((indexFormularios == 11 && viveCandelaria == true) ||
        (indexFormularios == 11 &&
            viveCandelaria == false &&
            trabajaCandelaria == true)) {
      return Center(
          child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
            RichText(
              textAlign: TextAlign.center,
              text: TextSpan(
                text: "Factores de riesgo de enfermar gravemente de COVID-19",
                style: TextStyle(
                    color: Color.fromRGBO(193, 29, 27, 1), fontSize: 20),
              ),
            ),
            Text("Enfermedad Pulmonar Obstructiva Crónica"),
            Container(
                child: Row(children: <Widget>[
              new Text(
                'No',
                style: new TextStyle(fontSize: 16.0),
              ),
              new Radio(
                value: 0,
                groupValue: radioValuePulmonar,
                onChanged: _handleRadioValueChangePulmonar,
              ),
              new Text(
                'Si',
                style: new TextStyle(fontSize: 16.0),
              ),
              new Radio(
                value: 1,
                groupValue: radioValuePulmonar,
                onChanged: _handleRadioValueChangePulmonar,
              ),
              /* new Text(
          'NO APLICA',
          style: new TextStyle(fontSize: 16.0),
        ),
        new Radio(
          value: 2,
          groupValue: _radioValue,
          onChanged: _handleRadioValueChange,
        ),*/
            ])),
            Container(
                alignment: Alignment.centerLeft,
                child: RichText(
                  textAlign: TextAlign.left,
                  text: TextSpan(
                    text: definiciones[28],
                    style: TextStyle(
                        color: Color.fromRGBO(0, 0, 0, 1), fontSize: 18),
                  ),
                )),
          ]));
    }

    ///////////
    ///
    ///
    ///////////

    if ((indexFormularios == 12 && viveCandelaria == true) ||
        (indexFormularios == 12 &&
            viveCandelaria == false &&
            trabajaCandelaria == true)) {
      return Center(
          child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
            RichText(
              textAlign: TextAlign.center,
              text: TextSpan(
                text: "Factores de riesgo de enfermar gravemente de COVID-19",
                style: TextStyle(
                    color: Color.fromRGBO(193, 29, 27, 1), fontSize: 20),
              ),
            ),
            Text("Inmunosupresión"),
            Container(
                child: Row(children: <Widget>[
              new Text(
                'No',
                style: new TextStyle(fontSize: 16.0),
              ),
              new Radio(
                value: 0,
                groupValue: radioValueImunusupresion,
                onChanged: _handleRadioValueChangeImunusupresion,
              ),
              new Text(
                'Si',
                style: new TextStyle(fontSize: 16.0),
              ),
              new Radio(
                value: 1,
                groupValue: radioValueImunusupresion,
                onChanged: _handleRadioValueChangeImunusupresion,
              ),
              /* new Text(
          'NO APLICA',
          style: new TextStyle(fontSize: 16.0),
        ),
        new Radio(
          value: 2,
          groupValue: _radioValue,
          onChanged: _handleRadioValueChange,
        ),*/
            ])),
            Container(
                alignment: Alignment.centerLeft,
                child: RichText(
                  textAlign: TextAlign.left,
                  text: TextSpan(
                    text: definiciones[29],
                    style: TextStyle(
                        color: Color.fromRGBO(0, 0, 0, 1), fontSize: 18),
                  ),
                )),
          ]));
    }

    ///////////
    ///
    ///
    ///////////

    if ((indexFormularios == 13 && viveCandelaria == true) ||
        (indexFormularios == 13 &&
            viveCandelaria == false &&
            trabajaCandelaria == true)) {
      return Center(
          child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
            RichText(
              textAlign: TextAlign.center,
              text: TextSpan(
                text: "Factores de riesgo de enfermar gravemente de COVID-19",
                style: TextStyle(
                    color: Color.fromRGBO(193, 29, 27, 1), fontSize: 20),
              ),
            ),
            Text("Obesidad"),
            Container(
                child: Row(children: <Widget>[
              new Text(
                'No',
                style: new TextStyle(fontSize: 16.0),
              ),
              new Radio(
                value: 0,
                groupValue: radioValueObesidad,
                onChanged: _handleRadioValueChangeObesidad,
              ),
              new Text(
                'Si',
                style: new TextStyle(fontSize: 16.0),
              ),
              new Radio(
                value: 1,
                groupValue: radioValueObesidad,
                onChanged: _handleRadioValueChangeObesidad,
              ),
              /* new Text(
          'NO APLICA',
          style: new TextStyle(fontSize: 16.0),
        ),
        new Radio(
          value: 2,
          groupValue: _radioValue,
          onChanged: _handleRadioValueChange,
        ),*/
            ])),
            Container(
                alignment: Alignment.centerLeft,
                child: RichText(
                  textAlign: TextAlign.left,
                  text: TextSpan(
                    text: definiciones[30],
                    style: TextStyle(
                        color: Color.fromRGBO(0, 0, 0, 1), fontSize: 18),
                  ),
                )),
          ]));
    }

    ///////////
    ///
    ///
    ///////////

    if ((indexFormularios == 14 && viveCandelaria == true) ||
        (indexFormularios == 14 &&
            viveCandelaria == false &&
            trabajaCandelaria == true)) {
      return Center(
          child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
            RichText(
              textAlign: TextAlign.center,
              text: TextSpan(
                text: "Factores de riesgo de enfermar gravemente de COVID-19",
                style: TextStyle(
                    color: Color.fromRGBO(193, 29, 27, 1), fontSize: 20),
              ),
            ),
            Text("Desnutrición"),
            Container(
                child: Row(children: <Widget>[
              new Text(
                'No',
                style: new TextStyle(fontSize: 16.0),
              ),
              new Radio(
                value: 0,
                groupValue: radioValueDesnutricion,
                onChanged: _handleRadioValueChangeDesnutricion,
              ),
              new Text(
                'Si',
                style: new TextStyle(fontSize: 16.0),
              ),
              new Radio(
                value: 1,
                groupValue: radioValueDesnutricion,
                onChanged: _handleRadioValueChangeDesnutricion,
              ),
              /* new Text(
          'NO APLICA',
          style: new TextStyle(fontSize: 16.0),
        ),
        new Radio(
          value: 2,
          groupValue: _radioValue,
          onChanged: _handleRadioValueChange,
        ),*/
            ])),
            Container(
                alignment: Alignment.centerLeft,
                child: RichText(
                  textAlign: TextAlign.left,
                  text: TextSpan(
                    text: definiciones[31],
                    style: TextStyle(
                        color: Color.fromRGBO(0, 0, 0, 1), fontSize: 18),
                  ),
                )),
          ]));
    }

    ///////////
    ///
    ///
    ///////////

    if ((indexFormularios == 15 && viveCandelaria == true) ||
        (indexFormularios == 15 &&
            viveCandelaria == false &&
            trabajaCandelaria == true)) {
      return Center(
          child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
            RichText(
              textAlign: TextAlign.center,
              text: TextSpan(
                text: "Factores de riesgo de enfermar gravemente de COVID-19",
                style: TextStyle(
                    color: Color.fromRGBO(193, 29, 27, 1), fontSize: 20),
              ),
            ),
            Text("Trastorno neurológico crónico"),
            Container(
                child: Row(children: <Widget>[
              new Text(
                'No',
                style: new TextStyle(fontSize: 16.0),
              ),
              new Radio(
                value: 0,
                groupValue: radioValueNeurologico,
                onChanged: _handleRadioValueChangeNeurologico,
              ),
              new Text(
                'Si',
                style: new TextStyle(fontSize: 16.0),
              ),
              new Radio(
                value: 1,
                groupValue: radioValueNeurologico,
                onChanged: _handleRadioValueChangeNeurologico,
              ),
              /* new Text(
          'NO APLICA',
          style: new TextStyle(fontSize: 16.0),
        ),
        new Radio(
          value: 2,
          groupValue: _radioValue,
          onChanged: _handleRadioValueChange,
        ),*/
            ])),
            Container(
                alignment: Alignment.centerLeft,
                child: RichText(
                  textAlign: TextAlign.left,
                  text: TextSpan(
                    text: definiciones[32],
                    style: TextStyle(
                        color: Color.fromRGBO(0, 0, 0, 1), fontSize: 18),
                  ),
                )),
          ]));
    }

    ///////////
    ///
    ///
    ///////////

    if ((indexFormularios == 16 && viveCandelaria == true) ||
        (indexFormularios == 16 &&
            viveCandelaria == false &&
            trabajaCandelaria == true)) {
      return Center(
          child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
            RichText(
              textAlign: TextAlign.center,
              text: TextSpan(
                text: "Factores de riesgo de enfermar gravemente de COVID-19",
                style: TextStyle(
                    color: Color.fromRGBO(193, 29, 27, 1), fontSize: 20),
              ),
            ),
            Text("Tabaquismo"),
            Container(
                child: Row(children: <Widget>[
              new Text(
                'No',
                style: new TextStyle(fontSize: 16.0),
              ),
              new Radio(
                value: 0,
                groupValue: radioValueTabaquismo,
                onChanged: _handleRadioValueChangeTabaquismo,
              ),
              new Text(
                'Si',
                style: new TextStyle(fontSize: 16.0),
              ),
              new Radio(
                value: 1,
                groupValue: radioValueTabaquismo,
                onChanged: _handleRadioValueChangeTabaquismo,
              ),
              /* new Text(
          'NO APLICA',
          style: new TextStyle(fontSize: 16.0),
        ),
        new Radio(
          value: 2,
          groupValue: _radioValue,
          onChanged: _handleRadioValueChange,
        ),*/
            ])),
            Container(
                alignment: Alignment.centerLeft,
                child: RichText(
                  textAlign: TextAlign.left,
                  text: TextSpan(
                    text: definiciones[33],
                    style: TextStyle(
                        color: Color.fromRGBO(0, 0, 0, 1), fontSize: 18),
                  ),
                )),
          ]));
    }

    ///////////
    ///
    ///
    ///////////

    if ((indexFormularios == 17 && viveCandelaria == true) ||
        (indexFormularios == 17 &&
            viveCandelaria == false &&
            trabajaCandelaria == true)) {
      return Center(
          child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
            RichText(
              textAlign: TextAlign.center,
              text: TextSpan(
                text: "Factores de riesgo de enfermar gravemente de COVID-19",
                style: TextStyle(
                    color: Color.fromRGBO(193, 29, 27, 1), fontSize: 20),
              ),
            ),
            Text("Alcoholismo"),
            Container(
                child: Row(children: <Widget>[
              new Text(
                'No',
                style: new TextStyle(fontSize: 16.0),
              ),
              new Radio(
                value: 0,
                groupValue: radioValueAlcoholismo,
                onChanged: _handleRadioValueChangeAlcoholismo,
              ),
              new Text(
                'Si',
                style: new TextStyle(fontSize: 16.0),
              ),
              new Radio(
                value: 1,
                groupValue: radioValueAlcoholismo,
                onChanged: _handleRadioValueChangeAlcoholismo,
              ),
              /* new Text(
          'NO APLICA',
          style: new TextStyle(fontSize: 16.0),
        ),
        new Radio(
          value: 2,
          groupValue: _radioValue,
          onChanged: _handleRadioValueChange,
        ),*/
            ])),
            Container(
                alignment: Alignment.centerLeft,
                child: RichText(
                  textAlign: TextAlign.left,
                  text: TextSpan(
                    text: definiciones[34],
                    style: TextStyle(
                        color: Color.fromRGBO(0, 0, 0, 1), fontSize: 18),
                  ),
                )),
          ]));
    }
    //////
    ///
    ///
    ///
    ///

    ///Factores de riesgo en salud mental
    if ((indexFormularios == 18 && viveCandelaria == true) ||
        (indexFormularios == 18 &&
            viveCandelaria == false &&
            trabajaCandelaria == true)) {
      return Container(
          child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
            RichText(
              textAlign: TextAlign.center,
              text: TextSpan(
                text: "Factores de riesgo en salud mental",
                style: TextStyle(
                    color: Color.fromRGBO(193, 29, 27, 1), fontSize: 20),
              ),
            ),
            Text(definiciones[35]),
            Container(
                height: 60.0,
                padding:
                    const EdgeInsets.only(left: 10.0, top: 5.0, right: 10.0),
                margin: const EdgeInsets.only(bottom: 20.0, right: 10.0),
                decoration: const BoxDecoration(
                  border: Border(
                    top: BorderSide(
                      width: 1,
                      color: Color.fromRGBO(120, 120, 120, 1),
                    ),
                    left: BorderSide(
                      width: 1,
                      color: Color.fromRGBO(120, 120, 120, 1),
                    ),
                    right: BorderSide(
                      width: 1,
                      color: Color.fromRGBO(120, 120, 120, 1),
                    ),
                    bottom: BorderSide(
                      width: 1,
                      color: Color.fromRGBO(120, 120, 120, 1),
                    ),
                  ),
                  borderRadius: BorderRadius.all(
                      Radius.circular(5.0) //         <--- border radius here
                      ),
                ),
                child: DropdownButton<String>(
                  isExpanded: true,
                  value: dropdownValueSaludMental,
                  icon: Icon(Icons.arrow_downward),
                  iconSize: 30,
                  style: TextStyle(
                      color: Color.fromRGBO(120, 120, 120, 1), fontSize: 15),
                  onChanged: (String newValue) async {
                    DatabaseSaludMental saludMentalSinc =
                        DatabaseSaludMental.instance;
                    SaludMental getSaludMental =
                        await saludMentalSinc.querySaludMentalId(newValue);
                    if (getSaludMental != null) {
                      setState(() {
                        dropdownValueSaludMental = newValue;
                        saludMental = getSaludMental.idSaludMental;
                      });
                    }
                  },
                  items: <String>[
                    for (var i = 0; i < mental.length; i++) mental[i],
                  ].map<DropdownMenuItem<String>>((String value) {
                    return DropdownMenuItem<String>(
                      value: value,
                      child: Text(value),
                    );
                  }).toList(),
                )),
          ]));
    }

    ///////////
    ///
    ///
    ///////////

    if ((indexFormularios == 19 && viveCandelaria == true) ||
        (indexFormularios == 19 &&
            viveCandelaria == false &&
            trabajaCandelaria == true) ||
        indexFormularios == 3 &&
            viveCandelaria == false &&
            trabajaCandelaria == false) {
      return Center(
          child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
            RichText(
              textAlign: TextAlign.center,
              text: TextSpan(
                text: "Signos y síntomas respiratorios",
                style: TextStyle(
                    color: Color.fromRGBO(193, 29, 27, 1), fontSize: 20),
              ),
            ),
            Text("Tos"),
            Container(
                child: Row(children: <Widget>[
              new Text(
                'No',
                style: new TextStyle(fontSize: 16.0),
              ),
              new Radio(
                value: 0,
                groupValue: radioValueTos,
                onChanged: _handleRadioValueChangeTos,
              ),
              new Text(
                'Si',
                style: new TextStyle(fontSize: 16.0),
              ),
              new Radio(
                value: 1,
                groupValue: radioValueTos,
                onChanged: _handleRadioValueChangeTos,
              ),
              /* new Text(
          'NO APLICA',
          style: new TextStyle(fontSize: 16.0),
        ),
        new Radio(
          value: 2,
          groupValue: _radioValue,
          onChanged: _handleRadioValueChange,
        ),*/
            ])),
            Container(
                alignment: Alignment.centerLeft,
                child: RichText(
                  textAlign: TextAlign.left,
                  text: TextSpan(
                    text: definiciones[36],
                    style: TextStyle(
                        color: Color.fromRGBO(0, 0, 0, 1), fontSize: 18),
                  ),
                )),
          ]));
    }

    ///////////
    ///
    ///
    ///////////

    if ((indexFormularios == 20 && viveCandelaria == true) ||
        (indexFormularios == 20 &&
            viveCandelaria == false &&
            trabajaCandelaria == true) ||
        indexFormularios == 4 &&
            viveCandelaria == false &&
            trabajaCandelaria == false) {
      return Center(
          child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
            RichText(
              textAlign: TextAlign.center,
              text: TextSpan(
                text: "Signos y síntomas respiratorios",
                style: TextStyle(
                    color: Color.fromRGBO(193, 29, 27, 1), fontSize: 20),
              ),
            ),
            Text("Dificultad respiratoria"),
            Container(
                child: Row(children: <Widget>[
              new Text(
                'No',
                style: new TextStyle(fontSize: 16.0),
              ),
              new Radio(
                value: 0,
                groupValue: radioValueDificultadRespiratoria,
                onChanged: _handleRadioValueChangeDificultadResp,
              ),
              new Text(
                'Si',
                style: new TextStyle(fontSize: 16.0),
              ),
              new Radio(
                value: 1,
                groupValue: radioValueDificultadRespiratoria,
                onChanged: _handleRadioValueChangeDificultadResp,
              ),
              /* new Text(
          'NO APLICA',
          style: new TextStyle(fontSize: 16.0),
        ),
        new Radio(
          value: 2,
          groupValue: _radioValue,
          onChanged: _handleRadioValueChange,
        ),*/
            ])),
            Container(
                alignment: Alignment.centerLeft,
                child: RichText(
                  textAlign: TextAlign.left,
                  text: TextSpan(
                    text: definiciones[37],
                    style: TextStyle(
                        color: Color.fromRGBO(0, 0, 0, 1), fontSize: 18),
                  ),
                )),
          ]));
    }

    ///////////
    ///
    ///
    ///////////

    if ((indexFormularios == 21 && viveCandelaria == true) ||
        (indexFormularios == 21 &&
            viveCandelaria == false &&
            trabajaCandelaria == true) ||
        indexFormularios == 5 &&
            viveCandelaria == false &&
            trabajaCandelaria == false) {
      return Center(
          child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
            RichText(
              textAlign: TextAlign.center,
              text: TextSpan(
                text: "Signos y síntomas respiratorios",
                style: TextStyle(
                    color: Color.fromRGBO(193, 29, 27, 1), fontSize: 20),
              ),
            ),
            Text("Dolor de garganta"),
            Container(
                child: Row(children: <Widget>[
              new Text(
                'No',
                style: new TextStyle(fontSize: 16.0),
              ),
              new Radio(
                value: 0,
                groupValue: radioValueDolorGarganta,
                onChanged: _handleRadioValueChangeDolorGarg,
              ),
              new Text(
                'Si',
                style: new TextStyle(fontSize: 16.0),
              ),
              new Radio(
                value: 1,
                groupValue: radioValueDolorGarganta,
                onChanged: _handleRadioValueChangeDolorGarg,
              ),
              /* new Text(
          'NO APLICA',
          style: new TextStyle(fontSize: 16.0),
        ),
        new Radio(
          value: 2,
          groupValue: _radioValue,
          onChanged: _handleRadioValueChange,
        ),*/
            ])),
            Container(
                alignment: Alignment.centerLeft,
                child: RichText(
                  textAlign: TextAlign.left,
                  text: TextSpan(
                    text: definiciones[38],
                    style: TextStyle(
                        color: Color.fromRGBO(0, 0, 0, 1), fontSize: 18),
                  ),
                )),
          ]));
    }

    ///////////
    ///
    ///
    ///////////

    if ((indexFormularios == 22 && viveCandelaria == true) ||
        (indexFormularios == 22 &&
            viveCandelaria == false &&
            trabajaCandelaria == true) ||
        indexFormularios == 6 &&
            viveCandelaria == false &&
            trabajaCandelaria == false) {
      return Center(
          child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
            RichText(
              textAlign: TextAlign.center,
              text: TextSpan(
                text: "Signos y síntomas respiratorios",
                style: TextStyle(
                    color: Color.fromRGBO(193, 29, 27, 1), fontSize: 20),
              ),
            ),
            Text("Fiebre cuantificada mayor a 38°C"),
            Container(
                child: Row(children: <Widget>[
              new Text(
                'No',
                style: new TextStyle(fontSize: 16.0),
              ),
              new Radio(
                value: 0,
                groupValue: radioValueFiebre,
                onChanged: _handleRadioValueChangeFiebre,
              ),
              new Text(
                'Si',
                style: new TextStyle(fontSize: 16.0),
              ),
              new Radio(
                value: 1,
                groupValue: radioValueFiebre,
                onChanged: _handleRadioValueChangeFiebre,
              ),
              /* new Text(
          'NO APLICA',
          style: new TextStyle(fontSize: 16.0),
        ),
        new Radio(
          value: 2,
          groupValue: _radioValue,
          onChanged: _handleRadioValueChange,
        ),*/
            ])),
            Container(
                alignment: Alignment.centerLeft,
                child: RichText(
                  textAlign: TextAlign.left,
                  text: TextSpan(
                    text: definiciones[39],
                    style: TextStyle(
                        color: Color.fromRGBO(0, 0, 0, 1), fontSize: 18),
                  ),
                )),
          ]));
    }

    ///////////
    ///
    ///
    ///////////

    if ((indexFormularios == 23 && viveCandelaria == true) ||
        (indexFormularios == 23 &&
            viveCandelaria == false &&
            trabajaCandelaria == true) ||
        indexFormularios == 7 &&
            viveCandelaria == false &&
            trabajaCandelaria == false) {
      return Center(
          child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
            RichText(
              textAlign: TextAlign.center,
              text: TextSpan(
                text: "Signos y síntomas respiratorios",
                style: TextStyle(
                    color: Color.fromRGBO(193, 29, 27, 1), fontSize: 20),
              ),
            ),
            Text("Fatiga o decaimiento"),
            Container(
                child: Row(children: <Widget>[
              new Text(
                'No',
                style: new TextStyle(fontSize: 16.0),
              ),
              new Radio(
                value: 0,
                groupValue: radioValueFatiga,
                onChanged: _handleRadioValueChangeFatiga,
              ),
              new Text(
                'Si',
                style: new TextStyle(fontSize: 16.0),
              ),
              new Radio(
                value: 1,
                groupValue: radioValueFatiga,
                onChanged: _handleRadioValueChangeFatiga,
              ),
              /* new Text(
          'NO APLICA',
          style: new TextStyle(fontSize: 16.0),
        ),
        new Radio(
          value: 2,
          groupValue: _radioValue,
          onChanged: _handleRadioValueChange,
        ),*/
            ])),
            Container(
                alignment: Alignment.centerLeft,
                child: RichText(
                  textAlign: TextAlign.left,
                  text: TextSpan(
                    text: definiciones[40],
                    style: TextStyle(
                        color: Color.fromRGBO(0, 0, 0, 1), fontSize: 18),
                  ),
                )),
          ]));
    }

    ///////////
    ///
    ///
    ///////////

    if ((indexFormularios == 24 && viveCandelaria == true) ||
        (indexFormularios == 24 &&
            viveCandelaria == false &&
            trabajaCandelaria == true) ||
        indexFormularios == 8 &&
            viveCandelaria == false &&
            trabajaCandelaria == false) {
      return Center(
          child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
            RichText(
              textAlign: TextAlign.center,
              text: TextSpan(
                text: "Signos y síntomas respiratorios",
                style: TextStyle(
                    color: Color.fromRGBO(193, 29, 27, 1), fontSize: 20),
              ),
            ),
            Text("Malestar general"),
            Container(
                child: Row(children: <Widget>[
              new Text(
                'No',
                style: new TextStyle(fontSize: 16.0),
              ),
              new Radio(
                value: 0,
                groupValue: radioValueMalestar,
                onChanged: _handleRadioValueChangeMalestar,
              ),
              new Text(
                'Si',
                style: new TextStyle(fontSize: 16.0),
              ),
              new Radio(
                value: 1,
                groupValue: radioValueMalestar,
                onChanged: _handleRadioValueChangeMalestar,
              ),
              /* new Text(
          'NO APLICA',
          style: new TextStyle(fontSize: 16.0),
        ),
        new Radio(
          value: 2,
          groupValue: _radioValue,
          onChanged: _handleRadioValueChange,
        ),*/
            ])),
            Container(
                alignment: Alignment.centerLeft,
                child: RichText(
                  textAlign: TextAlign.left,
                  text: TextSpan(
                    text: definiciones[41],
                    style: TextStyle(
                        color: Color.fromRGBO(0, 0, 0, 1), fontSize: 18),
                  ),
                )),
          ]));
    }

    ///////////
    ///
    ///
    ///////////

    if ((indexFormularios == 25 && viveCandelaria == true) ||
        (indexFormularios == 25 &&
            viveCandelaria == false &&
            trabajaCandelaria == true) ||
        indexFormularios == 9 &&
            viveCandelaria == false &&
            trabajaCandelaria == false) {
      return Center(
          child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
            RichText(
              textAlign: TextAlign.center,
              text: TextSpan(
                text: "Signos y síntomas respiratorios",
                style: TextStyle(
                    color: Color.fromRGBO(193, 29, 27, 1), fontSize: 20),
              ),
            ),
            Text("Disminución en el sentido del gusto"),
            Container(
                child: Row(children: <Widget>[
              new Text(
                'No',
                style: new TextStyle(fontSize: 16.0),
              ),
              new Radio(
                value: 0,
                groupValue: radioValueGusto,
                onChanged: _handleRadioValueChangeGusto,
              ),
              new Text(
                'Si',
                style: new TextStyle(fontSize: 16.0),
              ),
              new Radio(
                value: 1,
                groupValue: radioValueGusto,
                onChanged: _handleRadioValueChangeGusto,
              ),
              /* new Text(
          'NO APLICA',
          style: new TextStyle(fontSize: 16.0),
        ),
        new Radio(
          value: 2,
          groupValue: _radioValue,
          onChanged: _handleRadioValueChange,
        ),*/
            ])),
            Container(
                alignment: Alignment.centerLeft,
                child: RichText(
                  textAlign: TextAlign.left,
                  text: TextSpan(
                    text: definiciones[42],
                    style: TextStyle(
                        color: Color.fromRGBO(0, 0, 0, 1), fontSize: 18),
                  ),
                )),
          ]));
    }

    ///////////
    ///
    ///
    ///////////

    if ((indexFormularios == 26 && viveCandelaria == true) ||
        (indexFormularios == 26 &&
            viveCandelaria == false &&
            trabajaCandelaria == true) ||
        indexFormularios == 10 &&
            viveCandelaria == false &&
            trabajaCandelaria == false) {
      return Center(
          child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
            RichText(
              textAlign: TextAlign.center,
              text: TextSpan(
                text: "Signos y síntomas respiratorios",
                style: TextStyle(
                    color: Color.fromRGBO(193, 29, 27, 1), fontSize: 20),
              ),
            ),
            Text("Disminución en el sentido del olfato"),
            Container(
                child: Row(children: <Widget>[
              new Text(
                'No',
                style: new TextStyle(fontSize: 16.0),
              ),
              new Radio(
                value: 0,
                groupValue: radioValueOlfato,
                onChanged: _handleRadioValueChangeOlfato,
              ),
              new Text(
                'Si',
                style: new TextStyle(fontSize: 16.0),
              ),
              new Radio(
                value: 1,
                groupValue: radioValueOlfato,
                onChanged: _handleRadioValueChangeOlfato,
              ),
              /* new Text(
          'NO APLICA',
          style: new TextStyle(fontSize: 16.0),
        ),
        new Radio(
          value: 2,
          groupValue: _radioValue,
          onChanged: _handleRadioValueChange,
        ),*/
            ])),
            Container(
                alignment: Alignment.centerLeft,
                child: RichText(
                  textAlign: TextAlign.left,
                  text: TextSpan(
                    text: definiciones[43],
                    style: TextStyle(
                        color: Color.fromRGBO(0, 0, 0, 1), fontSize: 18),
                  ),
                )),
          ]));
    }

    ///////////
    ///
    ///
    ///////////

    if ((indexFormularios == 27 && viveCandelaria == true) ||
        (indexFormularios == 27 &&
            viveCandelaria == false &&
            trabajaCandelaria == true) ||
        indexFormularios == 11 &&
            viveCandelaria == false &&
            trabajaCandelaria == false) {
      return Center(
          child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
            RichText(
              textAlign: TextAlign.center,
              text: TextSpan(
                text: "Signos y síntomas respiratorios",
                style: TextStyle(
                    color: Color.fromRGBO(193, 29, 27, 1), fontSize: 20),
              ),
            ),
            Text("Secreción nasal"),
            Container(
                child: Row(children: <Widget>[
              new Text(
                'No',
                style: new TextStyle(fontSize: 16.0),
              ),
              new Radio(
                value: 0,
                groupValue: radioValueNasal,
                onChanged: _handleRadioValueChangeNasal,
              ),
              new Text(
                'Si',
                style: new TextStyle(fontSize: 16.0),
              ),
              new Radio(
                value: 1,
                groupValue: radioValueNasal,
                onChanged: _handleRadioValueChangeNasal,
              ),
              /* new Text(
          'NO APLICA',
          style: new TextStyle(fontSize: 16.0),
        ),
        new Radio(
          value: 2,
          groupValue: _radioValue,
          onChanged: _handleRadioValueChange,
        ),*/
            ])),
            Container(
                alignment: Alignment.centerLeft,
                child: RichText(
                  textAlign: TextAlign.left,
                  text: TextSpan(
                    text: definiciones[44],
                    style: TextStyle(
                        color: Color.fromRGBO(0, 0, 0, 1), fontSize: 18),
                  ),
                )),
          ]));
    }

    ///////////
    ///
    ///
    ///////////

    if ((indexFormularios == 28 && viveCandelaria == true) ||
        (indexFormularios == 28 &&
            viveCandelaria == false &&
            trabajaCandelaria == true) ||
        indexFormularios == 12 &&
            viveCandelaria == false &&
            trabajaCandelaria == false) {
      return Center(
          child:
              Column(crossAxisAlignment: CrossAxisAlignment.start, children: <
                  Widget>[
        RichText(
          textAlign: TextAlign.center,
          text: TextSpan(
            text: "Signos y síntomas respiratorios",
            style:
                TextStyle(color: Color.fromRGBO(193, 29, 27, 1), fontSize: 20),
          ),
        ),
        Text("Inicio de sintomas"),
        if (fechaSint == null)
          InkWell(
              onTap: () {
                showDatePicker(
                        context: context,
                        initialDate: DateTime.now(),
                        firstDate: DateTime(1920),
                        lastDate: DateTime.now())
                    .then((date) {
                  setState(() {
                    fechaSint = date;
                    fechaSintView = DateFormat('yyyy-MM-dd').format(fechaSint);
                  });
                });
              },
              child: Container(
                padding: const EdgeInsets.only(left: 10.0, top: 18.0),
                margin: const EdgeInsets.only(bottom: 20.0, right: 10.0),
                width: 730,
                height: 60.0,
                decoration: const BoxDecoration(
                  border: Border(
                    top: BorderSide(width: 0.8, color: Color(0xFFFF000000)),
                    left: BorderSide(width: 0.8, color: Color(0xFFFF000000)),
                    right: BorderSide(width: 0.8, color: Color(0xFFFF000000)),
                    bottom: BorderSide(width: 0.8, color: Color(0xFFFF000000)),
                  ),
                  borderRadius: BorderRadius.all(
                      Radius.circular(5.0) //         <--- border radius here
                      ),
                ),
                child: RichText(
                  textAlign: TextAlign.left,
                  text: TextSpan(
                    text: "Fecha de inicio de signos y síntomas",
                    style: TextStyle(
                        color: Color.fromRGBO(120, 120, 120, 1), fontSize: 18),
                  ),
                ),
              )),
        if (fechaSint != null)
          InkWell(
              onTap: () {
                showDatePicker(
                        context: context,
                        initialDate: DateTime.now(),
                        firstDate: DateTime(1920),
                        lastDate: DateTime.now())
                    .then((date) {
                  setState(() {
                    fechaSint = date;
                    fechaSintView = DateFormat('yyyy-MM-dd').format(fechaSint);
                  });
                });
              },
              child: Container(
                padding: const EdgeInsets.only(left: 10.0, top: 18.0),
                margin: const EdgeInsets.only(bottom: 20.0, right: 10.0),
                width: 730,
                height: 60.0,
                decoration: const BoxDecoration(
                  border: Border(
                    top: BorderSide(width: 0.8, color: Color(0xFFFF000000)),
                    left: BorderSide(width: 0.8, color: Color(0xFFFF000000)),
                    right: BorderSide(width: 0.8, color: Color(0xFFFF000000)),
                    bottom: BorderSide(width: 0.8, color: Color(0xFFFF000000)),
                  ),
                  borderRadius: BorderRadius.all(
                      Radius.circular(5.0) //         <--- border radius here
                      ),
                ),
                child: RichText(
                  textAlign: TextAlign.left,
                  text: TextSpan(
                    text: fechaSintView,
                    style: TextStyle(
                        color: Color.fromRGBO(120, 120, 120, 1), fontSize: 18),
                  ),
                ),
              )),
        Container(
            alignment: Alignment.centerLeft,
            child: RichText(
              textAlign: TextAlign.left,
              text: TextSpan(
                text: definiciones[45],
                style:
                    TextStyle(color: Color.fromRGBO(0, 0, 0, 1), fontSize: 18),
              ),
            )),
      ]));
    }

    ///////////
    ///
    ///
    ///////////

    if ((indexFormularios == 29 && viveCandelaria == true) ||
        (indexFormularios == 29 &&
            viveCandelaria == false &&
            trabajaCandelaria == true) ||
        indexFormularios == 13 &&
            viveCandelaria == false &&
            trabajaCandelaria == false) {
      return Center(
          child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
            RichText(
              textAlign: TextAlign.center,
              text: TextSpan(
                text: "Antecedentes de exposición",
                style: TextStyle(
                    color: Color.fromRGBO(193, 29, 27, 1), fontSize: 20),
              ),
            ),
            Text("Desplazamientos en los últimos 14 días"),
            Container(
                child: Row(children: <Widget>[
              new Text(
                'No',
                style: new TextStyle(fontSize: 16.0),
              ),
              new Radio(
                value: 0,
                groupValue: radioValueDesplazamiento,
                onChanged: _handleRadioValueChangeDesplazamiento,
              ),
              new Text(
                'Si',
                style: new TextStyle(fontSize: 16.0),
              ),
              new Radio(
                value: 1,
                groupValue: radioValueDesplazamiento,
                onChanged: _handleRadioValueChangeDesplazamiento,
              ),
              /* new Text(
          'NO APLICA',
          style: new TextStyle(fontSize: 16.0),
        ),
        new Radio(
          value: 2,
          groupValue: _radioValue,
          onChanged: _handleRadioValueChange,
        ),*/
            ])),
            Container(
                alignment: Alignment.centerLeft,
                child: RichText(
                  textAlign: TextAlign.left,
                  text: TextSpan(
                    text: definiciones[46],
                    style: TextStyle(
                        color: Color.fromRGBO(0, 0, 0, 1), fontSize: 18),
                  ),
                )),
          ]));
    }

    ///////////
    ///
    ///
    ///////////

    if ((indexFormularios == 30 && viveCandelaria == true) ||
        (indexFormularios == 30 &&
            viveCandelaria == false &&
            trabajaCandelaria == true) ||
        indexFormularios == 14 &&
            viveCandelaria == false &&
            trabajaCandelaria == false) {
      return Center(
          child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
            RichText(
              textAlign: TextAlign.center,
              text: TextSpan(
                text: "Antecedentes de exposición",
                style: TextStyle(
                    color: Color.fromRGBO(193, 29, 27, 1), fontSize: 20),
              ),
            ),
            Text("Lugar del desplazamiento"),
            Container(
                padding: const EdgeInsets.only(bottom: 20.0, right: 10.0),
                child: TextFormField(
                  controller: _lugarViajeController,
                  decoration: const InputDecoration(
                    hintText: 'Lugar del viaje ',
                    border: const OutlineInputBorder(),
                    filled: true,
                    fillColor: Colors.white,
                  ),
                )),
            Container(
                alignment: Alignment.centerLeft,
                child: RichText(
                  textAlign: TextAlign.left,
                  text: TextSpan(
                    text: definiciones[47],
                    style: TextStyle(
                        color: Color.fromRGBO(0, 0, 0, 1), fontSize: 18),
                  ),
                )),
          ]));
    }

    ///////////
    ///
    ///
    ///////////

    if ((indexFormularios == 31 && viveCandelaria == true) ||
        (indexFormularios == 31 &&
            viveCandelaria == false &&
            trabajaCandelaria == true) ||
        indexFormularios == 15 &&
            viveCandelaria == false &&
            trabajaCandelaria == false) {
      return Center(
          child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
            RichText(
              textAlign: TextAlign.center,
              text: TextSpan(
                text: "Antecedentes de exposición",
                style: TextStyle(
                    color: Color.fromRGBO(193, 29, 27, 1), fontSize: 20),
              ),
            ),
            Text(
                "Contacto con un caso confirmado o probable de infección por COVID-19"),
            Container(
                child: Row(children: <Widget>[
              new Text(
                'No',
                style: new TextStyle(fontSize: 16.0),
              ),
              new Radio(
                value: 0,
                groupValue: radioValueContacto,
                onChanged: _handleRadioValueChangeContacto,
              ),
              new Text(
                'Si',
                style: new TextStyle(fontSize: 16.0),
              ),
              new Radio(
                value: 1,
                groupValue: radioValueContacto,
                onChanged: _handleRadioValueChangeContacto,
              ),
              new Text(
                'No sabe no responde',
                style: new TextStyle(fontSize: 16.0),
              ),
              new Radio(
                value: 777,
                groupValue: radioValueContacto,
                onChanged: _handleRadioValueChangeContacto,
              ),
            ])),
            Container(
                alignment: Alignment.centerLeft,
                child: RichText(
                  textAlign: TextAlign.left,
                  text: TextSpan(
                    text: definiciones[48],
                    style: TextStyle(
                        color: Color.fromRGBO(0, 0, 0, 1), fontSize: 18),
                  ),
                )),
          ]));
    }

    ///////////
    ///
    ///
    ///////////

    if ((indexFormularios == 32 && viveCandelaria == true) ||
        (indexFormularios == 32 &&
            viveCandelaria == false &&
            trabajaCandelaria == true) ||
        indexFormularios == 16 &&
            viveCandelaria == false &&
            trabajaCandelaria == false) {
      return Center(
          child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
            RichText(
              textAlign: TextAlign.center,
              text: TextSpan(
                text: "Antecedentes de exposición",
                style: TextStyle(
                    color: Color.fromRGBO(193, 29, 27, 1), fontSize: 20),
              ),
            ),
            Text("Lugar del contato"),
            Container(
                padding: const EdgeInsets.only(bottom: 20.0, right: 10.0),
                child: TextFormField(
                  controller: _lugarContactoController,
                  decoration: const InputDecoration(
                    hintText: 'Lugar dónde ocurrio el contato cercano',
                    border: const OutlineInputBorder(),
                    filled: true,
                    fillColor: Colors.white,
                  ),
                )),
            Container(
                alignment: Alignment.centerLeft,
                child: RichText(
                  textAlign: TextAlign.left,
                  text: TextSpan(
                    text: definiciones[49],
                    style: TextStyle(
                        color: Color.fromRGBO(0, 0, 0, 1), fontSize: 18),
                  ),
                )),
          ]));
    }
    ///////////
    ///
    ///
    ///////////

    if ((indexFormularios == 33 && viveCandelaria == true) ||
        (indexFormularios == 33 &&
            viveCandelaria == false &&
            trabajaCandelaria == true) ||
        indexFormularios == 17 &&
            viveCandelaria == false &&
            trabajaCandelaria == false) {
      return Center(
          child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
            RichText(
              textAlign: TextAlign.center,
              text: TextSpan(
                text:
                    "Identificación de caso sospechoso para infección de COVID-19",
                style: TextStyle(
                    color: Color.fromRGBO(193, 29, 27, 1), fontSize: 20),
              ),
            ),
            Text("Medición de temperatura"),
            Container(
                child: Row(children: <Widget>[
              new Text(
                'No',
                style: new TextStyle(fontSize: 16.0),
              ),
              new Radio(
                value: 0,
                groupValue: radioValueTemperatura,
                onChanged: _handleRadioValueChangeTemperatura,
              ),
              new Text(
                'Si',
                style: new TextStyle(fontSize: 16.0),
              ),
              new Radio(
                value: 1,
                groupValue: radioValueTemperatura,
                onChanged: _handleRadioValueChangeTemperatura,
              ),
            ])),
            if (radioValueTemperatura == 1)
              Container(
                  padding: const EdgeInsets.only(bottom: 20.0, right: 10.0),
                  child: TextFormField(
                    controller: _resultadoTempController,
                    decoration: const InputDecoration(
                      hintText: 'Prueba de temperatura',
                      border: const OutlineInputBorder(),
                      filled: true,
                      fillColor: Colors.white,
                    ),
                    keyboardType: TextInputType.number,
                  )),
            Container(
                alignment: Alignment.centerLeft,
                child: RichText(
                  textAlign: TextAlign.left,
                  text: TextSpan(
                    text: definiciones[50],
                    style: TextStyle(
                        color: Color.fromRGBO(0, 0, 0, 1), fontSize: 18),
                  ),
                )),
          ]));
    }

    ///////////
    ///
    ///
    ///////////

    /*  if ((indexFormularios == 34 && viveCandelaria == true) ||
        (indexFormularios == 34 &&
            viveCandelaria == false &&
            trabajaCandelaria == true) || indexFormularios == 18 &&
            viveCandelaria == false &&
            trabajaCandelaria == false) {
      return Center(
          child: Column( crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
        RichText(
          textAlign: TextAlign.center,
          text: TextSpan(
            text: "Identificación de caso sospechoso para infección de COVID-19",
            style:
                TextStyle(color: Color.fromRGBO(193, 29, 27, 1), fontSize: 20),
          ),
        ),
        Text("Caso sospechoso"),
        Container(
            child: Row(children: <Widget>[
          new Text(
            'No',
            style: new TextStyle(fontSize: 16.0),
          ),
          new Radio(
            value: 0,
            groupValue: radioValueCasoSospechoso,
            onChanged: _handleRadioValueChangecasoSos,
          ),
          new Text(
            'Si',
            style: new TextStyle(fontSize: 16.0),
          ),
          new Radio(
            value: 1,
            groupValue: radioValueCasoSospechoso,
            onChanged: _handleRadioValueChangecasoSos,
          ),
        ])),
        Container(
            alignment: Alignment.centerLeft,
            child: RichText(
              textAlign: TextAlign.left,
              text: TextSpan(
                text: definiciones[51],
                style:
                    TextStyle(color: Color.fromRGBO(0, 0, 0, 1), fontSize: 18),
              ),
            )),
      ]));
    }*/

    ///////////
    ///
    ///
    ///////////

    if ((indexFormularios == 34 && viveCandelaria == true) ||
        (indexFormularios == 34 &&
            viveCandelaria == false &&
            trabajaCandelaria == true) ||
        indexFormularios == 18 &&
            viveCandelaria == false &&
            trabajaCandelaria == false) {
      return Center(
          child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
            RichText(
              textAlign: TextAlign.center,
              text: TextSpan(
                text:
                    "Resultado de pruebas serológicas y diagnósticas para COVID-19",
                style: TextStyle(
                    color: Color.fromRGBO(193, 29, 27, 1), fontSize: 20),
              ),
            ),
            Text("Resultado prueba de detección de anticuerpos"),
            Container(
                child: Row(children: <Widget>[
              new Text(
                'Resultado negativo (-)',
                style: new TextStyle(fontSize: 16.0),
              ),
              new Radio(
                value: 0,
                groupValue: radioValueAnticuerpos,
                onChanged: _handleRadioValueChangeAnticuerpos,
              ),
              new Text(
                'Resultado positivo (+)',
                style: new TextStyle(fontSize: 16.0),
              ),
              new Radio(
                value: 1,
                groupValue: radioValueAnticuerpos,
                onChanged: _handleRadioValueChangeAnticuerpos,
              ),
              new Text(
                'Inválido',
                style: new TextStyle(fontSize: 16.0),
              ),
              new Radio(
                value: 2,
                groupValue: radioValueAnticuerpos,
                onChanged: _handleRadioValueChangeAnticuerpos,
              ),
              new Text(
                'No aplica',
                style: new TextStyle(fontSize: 16.0),
              ),
              new Radio(
                value: 888,
                groupValue: radioValueAnticuerpos,
                onChanged: _handleRadioValueChangeAnticuerpos,
              ),
            ])),
            Container(
                alignment: Alignment.centerLeft,
                child: RichText(
                  textAlign: TextAlign.left,
                  text: TextSpan(
                    text: definiciones[52],
                    style: TextStyle(
                        color: Color.fromRGBO(0, 0, 0, 1), fontSize: 18),
                  ),
                )),
          ]));
    }

    ///////////
    ///
    ///
    ///////////

    if ((indexFormularios == 35 && viveCandelaria == true) ||
        (indexFormularios == 35 &&
            viveCandelaria == false &&
            trabajaCandelaria == true) ||
        indexFormularios == 19 &&
            viveCandelaria == false &&
            trabajaCandelaria == false) {
      return Center(
          child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
            RichText(
              textAlign: TextAlign.center,
              text: TextSpan(
                text:
                    "Resultado de pruebas serológicas y diagnósticas para COVID-19",
                style: TextStyle(
                    color: Color.fromRGBO(193, 29, 27, 1), fontSize: 20),
              ),
            ),
            Text("Resultado prueba de detección de antígenos"),
            Container(
                child: Row(children: <Widget>[
              new Text(
                'Resultado negativo (-)',
                style: new TextStyle(fontSize: 16.0),
              ),
              new Radio(
                value: 0,
                groupValue: radioValueAntigenos,
                onChanged: _handleRadioValueChangeAntigenos,
              ),
              new Text(
                'Resultado positivo (+)',
                style: new TextStyle(fontSize: 16.0),
              ),
              new Radio(
                value: 1,
                groupValue: radioValueAntigenos,
                onChanged: _handleRadioValueChangeAntigenos,
              ),
              new Text(
                'Inválido',
                style: new TextStyle(fontSize: 16.0),
              ),
              new Radio(
                value: 2,
                groupValue: radioValueAntigenos,
                onChanged: _handleRadioValueChangeAntigenos,
              ),
              new Text(
                'No aplica',
                style: new TextStyle(fontSize: 16.0),
              ),
              new Radio(
                value: 888,
                groupValue: radioValueAntigenos,
                onChanged: _handleRadioValueChangeAntigenos,
              ),
            ])),
            Container(
                alignment: Alignment.centerLeft,
                child: RichText(
                  textAlign: TextAlign.left,
                  text: TextSpan(
                    text: definiciones[53],
                    style: TextStyle(
                        color: Color.fromRGBO(0, 0, 0, 1), fontSize: 18),
                  ),
                )),
          ]));
    }

    ///////////
    ///
    ///
    ///////////

    if ((indexFormularios == 36 && viveCandelaria == true) ||
        (indexFormularios == 36 &&
            viveCandelaria == false &&
            trabajaCandelaria == true) ||
        indexFormularios == 20 &&
            viveCandelaria == false &&
            trabajaCandelaria == false) {
      return Center(
          child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
            RichText(
              textAlign: TextAlign.center,
              text: TextSpan(
                text:
                    "Resultado de pruebas serológicas y diagnósticas para COVID-19",
                style: TextStyle(
                    color: Color.fromRGBO(193, 29, 27, 1), fontSize: 20),
              ),
            ),
            Text("Resultado prueba moleculares (RT-PCR)"),
            Container(
                child: Row(children: <Widget>[
              new Text(
                'Resultado negativo (-)',
                style: new TextStyle(fontSize: 16.0),
              ),
              new Radio(
                value: 0,
                groupValue: radioValueMolecular,
                onChanged: _handleRadioValueChangeMoleculares,
              ),
              new Text(
                'Resultado positivo (+)',
                style: new TextStyle(fontSize: 16.0),
              ),
              new Radio(
                value: 1,
                groupValue: radioValueMolecular,
                onChanged: _handleRadioValueChangeMoleculares,
              ),
              new Text(
                'Inválido',
                style: new TextStyle(fontSize: 16.0),
              ),
              new Radio(
                value: 2,
                groupValue: radioValueMolecular,
                onChanged: _handleRadioValueChangeMoleculares,
              ),
              new Text(
                'No aplica',
                style: new TextStyle(fontSize: 16.0),
              ),
              new Radio(
                value: 888,
                groupValue: radioValueMolecular,
                onChanged: _handleRadioValueChangeMoleculares,
              ),
            ])),
            Container(
                alignment: Alignment.centerLeft,
                child: RichText(
                  textAlign: TextAlign.left,
                  text: TextSpan(
                    text: definiciones[54],
                    style: TextStyle(
                        color: Color.fromRGBO(0, 0, 0, 1), fontSize: 18),
                  ),
                )),
          ]));
    }
  }

  widgetMapa() {
    return Container(
        height: 500,
        child: FlutterMap(
            options: new MapOptions(
              center: new LatLng(lat, lon),
              zoom: 15.0,
              interactive: false,
            ),
            layers: [
              new TileLayerOptions(
                  urlTemplate:
                      "https://api.mapbox.com/styles/v1/pacho0522/ckdex357s5hnv1inz5r1kbbog/tiles/256/{z}/{x}/{y}@2x?access_token=pk.eyJ1IjoicGFjaG8wNTIyIiwiYSI6ImNrZGFybWt6eTA3NzIycHQ4YTlnYjRuanUifQ.qXv2hwJ7NKNrKZDU5kR4dQ",
                  additionalOptions: {
                    'accessToken':
                        'pk.eyJ1IjoicGFjaG8wNTIyIiwiYSI6ImNrZGV3Yzc3cTBiajkycW5hczhzaG00cWUifQ.TrdKX7gvxKd3ghC-_uw8aA',
                    'id': 'mapbox.mapbox-streets-v7'
                  }),
              MarkerLayerOptions(markers: [
                Marker(
                    width: 45.0,
                    height: 45.0,
                    point: new LatLng(lat, lon),
                    builder: (context) => new Container(
                          child: IconButton(
                            icon: Icon(Icons.location_on),
                            color: Colors.blue,
                            iconSize: 30.0,
                            onPressed: () {},
                          ),
                        ))
              ]),
            ]));
  }

  _getLocation() async {
    try {
      geolocator
          .placemarkFromAddress(
              _direccionController.text + "Valle del Cauca" + "Candelaria")
          //.placemarkFromAddress("Cali")

          .then((List<Placemark> placemark) {
        setState(() {
          lat = placemark[0].position.latitude;
          lon = placemark[0].position.longitude;
        });

        print(placemark[0].position);
      }).catchError((e) {
        print(e);
      });
    } catch (e) {
      print(e);
    }

    print(lat);
  }

  widgetBotones() {
    return Container(
        padding: const EdgeInsets.only(left: 20.0, right: 20.0),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            RaisedButton(
              onPressed: () {
                if (indexFormularios == 0) {
                  Navigator.of(context).pushAndRemoveUntil(
                      MaterialPageRoute(
                          builder: (BuildContext context) => LoginPage()),
                      (Route<dynamic> route) => false);
                } else if (mapa == false) {
                  setState(() {
                    indexFormularios = indexFormularios - 1;
                  });
                }

                if (mapa = true) {
                  setState(() {
                    mapa = false;
                  });
                }

                if ((indexFormularios == 28 &&
                        viveCandelaria == true &&
                        (fechaSint == null) &&
                        esCovid < 1) ||
                    (indexFormularios == 28 &&
                        viveCandelaria == false &&
                        trabajaCandelaria == true &&
                        (fechaSint == null) &&
                        esCovid < 1) ||
                    (indexFormularios == 12 &&
                        viveCandelaria == false &&
                        trabajaCandelaria == false &&
                        (fechaSint == null) &&
                        esCovid < 1)) {
                  setState(() {
                    indexFormularios = indexFormularios - 1;
                  });
                }

                if ((indexFormularios == 30 &&
                        viveCandelaria == true &&
                        desplazamiento == false &&
                        (_lugarViajeController.text == "")) ||
                    (indexFormularios == 30 &&
                        viveCandelaria == false &&
                        trabajaCandelaria == true &&
                        desplazamiento == false &&
                        (_lugarViajeController.text == "")) ||
                    (indexFormularios == 14 &&
                        viveCandelaria == false &&
                        trabajaCandelaria == false &&
                        desplazamiento == false &&
                        (_lugarViajeController.text == ""))) {
                  setState(() {
                    indexFormularios = indexFormularios - 1;
                  });
                }

                if ((indexFormularios == 32 &&
                        viveCandelaria == true &&
                        (_lugarContactoController.text == "") &&
                        (radioValueContacto == 0 ||
                            radioValueContacto == 777) ||
                    (indexFormularios == 32 &&
                        viveCandelaria == false &&
                        trabajaCandelaria == true &&
                        (_lugarContactoController.text == "") &&
                        (radioValueContacto == 0 ||
                            radioValueContacto == 777)) ||
                    (indexFormularios == 16 &&
                        viveCandelaria == false &&
                        trabajaCandelaria == false &&
                        (_lugarContactoController.text == "") &&
                        (radioValueContacto == 0 ||
                            radioValueContacto == 777)))) {
                  setState(() {
                    indexFormularios = indexFormularios - 1;
                  });
                }
              },
              child: RichText(
                textAlign: TextAlign.center,
                text: TextSpan(
                  text: "Volver",
                  style: TextStyle(
                      color: Color.fromRGBO(193, 29, 27, 1), fontSize: 18),
                ),
              ),
            ),
            if ((viveCandelaria == null &&
                    trabajaCandelaria == null &&
                    indexFormularios != 36) ||
                (viveCandelaria == false &&
                    trabajaCandelaria == null &&
                    indexFormularios != 36) ||
                (viveCandelaria == true &&
                    trabajaCandelaria == null &&
                    indexFormularios != 36) ||
                (viveCandelaria == false &&
                    trabajaCandelaria == true &&
                    indexFormularios != 36) ||
                (viveCandelaria == false &&
                    trabajaCandelaria == false &&
                    indexFormularios != 20))
              RaisedButton(
                onPressed: () {
                  setState(() {
                    esCovid = radioValueTos +
                        radioValueDificultadRespiratoria +
                        radioValueDolorGarganta +
                        radioValueFiebre +
                        radioValueFatiga +
                        radioValueMalestar +
                        radioValueGusto +
                        radioValueOlfato +
                        radioValueNasal;

                    
                  });

                  setState(() {
                    indexFormularios = indexFormularios + 1;
                  });

                  if (indexFormularios == 1 &&
                      (identificacion == 0 ||
                          _nombreDelEncuestadoController.text == "" ||
                          _apellidosDelEncuestadoController.text == "" ||
                          _numeroDeIdentificacionController.text == "" ||
                          viveCandelaria == null)) {
                    _campoVacio();
                    setState(() {
                      indexFormularios = indexFormularios - 1;
                    });
                  }

                  if (indexFormularios == 2 &&
                      viveCandelaria == true &&                      
                      (corregimiento == 0 ||
                          vereda == 0 ||
                          callejon == 0 ||
                          barrio == 0 ||
                          _direccionController.text == "")) {
                    _campoVacio();
                    setState(() {
                      indexFormularios = indexFormularios - 1;
                    });
                          }

                 /* if (indexFormularios == 2 &&
                      viveCandelaria == true &&
                      mapa == false &&
                      (corregimiento == 0 ||
                          vereda == 0 ||
                          callejon == 0 ||
                          barrio == 0 ||
                          _direccionController.text == "")) {
                    _campoVacio();
                    setState(() {
                      indexFormularios = indexFormularios - 1;
                    });
                  } else if (indexFormularios == 2 &&
                      viveCandelaria == true &&
                      mapa == false &&
                      (corregimiento != 0 ||
                          vereda != 0 ||
                          callejon != 0 ||
                          barrio != 0 ||
                          _direccionController.text != "")) {
                    setState(() {
                      indexFormularios = indexFormularios - 1;
                      _getLocation();
                      mapa = true;
                    });
                  } else if (indexFormularios == 2 &&
                      viveCandelaria == true &&
                      mapa == true) {
                    setState(() {
                      mapa = false;
                    });
                  }*/

                  if ((indexFormularios == 3 &&
                          viveCandelaria == true &&
                          (ocupacion == 0 || trabajador == 0)) ||
                      (indexFormularios == 3 &&
                          viveCandelaria == false &&
                          trabajaCandelaria == true &&
                          (ocupacion == 0 || trabajador == 0))) {
                    _campoVacio();
                    setState(() {
                      indexFormularios = indexFormularios - 1;
                    });
                  }

                  if ((indexFormularios == 4 &&
                          viveCandelaria == true &&
                          (sexo == 0 ||
                              etnia == 0 ||
                              grupo == 0 ||
                              fechaNac == null)) ||
                      (indexFormularios == 4 &&
                          viveCandelaria == false &&
                          trabajaCandelaria == true &&
                          (sexo == 0 ||
                              etnia == 0 ||
                              grupo == 0 ||
                              fechaNac == null))) {
                    _campoVacio();
                    setState(() {
                      indexFormularios = indexFormularios - 1;
                    });
                  }

                  if ((indexFormularios == 5 &&
                          viveCandelaria == true &&
                          (vulnerable == 0)) ||
                      (indexFormularios == 5 &&
                          viveCandelaria == false &&
                          trabajaCandelaria == true &&
                          (vulnerable == 0))) {
                    _campoVacio();
                    setState(() {
                      indexFormularios = indexFormularios - 1;
                    });
                  }

                  if ((indexFormularios == 6 &&
                          viveCandelaria == true &&
                          (eps == 0 || dificultadEps == 0)) ||
                      (indexFormularios == 6 &&
                          viveCandelaria == false &&
                          trabajaCandelaria == true &&
                          (eps == 0 || dificultadEps == 0))) {
                    _campoVacio();
                    setState(() {
                      indexFormularios = indexFormularios - 1;
                    });
                  }

                  if ((indexFormularios == 6 &&
                          viveCandelaria == true &&
                          (eps == 25 && _nombreOtraEPSController.text == "")) ||
                      (indexFormularios == 6 &&
                          viveCandelaria == false &&
                          trabajaCandelaria == true &&
                          (eps == 25 && _nombreOtraEPSController.text == ""))) {
                    _campoVacio();
                    setState(() {
                      indexFormularios = indexFormularios - 1;
                    });
                  }

                  if ((indexFormularios == 7 &&
                          viveCandelaria == true &&
                          diabetes == null) ||
                      (indexFormularios == 7 &&
                          viveCandelaria == false &&
                          trabajaCandelaria == true &&
                          diabetes == null)) {
                    _campoVacio();
                    setState(() {
                      indexFormularios = indexFormularios - 1;
                    });
                  }

                  if ((indexFormularios == 8 &&
                          viveCandelaria == true &&
                          cardiovascular == null) ||
                      (indexFormularios == 8 &&
                          viveCandelaria == false &&
                          trabajaCandelaria == true &&
                          cardiovascular == null)) {
                    _campoVacio();
                    setState(() {
                      indexFormularios = indexFormularios - 1;
                    });
                  }

                  if ((indexFormularios == 9 &&
                          viveCandelaria == true &&
                          (cancer == null)) ||
                      (indexFormularios == 9 &&
                          viveCandelaria == false &&
                          trabajaCandelaria == true &&
                          (cancer == null))) {
                    _campoVacio();
                    setState(() {
                      indexFormularios = indexFormularios - 1;
                    });
                  }

                  if ((indexFormularios == 10 &&
                          viveCandelaria == true &&
                          (renal == null)) ||
                      (indexFormularios == 10 &&
                          viveCandelaria == false &&
                          trabajaCandelaria == true &&
                          (renal == null))) {
                    _campoVacio();
                    setState(() {
                      indexFormularios = indexFormularios - 1;
                    });
                  }

                  if ((indexFormularios == 11 &&
                          viveCandelaria == true &&
                          (asma == null)) ||
                      (indexFormularios == 11 &&
                          viveCandelaria == false &&
                          trabajaCandelaria == true &&
                          (asma == null))) {
                    _campoVacio();
                    setState(() {
                      indexFormularios = indexFormularios - 1;
                    });
                  }

                  if ((indexFormularios == 12 &&
                          viveCandelaria == true &&
                          (pulmonar == null)) ||
                      indexFormularios == 12 &&
                          viveCandelaria == false &&
                          trabajaCandelaria == true &&
                          (pulmonar == null)) {
                    _campoVacio();
                    setState(() {
                      indexFormularios = indexFormularios - 1;
                    });
                  }

                  if ((indexFormularios == 13 &&
                          viveCandelaria == true &&
                          (imunusupresion == null)) ||
                      (indexFormularios == 13 &&
                          viveCandelaria == false &&
                          trabajaCandelaria == true &&
                          (imunusupresion == null))) {
                    _campoVacio();
                    setState(() {
                      indexFormularios = indexFormularios - 1;
                    });
                  }

                  if ((indexFormularios == 14 &&
                          viveCandelaria == true &&
                          (obesidad == null)) ||
                      (indexFormularios == 14 &&
                          viveCandelaria == false &&
                          trabajaCandelaria == true &&
                          (obesidad == null))) {
                    _campoVacio();
                    setState(() {
                      indexFormularios = indexFormularios - 1;
                    });
                  }

                  if ((indexFormularios == 15 &&
                          viveCandelaria == true &&
                          (desnutricion == null)) ||
                      (indexFormularios == 15 &&
                          viveCandelaria == false &&
                          trabajaCandelaria == true &&
                          (desnutricion == null))) {
                    _campoVacio();
                    setState(() {
                      indexFormularios = indexFormularios - 1;
                    });
                  }

                  if ((indexFormularios == 16 &&
                          viveCandelaria == true &&
                          (neurologico == null)) ||
                      (indexFormularios == 16 &&
                          viveCandelaria == false &&
                          trabajaCandelaria == true &&
                          (neurologico == null))) {
                    _campoVacio();
                    setState(() {
                      indexFormularios = indexFormularios - 1;
                    });
                  }

                  if ((indexFormularios == 17 &&
                          viveCandelaria == true &&
                          (tabaquismo == null)) ||
                      (indexFormularios == 17 &&
                          viveCandelaria == false &&
                          trabajaCandelaria == true &&
                          (tabaquismo == null))) {
                    _campoVacio();
                    setState(() {
                      indexFormularios = indexFormularios - 1;
                    });
                  }

                  if ((indexFormularios == 18 &&
                          viveCandelaria == true &&
                          (alcoholismo == null)) ||
                      (indexFormularios == 18 &&
                          viveCandelaria == false &&
                          trabajaCandelaria == true &&
                          (alcoholismo == null))) {
                    _campoVacio();
                    setState(() {
                      indexFormularios = indexFormularios - 1;
                    });
                  }

                  if ((indexFormularios == 19 &&
                          viveCandelaria == true &&
                          (saludMental == 0)) ||
                      (indexFormularios == 19 &&
                          viveCandelaria == false &&
                          trabajaCandelaria == true &&
                          (saludMental == 0))) {
                    _campoVacio();
                    setState(() {
                      indexFormularios = indexFormularios - 1;
                    });
                  }

                  if ((indexFormularios == 20 &&
                          viveCandelaria == true &&
                          (tos == null)) ||
                      (indexFormularios == 20 &&
                          viveCandelaria == false &&
                          trabajaCandelaria == true &&
                          (tos == null)) ||
                      (indexFormularios == 4 &&
                          viveCandelaria == false &&
                          trabajaCandelaria == false &&
                          (tos == null))) {
                    _campoVacio();
                    setState(() {
                      indexFormularios = indexFormularios - 1;
                    });
                  }

                  if ((indexFormularios == 21 &&
                          viveCandelaria == true &&
                          (dificultadRespiratoria == null)) ||
                      (indexFormularios == 21 &&
                          viveCandelaria == false &&
                          trabajaCandelaria == true &&
                          (dificultadRespiratoria == null)) ||
                      (indexFormularios == 5 &&
                          viveCandelaria == false &&
                          trabajaCandelaria == false &&
                          (dificultadRespiratoria == null))) {
                    _campoVacio();
                    setState(() {
                      indexFormularios = indexFormularios - 1;
                    });
                  }

                  if ((indexFormularios == 22 &&
                          viveCandelaria == true &&
                          (dolorGarganta == null)) ||
                      (indexFormularios == 22 &&
                          viveCandelaria == false &&
                          trabajaCandelaria == true &&
                          (dolorGarganta == null)) ||
                      (indexFormularios == 6 &&
                          viveCandelaria == false &&
                          trabajaCandelaria == false &&
                          (dolorGarganta == null))) {
                    _campoVacio();
                    setState(() {
                      indexFormularios = indexFormularios - 1;
                    });
                  }

                  if ((indexFormularios == 23 &&
                          viveCandelaria == true &&
                          (fiebre == null)) ||
                      (indexFormularios == 23 &&
                          viveCandelaria == false &&
                          trabajaCandelaria == true &&
                          (fiebre == null)) ||
                      (indexFormularios == 7 &&
                          viveCandelaria == false &&
                          trabajaCandelaria == false &&
                          (fiebre == null))) {
                    _campoVacio();
                    setState(() {
                      indexFormularios = indexFormularios - 1;
                    });
                  }

                  if ((indexFormularios == 24 &&
                          viveCandelaria == true &&
                          (fatiga == null)) ||
                      (indexFormularios == 24 &&
                          viveCandelaria == false &&
                          trabajaCandelaria == true &&
                          (fatiga == null)) ||
                      (indexFormularios == 8 &&
                          viveCandelaria == false &&
                          trabajaCandelaria == false &&
                          (fatiga == null))) {
                    _campoVacio();
                    setState(() {
                      indexFormularios = indexFormularios - 1;
                    });
                  }

                  if ((indexFormularios == 25 &&
                          viveCandelaria == true &&
                          (malestar == null)) ||
                      (indexFormularios == 25 &&
                          viveCandelaria == false &&
                          trabajaCandelaria == true &&
                          (malestar == null)) ||
                      (indexFormularios == 9 &&
                          viveCandelaria == false &&
                          trabajaCandelaria == false &&
                          (malestar == null))) {
                    _campoVacio();
                    setState(() {
                      indexFormularios = indexFormularios - 1;
                    });
                  }

                  if ((indexFormularios == 26 &&
                          viveCandelaria == true &&
                          (gusto == null)) ||
                      (indexFormularios == 26 &&
                          viveCandelaria == false &&
                          trabajaCandelaria == true &&
                          (gusto == null)) ||
                      (indexFormularios == 10 &&
                          viveCandelaria == false &&
                          trabajaCandelaria == false &&
                          (gusto == null))) {
                    _campoVacio();
                    setState(() {
                      indexFormularios = indexFormularios - 1;
                    });
                  }

                  if ((indexFormularios == 27 &&
                          viveCandelaria == true &&
                          (olfato == null)) ||
                      (indexFormularios == 27 &&
                          viveCandelaria == false &&
                          trabajaCandelaria == true &&
                          (olfato == null)) ||
                      (indexFormularios == 11 &&
                          viveCandelaria == false &&
                          trabajaCandelaria == false &&
                          (olfato == null))) {
                    _campoVacio();
                    setState(() {
                      indexFormularios = indexFormularios - 1;
                    });
                  }

                  if ((indexFormularios == 28 &&
                          viveCandelaria == true &&
                          (nasal == null)) ||
                      (indexFormularios == 28 &&
                          viveCandelaria == false &&
                          trabajaCandelaria == true &&
                          (nasal == null)) ||
                      (indexFormularios == 12 &&
                          viveCandelaria == false &&
                          trabajaCandelaria == false &&
                          (nasal == null))) {
                    _campoVacio();
                    setState(() {
                      indexFormularios = indexFormularios - 1;
                    });
                  }

                  if ((indexFormularios == 28 &&
                          viveCandelaria == true &&
                          esCovid < 1 &&
                          (nasal != null)) ||
                      (indexFormularios == 28 &&
                          viveCandelaria == false &&
                          trabajaCandelaria == true &&
                          esCovid < 1 &&
                          (nasal != null)) ||
                      (indexFormularios == 12 &&
                          viveCandelaria == false &&
                          trabajaCandelaria == false &&
                          esCovid < 1 &&
                          (nasal != null))) {
                    setState(() {
                      indexFormularios = indexFormularios + 1;
                    });
                  }

                  if ((indexFormularios == 29 &&
                          viveCandelaria == true &&
                          (fechaSint == null) &&
                          esCovid > 0) ||
                      (indexFormularios == 29 &&
                          viveCandelaria == false &&
                          trabajaCandelaria == true &&
                          (fechaSint == null) &&
                          esCovid > 0) ||
                      (indexFormularios == 13 &&
                          viveCandelaria == false &&
                          trabajaCandelaria == false &&
                          (fechaSint == null) &&
                          esCovid > 0)) {
                    _campoVacio();
                    setState(() {
                      indexFormularios = indexFormularios - 1;
                    });
                  }

                  if ((indexFormularios == 30 &&
                          viveCandelaria == true &&
                          (desplazamiento == null)) ||
                      (indexFormularios == 30 &&
                          viveCandelaria == false &&
                          trabajaCandelaria == true &&
                          (desplazamiento == null)) ||
                      (indexFormularios == 14 &&
                          viveCandelaria == false &&
                          trabajaCandelaria == false &&
                          (desplazamiento == null))) {
                    _campoVacio();
                    setState(() {
                      indexFormularios = indexFormularios - 1;
                    });
                  }

                  if ((indexFormularios == 30 &&
                          viveCandelaria == true &&
                          (desplazamiento == false)) ||
                      (indexFormularios == 30 &&
                          viveCandelaria == false &&
                          trabajaCandelaria == true &&
                          (desplazamiento == false)) ||
                      (indexFormularios == 14 &&
                          viveCandelaria == false &&
                          trabajaCandelaria == false &&
                          (desplazamiento == false))) {
                    setState(() {
                      indexFormularios = indexFormularios + 1;
                    });
                  }

                  if ((indexFormularios == 31 &&
                          viveCandelaria == true &&
                          desplazamiento == true &&
                          (_lugarViajeController.text == "")) ||
                      (indexFormularios == 31 &&
                          viveCandelaria == false &&
                          trabajaCandelaria == true &&
                          desplazamiento == true &&
                          (_lugarViajeController.text == "")) ||
                      (indexFormularios == 15 &&
                          viveCandelaria == false &&
                          trabajaCandelaria == false &&
                          desplazamiento == true &&
                          (_lugarViajeController.text == ""))) {
                    _campoVacio();
                    setState(() {
                      indexFormularios = indexFormularios - 1;
                    });
                  }

                  if ((indexFormularios == 32 &&
                          viveCandelaria == true &&
                          (radioValueContacto == -1)) ||
                      (indexFormularios == 32 &&
                          viveCandelaria == false &&
                          trabajaCandelaria == true &&
                          (radioValueContacto == -1)) ||
                      (indexFormularios == 16 &&
                          viveCandelaria == false &&
                          trabajaCandelaria == false &&
                          (radioValueContacto == -1))) {
                    _campoVacio();
                    setState(() {
                      indexFormularios = indexFormularios - 1;
                    });
                  }

                  if ((indexFormularios == 32 &&
                          viveCandelaria == true &&
                          (radioValueContacto == 0 ||
                              radioValueContacto == 777)) ||
                      (indexFormularios == 32 &&
                          viveCandelaria == false &&
                          trabajaCandelaria == true &&
                          (radioValueContacto == 0 ||
                              radioValueContacto == 777)) ||
                      (indexFormularios == 16 &&
                          viveCandelaria == false &&
                          trabajaCandelaria == false &&
                          (radioValueContacto == 0 ||
                              radioValueContacto == 777))) {
                    setState(() {
                      indexFormularios = indexFormularios + 1;
                    });
                  }

                  if ((indexFormularios == 33 &&
                          viveCandelaria == true &&
                          (_lugarContactoController.text == "") &&
                          radioValueContacto == 1) ||
                      (indexFormularios == 33 &&
                          viveCandelaria == false &&
                          trabajaCandelaria == true &&
                          (_lugarContactoController.text == "") &&
                          radioValueContacto == 1) ||
                      (indexFormularios == 17 &&
                          viveCandelaria == false &&
                          trabajaCandelaria == false &&
                          (_lugarContactoController.text == "") &&
                          radioValueContacto == 1)) {
                    _campoVacio();
                    setState(() {
                      indexFormularios = indexFormularios - 1;
                    });
                  }

                  if ((indexFormularios == 34 &&
                          viveCandelaria == true &&
                          (temperatura == null)) ||
                      (indexFormularios == 34 &&
                          viveCandelaria == false &&
                          trabajaCandelaria == true &&
                          (temperatura == null)) ||
                      (indexFormularios == 18 &&
                          viveCandelaria == false &&
                          trabajaCandelaria == false &&
                          (temperatura == null))) {
                    _campoVacio();
                    setState(() {
                      indexFormularios = indexFormularios - 1;
                    });
                  }

                  if ((indexFormularios == 34 &&
                          viveCandelaria == true &&
                          temperatura == true &&
                          (_resultadoTempController.text == "")) ||
                      (indexFormularios == 34 &&
                          viveCandelaria == false &&
                          trabajaCandelaria == true &&
                          temperatura == true &&
                          (_resultadoTempController.text == "")) ||
                      (indexFormularios == 18 &&
                          viveCandelaria == false &&
                          trabajaCandelaria == false &&
                          temperatura == true &&
                          (_resultadoTempController.text == ""))) {
                    _campoVacio();
                    setState(() {
                      indexFormularios = indexFormularios - 1;
                    });
                  }

                 /* if ((indexFormularios == 35 &&
                          viveCandelaria == true &&
                          (casoSospechoso == null)) ||
                      (indexFormularios == 35 &&
                          viveCandelaria == false &&
                          trabajaCandelaria == true &&
                          (casoSospechoso == null)) ||
                      (indexFormularios == 19 &&
                          viveCandelaria == false &&
                          trabajaCandelaria == false &&
                          (casoSospechoso == null))) {
                    _campoVacio();
                    setState(() {
                      indexFormularios = indexFormularios - 1;
                    });
                  }*/

                  if ((indexFormularios == 35 &&
                          viveCandelaria == true &&
                          (radioValueAnticuerpos == -1)) ||
                      indexFormularios == 35 &&
                          viveCandelaria == false &&
                          trabajaCandelaria == true &&
                          (radioValueAnticuerpos == -1) ||
                      indexFormularios == 19 &&
                          viveCandelaria == false &&
                          trabajaCandelaria == false &&
                          (radioValueAnticuerpos == -1)) {
                    _campoVacio();
                    setState(() {
                      indexFormularios = indexFormularios - 1;
                    });
                  }

                  if ((indexFormularios == 36 &&
                          viveCandelaria == true &&
                          (radioValueAntigenos == -1)) ||
                      (indexFormularios == 36 &&
                          viveCandelaria == false &&
                          trabajaCandelaria == true &&
                          (radioValueAntigenos == -1)) ||
                      (indexFormularios == 20 &&
                          viveCandelaria == false &&
                          trabajaCandelaria == false &&
                          (radioValueAntigenos == -1))) {
                    _campoVacio();
                    setState(() {
                      indexFormularios = indexFormularios - 1;
                    });
                  }

////////////////////////////// Segundo Camino

                  if (indexFormularios == 2 &&
                      (viveCandelaria == false && trabajaCandelaria == null)) {
                    _campoVacio();
                    setState(() {
                      indexFormularios = indexFormularios - 1;
                    });
                  }

                  ////////////////////////////// Tercer Camino

                  if (indexFormularios == 3 &&
                      (viveCandelaria == false && trabajaCandelaria == false) &&
                      (departamento == 0 ||
                          municipio == 0 ||
                          _frecuenciaMensualController.text == "" ||
                          _lugarFrecuentaController.text == "" ||
                          _motivoVisitaController.text == "" ||
                          _tiempoVisitaController.text == "")) {
                    _campoVacio();
                    setState(() {
                      indexFormularios = indexFormularios - 1;
                    });
                  }
                },
                child: RichText(
                  textAlign: TextAlign.center,
                  text: TextSpan(
                    text: "Siguiente",
                    style: TextStyle(
                        color: Color.fromRGBO(193, 29, 27, 1), fontSize: 18),
                  ),
                ),
              ),
            if (indexFormularios == 20 &&
                viveCandelaria == false &&
                trabajaCandelaria == false)
              RaisedButton(
                onPressed: () {
                  if ((viveCandelaria == true && (radioValueMolecular == -1)) ||
                      (viveCandelaria == false &&
                          trabajaCandelaria == true &&
                          (radioValueMolecular == -1)) ||
                      (viveCandelaria == false &&
                          trabajaCandelaria == false &&
                          (radioValueMolecular == -1))) {
                    _campoVacio();
                  } else {
                    _save();
                  }
                },
                child: RichText(
                  textAlign: TextAlign.center,
                  text: TextSpan(
                    text: "Finalizar Encuesta",
                    style: TextStyle(
                        color: Color.fromRGBO(193, 29, 27, 1), fontSize: 18),
                  ),
                ),
              ),
            if (indexFormularios == 36)
              RaisedButton(
                onPressed: () {
                  if ((viveCandelaria == true && (radioValueMolecular == -1)) ||
                      (viveCandelaria == false &&
                          trabajaCandelaria == true &&
                          (radioValueMolecular == -1))) {
                    _campoVacio();
                  } else {
                    _save();
                  }
                },
                child: RichText(
                  textAlign: TextAlign.center,
                  text: TextSpan(
                    text: "Finalizar Encuesta",
                    style: TextStyle(
                        color: Color.fromRGBO(193, 29, 27, 1), fontSize: 18),
                  ),
                ),
              ),
          ],
        ));
  }

  widgetProgress() {
    if (viveCandelaria == false && trabajaCandelaria == false) {
      return Padding(
        padding: EdgeInsets.all(15.0),
        child: new LinearPercentIndicator(
          width: MediaQuery.of(context).size.width - 50,
          animation: true,
          lineHeight: 20.0,
          animationDuration: 2000,
          percent: (indexFormularios + 1) / 21,
          center: Text((indexFormularios + 1).toString() + "/" + "21"),
          linearStrokeCap: LinearStrokeCap.roundAll,
          progressColor: Colors.greenAccent,
        ),
      );
    } else {
      return Padding(
        padding: EdgeInsets.all(15.0),
        child: new LinearPercentIndicator(
          width: MediaQuery.of(context).size.width - 50,
          animation: true,
          lineHeight: 20.0,
          animationDuration: 2000,
          percent: (indexFormularios + 1) / 37,
          center: Text((indexFormularios + 1).toString() + "/" + "37"),
          linearStrokeCap: LinearStrokeCap.roundAll,
          progressColor: Colors.greenAccent,
        ),
      );
    }
  }

  Future<void> borrarEncuesta() async {
    return showDialog<void>(
      context: context,
      barrierDismissible: false, // user must tap button!
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text('Se eliminarán los datos'),
          content: SingleChildScrollView(
            child: ListBody(
              children: <Widget>[
                Text(
                    'Desea regresar, si lo hace los datos que lleno previamente se eliminarán'),
              ],
            ),
          ),
          actions: <Widget>[
            FlatButton(
              child: Text('Si'),
              onPressed: () {
                Navigator.of(context).pushAndRemoveUntil(
                    MaterialPageRoute(
                        builder: (BuildContext context) =>
                            BuscarEmpresaPage(idUsuario: "idUsuario")),
                    (Route<dynamic> route) => false);
              },
            ),
            FlatButton(
              child: Text('No'),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
          ],
        );
      },
    );
  }

  Future<void> _campoVacio() async {
    return showDialog<void>(
      context: context,
      barrierDismissible: false, // user must tap button!
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text('Hay un campo vacío'),
          content: SingleChildScrollView(
            child: ListBody(
              children: <Widget>[
                Text('Por favor rellene todos los campos'),
              ],
            ),
          ),
          actions: <Widget>[
            FlatButton(
              child: Text('Ok'),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
          ],
        );
      },
    );
  }

  _save() async {
    EncuestaCiudadano encuesta = EncuestaCiudadano();

    encuesta.nombre = _nombreDelEncuestadoController.text;
    encuesta.apellidos = _apellidosDelEncuestadoController.text;
    encuesta.tipoIdentificacion = identificacion;
    encuesta.identificacion = _numeroDeIdentificacionController.text;
    encuesta.telefono = _telefonoController.text;
    if(viveCandelaria == true){
    encuesta.viveCandelaria = "True";
    }
    else{
    encuesta.viveCandelaria = "False";
    }
    if(trabajaCandelaria == true){
    encuesta.trabajaCandelaria = "True";
    }
    else{
    encuesta.trabajaCandelaria = "False";
    }
    if(departamento == 0){
      encuesta.departamento = 76;
    }else{
      encuesta.departamento = departamento;
    }
    if(municipio == 0){
      encuesta.municipio = 1017;
    }else{
      encuesta.municipio = municipio;
    }
 if(corregimiento == 0){
      encuesta.corregimiento = 1;
    }else{
      encuesta.corregimiento = corregimiento;
    }


     if(vereda == 0){
      encuesta.vereda = 888;
    }else{
      encuesta.vereda = vereda;
    }

    if(callejon == 0){
      encuesta.callejon = 888;
    }else{
      encuesta.callejon = callejon;
    }

    if(barrio == 0){
      encuesta.barrio = 888;
    }else{
      encuesta.barrio = barrio;
    }

    


    encuesta.direccion = _direccionController.text;
    encuesta.frecuenciaVisita = _frecuenciaMensualController.text;
    encuesta.lugarFrecuenta = _lugarFrecuentaController.text;
    encuesta.motivoVisita = _motivoVisitaController.text;
    encuesta.tiempoVisita = _tiempoVisitaController.text;

    if(sexo == 0){
       encuesta.sexo = 1;
    }else{
      encuesta.sexo = sexo;
    }

    if(fechaView == ""){
      encuesta.nacimiento = "1111-11-11";
    }else{
      encuesta.nacimiento = fechaView;
    }

    if(etnia == 0){
      encuesta.etnia = 6;
    }else{
      encuesta.etnia = etnia;
    }

    if(grupo == 0){
      encuesta.grupoPoblacional = 11;
    }else{
      encuesta.grupoPoblacional = grupo;
    }

    if(vulnerable == 0){
      encuesta.vulnerabilidad = 10;
    }else{
      encuesta.vulnerabilidad = vulnerable;
    }

    if(ocupacion == 0){
      encuesta.ocupacion = 7;
    }else{
      encuesta.ocupacion = ocupacion;
    }

    if(trabajador == 0){
      encuesta.tipoTrabajador = 888;
    }else{
      encuesta.tipoTrabajador = trabajador;
    }


    encuesta.nombreEmpresa = _nombreEmpresaController.text;
    encuesta.direccionEmpresa = _direccionEmpresaController.text;

    if(eps == 0){
      encuesta.eps = 777;
    }else{
      encuesta.eps = eps;
    }
   

    encuesta.otraEps = _nombreOtraEPSController.text;

    if(dificultadEps == 0){
      encuesta.dificultadEps = 888;
    }else{
      encuesta.dificultadEps = dificultadEps;
    }

    if(diabetes == false){
      encuesta.diabetes = "False";
    }else{
      encuesta.diabetes = "True";
    }

    if(cardiovascular == false){
      encuesta.enfermedadCardiovascular = "False";
    }else{
      encuesta.enfermedadCardiovascular = "True";
    }

    if(cancer == false){
      encuesta.cancer = "False";
    }else{
      encuesta.cancer = "True";
    }

    if(renal == false){
      encuesta.enfermedadRenal = "False";
    }else{
      encuesta.enfermedadRenal = "True";
    }

    if(asma == false){
      encuesta.asma = "False";
    }else{
      encuesta.asma = "True";
    }

    if(pulmonar == false){
      encuesta.enfermedadPulmonar = "False";
    }else{
      encuesta.enfermedadPulmonar = "True";
    }

     if(imunusupresion == false){
      encuesta.imunusupresion = "False";
    }else{
      encuesta.imunusupresion = "True";
    }

     if(obesidad == false){
      encuesta.obesidad = "False";
    }else{
      encuesta.obesidad = "True";
    }

     if(desnutricion == false){
      encuesta.desnutricion = "False";
    }else{
      encuesta.desnutricion = "True";
    }

    if(neurologico == false){
      encuesta.trastornoNeurologico = "False";
    }else{
      encuesta.trastornoNeurologico = "True";
    }

    if(tabaquismo == false){
      encuesta.tabaquismo = "False";
    }else{
      encuesta.tabaquismo = "True";
    }

    if(alcoholismo == false){
      encuesta.alcoholismo = "False";
    }else{
      encuesta.alcoholismo = "True";
    }
    
    if(saludMental == 0){
      encuesta.saludMental = 10;
    }else{
      encuesta.saludMental = saludMental;
    }


    if(tos == false){
      encuesta.tos = "False";
    }else{
      encuesta.tos = "True";
    }

     if(dificultadRespiratoria == false){
      encuesta.dificultadRespiratoria = "False";
    }else{
      encuesta.dificultadRespiratoria = "True";
    }

     if(dolorGarganta == false){
      encuesta.dolorGarganta = "False";
    }else{
      encuesta.dolorGarganta = "True";
    }

     if(fiebre == false){
      encuesta.fiebre = "False";
    }else{
      encuesta.fiebre = "True";
    }

     if(fatiga == false){
      encuesta.fatiga = "False";
    }else{
      encuesta.fatiga = "True";
    }

      if(malestar == false){
      encuesta.malestarGeneral = "False";
    }else{
      encuesta.malestarGeneral = "True";
    }

     if(gusto == false){
      encuesta.disminucionGusto = "False";
    }else{
      encuesta.disminucionGusto = "True";
    }

     if(olfato == false){
      encuesta.disminucionOlfato = "False";
    }else{
      encuesta.disminucionOlfato = "True";
    }

      if(nasal == false){
      encuesta.secrecionNasal = "False";
    }else{
      encuesta.secrecionNasal = "True";
    }

    if(fechaSintView == ""){
      encuesta.fechaInicioSintomas = "1111-11-11";
    }else{
       encuesta.fechaInicioSintomas = fechaSintView;
    }


      if(desplazamiento == false){
      encuesta.desplazamientoUltimos = "False";
    }else{
      encuesta.desplazamientoUltimos = "True";
    }



    encuesta.lugarViaje = _lugarViajeController.text;

  if(contacto == false){
      encuesta.contactoPosible = "False";
    }else{
      encuesta.contactoPosible = "True";
    }



    encuesta.lugarContacto = _lugarContactoController.text;

if(temperatura == false){
      encuesta.pruebaTemperatura = "False";
    }else{
      encuesta.pruebaTemperatura = "True";
    }



    encuesta.resultadoPruebaTemp = _resultadoTempController.text;
    
    if (esCovid > 1) {
     encuesta.casoSospechoso = "True";
    } else {
      encuesta.casoSospechoso = "False";
                    }
    encuesta.pruebaAnticuerpos = radioValueAnticuerpos.toString();
    encuesta.pruebaAntigenos = radioValueAntigenos.toString();
    encuesta.pruebaMolecular = radioValueMolecular.toString();
    encuesta.latitud = "0";
    encuesta.longitud = "0";
    encuesta.estado = "false";

    DatabaseEncuestaCiudadano helper = DatabaseEncuestaCiudadano.instance;
    int id = await helper.insertEncuestaCiudadano(encuesta);
    print('inserted row: $id');

    Navigator.of(context).pushAndRemoveUntil(
        MaterialPageRoute(builder: (BuildContext context) => LoginPage()),
        (Route<dynamic> route) => false);
  }

  Future<bool> _onBackPressed() {
    return showDialog(
          context: context,
          builder: (context) => new AlertDialog(
            title: new Text('Salir de la aplicación'),
            content: new Text('¿Quiere salir de la aplicación?'),
            actions: <Widget>[
              new FlatButton(
                child: new Text("Si"),
                onPressed: () {
                  Navigator.of(context).pop(true);
                },
              ),
              new FlatButton(
                child: new Text("No"),
                onPressed: () {
                  Navigator.of(context).pop(false);
                },
              ),
              SizedBox(height: 16),
            ],
          ),
        ) ??
        false;
  }
}
