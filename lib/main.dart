import 'package:flutter/material.dart';
import 'login.dart';
import 'package:flutter/services.dart';
import 'dart:io';


/*void main() {
  WidgetsFlutterBinding.ensureInitialized();
  SystemChrome.setPreferredOrientations([DeviceOrientation.landscapeRight])
    .then((_) {
      runApp(new MyApp());
    });
}*/

Future<bool> addSelfSingedCertificate() async{
  ByteData data = await rootBundle.load('assets/raw/certificate.pem');
  SecurityContext context = SecurityContext.defaultContext;
  context.setTrustedCertificatesBytes(data.buffer.asUint8List());
  return true;
}


void main() async{ 
  WidgetsFlutterBinding.ensureInitialized(); 
  await addSelfSingedCertificate();
      runApp(new MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
      home: LoginPage(title: 'Flutter Demo Home Page'),
    );
  }
}
