import 'package:flutter/material.dart';
import 'package:sqflite/sqflite.dart';
import 'encuesta.dart';
import './db/database_helpers.dart';
import 'login.dart';
import 'dart:async';

final _nitController = TextEditingController();

/*final _arlController = TextEditingController();
final _epsController = TextEditingController();
final _afpController = TextEditingController();

bool arl = false;
bool eps = false;
bool afp = false;

bool protocolo = false;
bool certificado = false;*/

final _nombreRepController = TextEditingController();
final _cedulaRepController = TextEditingController();
final _nombreResController = TextEditingController();
final _cedulaResController = TextEditingController();
final _normativaController = TextEditingController();
bool normativa = false;

int idEmpresa = 0;
String nombreEmpresa = "";
String nitEmpresa = "";
String telefono = "";
String correo = "";
String direccion = "";
String corregimiento = "";
//String actividad = "";
//String categoria = "";
String encuestada = "";
String representante = "";

class BuscarEmpresaPage extends StatefulWidget {
  BuscarEmpresaPage({Key key, this.idUsuario}) : super(key: key);
  String idUsuario;

  @override
  _BuscarEmpresaPage createState() => _BuscarEmpresaPage();
}

class _BuscarEmpresaPage extends State<BuscarEmpresaPage>
    with SingleTickerProviderStateMixin {
  int _tabIndex = 0;
  TabController _tabController;
  double _progress = 0;

  startTimer() {
    new Timer.periodic(
      Duration(seconds: 1),
      (Timer timer) => setState(
        () {
          if (_progress == 1) {
            timer.cancel();
          } else {
            _progress += 0.25;
          }
        },
      ),
    );
  }

  @override
  void initState() {
   
    super.initState();
    _tabController = TabController(vsync: this, length: 5);
  }

  void _toggleTab() {
    _tabIndex = _tabController.index + 1;
    _tabController.animateTo(_tabIndex);
  }

  void _toggleTabPrev() {
    _tabIndex = _tabController.index - 1;
    _tabController.animateTo(_tabIndex);
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
        onWillPop: _onBackPressed,
        child: Scaffold(
        body: SafeArea(
      child: DefaultTabController(
        length: 1,
        child: Column(
          children: <Widget>[
            Container(
              constraints: BoxConstraints(maxHeight: 150.0),
              child: Material(
                color: Colors.indigo,
                child: TabBar(
                  controller: _tabController,
                  tabs: [
                    Tab(text: "1"),
                    Tab(text: "2"),
                    Tab(text: "3"),
                    Tab(text: "4"),
                    Tab(text: "5"),
                  ],
                ),
              ),
            ),
            Expanded(
              child: TabBarView(
                physics: NeverScrollableScrollPhysics(),
                controller: _tabController,
                children: [
                  widgetFormularioRepresentante(),
                  widgetFormularioResponsable(),
                  widgetFormularioNormativa(),
                  widgetFormularioNit(),
                  widgetFormularioEmpresa(),
                ],
              ),
            ),
          ],
        ),
      ),
    )));
  }

  widgetTitulo() {
    return Container(
        margin: const EdgeInsets.only(top: 5.0, bottom: 20.0),
        child: RichText(
          textAlign: TextAlign.center,
          text: TextSpan(
            text:
                "Ingrese el NIT de la empresa a la que se realizará la encuesta",
            style:
                TextStyle(color: Color.fromRGBO(193, 29, 27, 1), fontSize: 20),
          ),
        ));
  }

  Widget widgetFormularioNit() {
    return Container(
        padding: const EdgeInsets.only(left: 20.0, right: 20.0, top: 10.0),
        child: Column(children: <Widget>[
          widgetTitulo(),
          Container(
              padding: const EdgeInsets.only(bottom: 20.0),
              child: TextFormField(
                controller: _nitController,
                decoration: const InputDecoration(
                  hintText: 'NIT',
                  border: const OutlineInputBorder(),
                  filled: true,
                  fillColor: Colors.white,
                ),
              )),
          Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                RaisedButton(
                  color: Color.fromRGBO(193, 29, 27, 1),
                  onPressed: () {
                    _toggleTabPrev();
                  },
                  child: RichText(
                    textAlign: TextAlign.center,
                    text: TextSpan(
                      text: "Volver",
                      style: TextStyle(
                          color: Color.fromRGBO(255, 255, 255, 1),
                          fontSize: 18),
                    ),
                  ),
                ),
                RaisedButton(
                  color: Color.fromRGBO(193, 29, 27, 1),
                  onPressed: () {
                    if (_nitController.text == "") {
                      _campoVacio();
                    } else {
                      setState(() {
                        buscar(_nitController.text);
                      });
                    }
                  },
                  child: RichText(
                    textAlign: TextAlign.center,
                    text: TextSpan(
                      text: "Buscar",
                      style: TextStyle(
                          color: Color.fromRGBO(255, 255, 255, 1),
                          fontSize: 18),
                    ),
                  ),
                ),
              ])
        ]));
  }

  widgetFormularioEmpresa() {
    if(nitEmpresa == ""){
      return Column(
         mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                RichText(
              text: TextSpan(
                text: "No hay información de la empresa, vuelva para buscar mediante el NIT",
                style:
                    TextStyle(color: Color.fromRGBO(0, 0, 0, 1), fontSize: 20),
              ),
            ),
                RaisedButton(
                  color: Color.fromRGBO(193, 29, 27, 1),
                  onPressed: () {
                    _toggleTabPrev();
                  },
                  child: RichText(
                    textAlign: TextAlign.center,
                    text: TextSpan(
                      text: "Volver",
                      style: TextStyle(
                          color: Color.fromRGBO(255, 255, 255, 1),
                          fontSize: 18),
                    ),
                  ),
                )]);
    }
    if (_progress != 1) {
      return Padding(
            padding: EdgeInsets.only(top: 80.0, bottom:80.0, left: 250.0, right: 250.0 ),
          child: CircularProgressIndicator(
            strokeWidth: 25,
            backgroundColor: Colors.cyanAccent,                                            
            valueColor: new AlwaysStoppedAnimation<Color>(Colors.red),
            value: _progress,
          ));
    } else {
      return Container(
          padding: const EdgeInsets.only(left: 20.0, right: 20.0, top: 10.0),
          child:
              Column(crossAxisAlignment: CrossAxisAlignment.start, children: <
                  Widget>[
            RichText(
              text: TextSpan(
                text: "Información de la empresa",
                style:
                    TextStyle(color: Color.fromRGBO(0, 0, 0, 1), fontSize: 30),
              ),
            ),
            RichText(
              text: TextSpan(
                text: "Nombre de la empresa: ",
                style:
                    TextStyle(color: Color.fromRGBO(0, 0, 0, 1), fontSize: 20),
                children: <TextSpan>[
                  TextSpan(
                      text: nombreEmpresa,
                      style: TextStyle(fontWeight: FontWeight.bold)),
                ],
              ),
            ),
            RichText(
              textAlign: TextAlign.center,
              text: TextSpan(
                text: "NIT de la empresa: ",
                style:
                    TextStyle(color: Color.fromRGBO(0, 0, 0, 1), fontSize: 20),
                children: <TextSpan>[
                  TextSpan(
                      text: nitEmpresa.toString(),
                      style: TextStyle(fontWeight: FontWeight.bold)),
                ],
              ),
            ),
            RichText(
              textAlign: TextAlign.center,
              text: TextSpan(
                text: "Número Telefónico: ",
                style:
                    TextStyle(color: Color.fromRGBO(0, 0, 0, 1), fontSize: 20),
                children: <TextSpan>[
                  TextSpan(
                      text: telefono.toString(),
                      style: TextStyle(fontWeight: FontWeight.bold)),
                ],
              ),
            ),
            RichText(
              textAlign: TextAlign.center,
              text: TextSpan(
                text: "Correo electrónico: ",
                style:
                    TextStyle(color: Color.fromRGBO(0, 0, 0, 1), fontSize: 20),
                children: <TextSpan>[
                  TextSpan(
                      text: correo,
                      style: TextStyle(fontWeight: FontWeight.bold)),
                ],
              ),
            ),
            RichText(
              textAlign: TextAlign.center,
              text: TextSpan(
                text: "Dirección de la empresa: ",
                style:
                    TextStyle(color: Color.fromRGBO(0, 0, 0, 1), fontSize: 20),
                children: <TextSpan>[
                  TextSpan(
                      text: direccion,
                      style: TextStyle(fontWeight: FontWeight.bold)),
                ],
              ),
            ),
            RichText(
              textAlign: TextAlign.center,
              text: TextSpan(
                text: "Corregimiento: ",
                style:
                    TextStyle(color: Color.fromRGBO(0, 0, 0, 1), fontSize: 20),
                children: <TextSpan>[
                  TextSpan(
                      text: corregimiento,
                      style: TextStyle(fontWeight: FontWeight.bold)),
                ],
              ),
            ),
            /*RichText(
              textAlign: TextAlign.center,
              text: TextSpan(
                text: "Actividad: ",
                style:
                    TextStyle(color: Color.fromRGBO(0, 0, 0, 1), fontSize: 20),
                children: <TextSpan>[
                  TextSpan(
                      text: actividad,
                      style: TextStyle(fontWeight: FontWeight.bold)),
                ],
              ),
            ),
            RichText(
              textAlign: TextAlign.center,
              text: TextSpan(
                text: "Categoría: ",
                style:
                    TextStyle(color: Color.fromRGBO(0, 0, 0, 1), fontSize: 20),
                children: <TextSpan>[
                  TextSpan(
                      text: categoria,
                      style: TextStyle(fontWeight: FontWeight.bold)),
                ],
              ),
            ),
            RichText(
              textAlign: TextAlign.center,
              text: TextSpan(
                text: "Representante legal: ",
                style:
                    TextStyle(color: Color.fromRGBO(0, 0, 0, 1), fontSize: 20),
                children: <TextSpan>[
                  TextSpan(
                      text: representante,
                      style: TextStyle(fontWeight: FontWeight.bold)),
                ],
              ),
            ),*/
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                RaisedButton(
                  color: Color.fromRGBO(193, 29, 27, 1),
                  onPressed: () {
                    _toggleTabPrev();
                  },
                  child: RichText(
                    textAlign: TextAlign.center,
                    text: TextSpan(
                      text: "Volver",
                      style: TextStyle(
                          color: Color.fromRGBO(255, 255, 255, 1),
                          fontSize: 18),
                    ),
                  ),
                ),
                if (nitEmpresa != "" &&
                    _nombreResController.text != "" &&
                    _cedulaResController.text != "" &&
                    _nombreRepController.text != "" &&
                    _cedulaRepController.text != "")
                  RaisedButton(
                    color: Color.fromRGBO(193, 29, 27, 1),
                    onPressed: () {
                      Map encuesta = {
                        "id_empresa": idEmpresa,
                        "responsable_sst": _nombreResController.text,
                        "identificacion_sst": _cedulaResController.text,
                        "encuestado": _nombreRepController.text,
                        "identificacion_encuestado": _cedulaRepController.text,
                        "normatividad": normativa.toString(),
                        "descripcion_normatividad": _normativaController.text
                      };

                      if (encuestada == "false") {
                        Navigator.of(context).pushAndRemoveUntil(
                            MaterialPageRoute(
                                builder: (BuildContext context) => EncuestaPage(
                                    idUsuario: "idUsuario",
                                    encuesta: encuesta)),
                            (Route<dynamic> route) => false);
                      } else {
                        yaFueEncuestada();
                      }
                    },
                    child: RichText(
                      textAlign: TextAlign.center,
                      text: TextSpan(
                        text: "Empezar Encuesta",
                        style: TextStyle(
                            color: Color.fromRGBO(255, 255, 255, 1),
                            fontSize: 18),
                      ),
                    ),
                  ),
              ],
            )
          ]));
    }
  }

  Widget widgetFormularioRepresentante() {
    return Container(
        padding: const EdgeInsets.only(left: 20.0, right: 20.0, top: 10.0),
        child: Column(children: <Widget>[
          Container(
              padding: const EdgeInsets.only(bottom: 20.0),
              child: TextFormField(
                controller: _nombreRepController,
                decoration: const InputDecoration(
                  hintText: 'NOMBRE DEL REPRESENTANTE LEGAL',
                  border: const OutlineInputBorder(),
                  filled: true,
                  fillColor: Colors.white,
                ),
              )),
          Container(
              padding: const EdgeInsets.only(bottom: 20.0),
              child: TextFormField(
                controller: _cedulaRepController,
                decoration: const InputDecoration(
                  hintText: 'CÉDULA DEL REPRESENTANTE LEGAL',
                  border: const OutlineInputBorder(),
                  filled: true,
                  fillColor: Colors.white,
                ),
                keyboardType: TextInputType.number,
              )),
          Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                RaisedButton(
                  color: Color.fromRGBO(193, 29, 27, 1),
                  onPressed: () {
                    Navigator.of(context).pushAndRemoveUntil(
                        MaterialPageRoute(
                            builder: (BuildContext context) => LoginPage()),
                        (Route<dynamic> route) => false);
                  },
                  child: RichText(
                    textAlign: TextAlign.center,
                    text: TextSpan(
                      text: "Volver",
                      style: TextStyle(
                          color: Color.fromRGBO(255, 255, 255, 1),
                          fontSize: 18),
                    ),
                  ),
                ),
                RaisedButton(
                  color: Color.fromRGBO(193, 29, 27, 1),
                  onPressed: () {
                    if (_nombreRepController.text == "" ||
                        _cedulaRepController.text == "") {
                      _campoVacio();
                    } else {
                      _toggleTab();
                    }
                  },
                  child: RichText(
                    textAlign: TextAlign.center,
                    text: TextSpan(
                      text: "Siguiente",
                      style: TextStyle(
                          color: Color.fromRGBO(255, 255, 255, 1),
                          fontSize: 18),
                    ),
                  ),
                ),
              ])
        ]));
  }

  Widget widgetFormularioResponsable() {
    return Container(
        padding: const EdgeInsets.only(left: 20.0, right: 20.0, top: 10.0),
        child: Column(children: <Widget>[
          Container(
              padding: const EdgeInsets.only(bottom: 20.0),
              child: TextFormField(
                controller: _nombreResController,
                decoration: const InputDecoration(
                  hintText: 'NOMBRE DEL RESPONSABLE DE SST',
                  border: const OutlineInputBorder(),
                  filled: true,
                  fillColor: Colors.white,
                ),
              )),
          Container(
              padding: const EdgeInsets.only(bottom: 20.0),
              child: TextFormField(
                controller: _cedulaResController,
                decoration: const InputDecoration(
                  hintText: 'CÉDULA DEL RESPONSABLE DE SST',
                  border: const OutlineInputBorder(),
                  filled: true,
                  fillColor: Colors.white,
                ),
                keyboardType: TextInputType.number,
              )),
          Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                RaisedButton(
                  color: Color.fromRGBO(193, 29, 27, 1),
                  onPressed: () {
                    _toggleTabPrev();
                  },
                  child: RichText(
                    textAlign: TextAlign.center,
                    text: TextSpan(
                      text: "Volver",
                      style: TextStyle(
                          color: Color.fromRGBO(255, 255, 255, 1),
                          fontSize: 18),
                    ),
                  ),
                ),
                RaisedButton(
                  color: Color.fromRGBO(193, 29, 27, 1),
                  onPressed: () {
                    if (_nombreResController.text == "" ||
                        _cedulaResController.text == "") {
                      _campoVacio();
                    } else {
                      _toggleTab();
                    }
                  },
                  child: RichText(
                    textAlign: TextAlign.center,
                    text: TextSpan(
                      text: "Siguiente",
                      style: TextStyle(
                          color: Color.fromRGBO(255, 255, 255, 1),
                          fontSize: 18),
                    ),
                  ),
                ),
              ])
        ]));
  }

  Widget widgetFormularioNormativa() {
    return Container(
        padding: const EdgeInsets.only(left: 20.0, right: 20.0, top: 10.0),
        child: Column(children: <Widget>[
          Row(children: <Widget>[
            RichText(
              textAlign: TextAlign.center,
              text: TextSpan(
                text: "NORMATIVIDAD ADICIONAL DE LA RESOLUCIÓN 666 DEL 2020",
                style: TextStyle(
                    color: Color.fromRGBO(100, 100, 100, 1), fontSize: 18),
              ),
            ),
            Checkbox(
              value: normativa,
              onChanged: (bool value) {
                setState(() {
                  normativa = value;
                });
              },
            )
          ]),
          if (normativa == true)
            Container(
                padding: const EdgeInsets.only(bottom: 20.0),
                child: TextFormField(
                  controller: _normativaController,
                  decoration: const InputDecoration(
                    hintText: '¿CUÁL?',
                    border: const OutlineInputBorder(),
                    filled: true,
                    fillColor: Colors.white,
                  ),
                )),
          Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                RaisedButton(
                  color: Color.fromRGBO(193, 29, 27, 1),
                  onPressed: () {
                    _toggleTabPrev();
                  },
                  child: RichText(
                    textAlign: TextAlign.center,
                    text: TextSpan(
                      text: "Volver",
                      style: TextStyle(
                          color: Color.fromRGBO(255, 255, 255, 1),
                          fontSize: 18),
                    ),
                  ),
                ),
                RaisedButton(
                  color: Color.fromRGBO(193, 29, 27, 1),
                  onPressed: () {
                    if (normativa == true && _normativaController.text == "") {
                      _campoVacio();
                    } else {
                      _toggleTab();
                    }
                  },
                  child: RichText(
                    textAlign: TextAlign.center,
                    text: TextSpan(
                      text: "Siguiente",
                      style: TextStyle(
                          color: Color.fromRGBO(255, 255, 255, 1),
                          fontSize: 18),
                    ),
                  ),
                ),
              ])
        ]));
  }

  buscar(nit) async {
    DatabaseHelper helper = DatabaseHelper.instance;
    Empresa empresa = await helper.queryProducto(nit.toString());
    if (empresa != null) {
      setState(() {
        idEmpresa = empresa.idEmpresa;
        nombreEmpresa = empresa.nombre;
        nitEmpresa = empresa.nit;
        telefono = empresa.telefono;
        correo = empresa.correo;
        direccion = empresa.direccion;
        corregimiento = empresa.corregimiento;
        encuestada = empresa.encuestada;
        representante = empresa.representante;
        print(idEmpresa);
      });
      if (nitEmpresa != "") {
        startTimer();
        _toggleTab();
      }
    } else {
      _noExisteEmpresa();
    }
  }

  Future<void> _noExisteEmpresa() async {
    return showDialog<void>(
      context: context,
      barrierDismissible: false, // user must tap button!
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text('La empresa con el NIT: ' +
              _nitController.text +
              ' No se encontro en la base de datos'),
          content: SingleChildScrollView(
            child: ListBody(
              children: <Widget>[
                Text(
                    'Por favor asegurese de que el NIT es correcto y que se realizó la actualización de la base de datos'),
              ],
            ),
          ),
          actions: <Widget>[
            FlatButton(
              child: Text('Ok'),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
          ],
        );
      },
    );
  }

  Future<void> _campoVacio() async {
    return showDialog<void>(
      context: context,
      barrierDismissible: false, // user must tap button!
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text('Hay un campo vacío'),
          content: SingleChildScrollView(
            child: ListBody(
              children: <Widget>[
                Text('Por favor rellene todos los campos'),
              ],
            ),
          ),
          actions: <Widget>[
            FlatButton(
              child: Text('Ok'),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
          ],
        );
      },
    );
  }

  Future<void> yaFueEncuestada() async {
    return showDialog<void>(
      context: context,
      barrierDismissible: false, // user must tap button!
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text('Esta empresa ya fue encuestada previamente'),
          content: SingleChildScrollView(
            child: ListBody(
              children: <Widget>[
                Text('Si considera que hay un error, comunicarse con el encargado'),
              ],
            ),
          ),
          actions: <Widget>[
            FlatButton(
              child: Text('Ok'),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
          ],
        );
      },
    );
  }
  Future<bool> _onBackPressed() {
    return showDialog(
          context: context,
          builder: (context) => new AlertDialog(
            title: new Text('Salir de la aplicación'),
            content: new Text('¿Quiere salir de la aplicación?'),
            actions: <Widget>[
              new FlatButton(
                child: new Text("Si"),
                onPressed: () {
                  Navigator.of(context).pop(true);
                },
              ),
              new FlatButton(
                child: new Text("No"),
                onPressed: () {
                  Navigator.of(context).pop(false);
                },
              ),
              SizedBox(height: 16),
            ],
          ),
        ) ??
        false;
  }
}
